<?php
namespace Api\V8\Controller;
// include_once('./lib/PushNotification.php');
include_once('./lib/custom/CallOlaApi.php');
use Api\V8\Service\LogoutService;
use League\OAuth2\Server\ResourceServer;
use Slim\Http\Request;
use Slim\Http\Response;
use Api\V8\Service\AuthService; 
use Api\V8\Service\UserService;  
use Api\V8\Service\CommonService; 
class AuthController extends BaseController 
{
    protected $userService; 
    public function __construct(AuthService $authService,UserService $userService,CommonService $common)
    {
        $this->authService = $authService;
        $this->userService = $userService;  
        $this->common = $common; 
        
    } 

    public function getLogin(Request $request, Response $response){  
        try{
            
            $data = json_decode(file_get_contents('php://input'), true);
            $user = $this->authService->getLogin($data); 
            if(!empty($user)){
                $userResponse = ['resCode' => 1,'message' => 'succesfull','data' => $user];
            }else{
                $userResponse = ['resCode'=>0,'message' => 'please verify username or password','data' => []];
            }
            return $this->generateResponse($response, $userResponse, 200);
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }
    } 

    public function updateProfile(Request $request, Response $response){
        try{
            $jsonResponse = $this->userService->getCurrentUser($request);  
            $user = json_decode(json_encode($jsonResponse),true);
            $data = json_decode(file_get_contents('php://input'), true);
            $userProfile = $this->authService->updateProfile($data, $user);  
            if(!empty($userProfile)){
                $userResponse = ['resCode' => 1,'message' => 'User profile update succesfully','data' => []];
            }else{
                $userResponse = ['resCode'=>0,'message' => 'please verify username or password','data' => []];
            }
            return $this->generateResponse($response, $userResponse, 200);
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }
    }
    public function updateCuProfile(Request $request, Response $response){
        try{
            $jsonResponse = $this->userService->getCurrentUser($request);
            $user = json_decode(json_encode($jsonResponse),true);
            $data = json_decode(file_get_contents('php://input'), true);
            $userProfile = $this->authService->updateCuProfile($data, $user);  
            if(!empty($userProfile)){
                $userResponse = ['resCode' => 1,'message' => 'Customer profile update succesfully','data' => []];
            }else{
                $userResponse = ['resCode'=>0,'message' => 'please verify username or password','data' => []];
            }
            return $this->generateResponse($response, $userResponse, 200);
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }  
    }
    public function getPlanDetail(Request $request, Response $response){
        try{
            $jsonResponse = $this->userService->getCurrentUser($request);
            $user = json_decode(json_encode($jsonResponse),true);
            $cuProfile = $this->authService->getPlanDetail($user);
            // $cuProfile = [];
            $data['plan_data'] = (object)[];
            if(!empty($cuProfile)){ 
                $userResponse = ['resCode' => 1,'message' => 'Records found','data' => $cuProfile];
            }else{
                $userResponse = ['resCode'=>1,'message' => 'Plan not found','data' => $data];
            }
            return $this->generateResponse($response, $userResponse, 200);
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }    
    }
    public function changePassword(Request $request, Response $response) {
        try{
            $jsonResponse = $this->userService->getCurrentUser($request);  
            $user = json_decode(json_encode($jsonResponse),true);
            $data = json_decode(file_get_contents('php://input'), true);
          
            $checkOldPassword = $this->authService->checkOldPassword($data,$user);
            if($checkOldPassword == 0){
                $userResponse = ['resCode' => 0,'message' => 'Please check old password'];
                return $this->generateResponse($response, $userResponse, 200);
            }
            $userProfile = $this->authService->changePassword($data,$user);  
            if($userProfile){ 
                $userResponse = ['resCode' => 1,'message' => 'Change password succesfully'];
            }else{
                $userResponse = ['resCode'=>0,'message' => 'Change password failed'];
            }
            return $this->generateResponse($response, $userResponse, 200);
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }
    }

    public function checkType(Request $request, Response $response, array $args) {
        try {
            $data = json_decode(file_get_contents('php://input'), true); 
            $jsonResponse = $this->common->checkType($data);
            if(!empty($jsonResponse)){ 
                $typeResponse = ['resCode' => 1,'message' => 'Type get succesfully','data' => $jsonResponse];
            }else{
                $typeResponse = ['resCode'=>0,'message' => 'Records not found','data' => []];
            }
            return $this->generateResponse($response, $typeResponse, 200);
        } catch (Exception $exception) { 
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }
    }
    public function checkSubType(Request $request, Response $response, array $args) {
        try {
            $data = json_decode(file_get_contents('php://input'), true);
            $jsonResponse = $this->common->checkSubType($data); 
            if(!empty($jsonResponse)){ 
                $typeResponse = ['resCode' => 1,'message' => 'SubType get succesfully','data' => $jsonResponse];
            }else{
                $typeResponse = ['resCode'=>0,'message' => 'Records not found','data' => []];
            }
            return $this->generateResponse($response, $typeResponse, 200);
        } catch (Exception $exception) { 
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }
    }

    public function location(Request $request, Response $response, array $args) {
        try{
            $data = json_decode(file_get_contents('php://input'), true);
            $locationData = $this->authService->location($data);
            if(!empty($locationData)){
                $locationResponse = ['resCode' => 1,'message' => 'Location are added','data' => []];
            } else {
                $locationResponse = ['resCode'=>0,'message' => 'Records not founds','data' => []];
            }
            return $this->generateResponse($response, $locationResponse, 200);
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }
    }

    public function registerDevice(Request $request, Response $response, array $args) {
        try{
            $data = json_decode(file_get_contents('php://input'), true);
            $deviceData = $this->authService->registerDevice($data); 
            if(!empty($deviceData)){
                $deviceResponse = ['resCode' => 1,'message' => 'Your device register successfully','data' => []];
            } else {
                $deviceResponse = ['resCode'=>0,'message' => 'Some parameters are missing','data' => []];
            }
            return $this->generateResponse($response, $deviceResponse, 200);
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }
    }
    public function getNotificationList(Request $request, Response $response, array $args) {
        try{
            // $data = json_decode(file_get_contents('php://input'), true);
             $jsonResponse = $this->userService->getCurrentUser($request); 
             $data = json_decode(json_encode($jsonResponse),true);
             $notificationData = $this->authService->getNotificationList($data);  
             if(!empty($notificationData)){
                 $notificationResponse = ['resCode' => 1,'message' => 'Notification list get successfully','data' => $notificationData];
             }else{
                $notificationResponse = ['resCode'=>1,'message' => 'Notification list is empty','data' => []];
             }
             return $this->generateResponse($response, $notificationResponse, 200);
         } catch (Exception $exception) {
             $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
             return $this->generateErrorResponse($response, $exception, 400);
         }
    }    
    /**
     * @param $request (Email or Username)
     * @return string|null
     */
    public function forgotPassword(Request $request, Response $response, array $args){
        try{
            $data = json_decode(file_get_contents('php://input'), true);
            $deviceData = $this->authService->forgotPassword($data); 
            if(!empty($deviceData)){
                $deviceResponse = ['resCode' => 1,'message' => 'Forgot password link has been sent to your registered  email succesffuly','data' => (object)[]]; 
            } else {
                $deviceResponse = ['resCode'=>0,'message' => 'Please make sure you have entered valid username','data' => (object)[]];
            }
            return $this->generateResponse($response, $deviceResponse, 200);
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400); 
        }
    }
    public function getCountry(Request $request, Response $response){
        try{
           
            $country = $this->authService->getCountry(); 

            if(!empty($country)){ 
                $userResponse = ['resCode' => 1,'message' => 'Records found','data' => $country];
            }else{
                $userResponse = ['resCode'=>1,'message' => 'Records not found','data' => []];
            }
            return $this->generateResponse($response, $userResponse, 200);
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }    
    }
    public function getState(Request $request, Response $response){
        try{
            $state = $this->authService->getState(); 
            if(!empty($state)){ 
                $userResponse = ['resCode' => 1,'message' => 'Records found','data' => $state];
            }else{
                $userResponse = ['resCode'=>1,'message' => 'Records not found','data' => []];
            }
            return $this->generateResponse($response, $userResponse, 200);
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }    
    }
    public function getCity(Request $request, Response $response){
        try{
            $city = $this->authService->getCity(); 
            if(!empty($city)){ 
                $userResponse = ['resCode' => 1,'message' => 'Records found','data' => $city];
            }else{
                $userResponse = ['resCode'=>1,'message' => 'Records not found','data' => []];
            }
            return $this->generateResponse($response, $userResponse, 200);
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }    
    }
    public function getCustomerPojo(Request $request, Response $response){
        try{
            $jsonResponse = $this->userService->getCurrentUser($request); 
            $data = json_decode(json_encode($jsonResponse),true);
            if(!empty($data)){
                $cuData = $this->authService->getCutomerDtail($data['data']['attributes']['user_name']);
            }
            if(!empty($cuData)){ 
                $userResponse = ['resCode' => 1,'message' => 'Records found','data' => $cuData];
            }else{
                $userResponse = ['resCode'=>1,'message' => 'Records not found','data' => []];
            }
            return $this->generateResponse($response, $userResponse, 200); 
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        } 
    }
}

