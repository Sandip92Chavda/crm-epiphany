<?php
namespace Api\V8\Controller;
// include_once('./lib/PushNotification.php');
include_once('./lib/custom/CallOlaApi.php');
use Api\V8\Service\LogoutService;
use League\OAuth2\Server\ResourceServer;
use Slim\Http\Request;
use Slim\Http\Response;
use Api\V8\Service\AuthService; 
use Api\V8\Service\UserService;  
use Api\V8\Service\CommonService; 
class CommonController extends BaseController 
{
    protected $common; 
    protected $authService; 
    public function __construct(AuthService $authService,UserService $userService,CommonService $common)
    {
        $this->authService = $authService;
        $this->userService = $userService;  
        $this->common = $common; 
    } 

    public function getBillList(Request $request, Response $response){   
        try{
            $jsonResponse = $this->userService->getCurrentUser($request);
            $user = json_decode(json_encode($jsonResponse),true);
            $records = $this->common->getBillList($user);
            if(!empty($records)){ 
                $userResponse = ['resCode' => 1,'message' => 'Records found','data' => $records];
            }else{
                $userResponse = ['resCode'=>1,'message' => 'Records not found','data' => []];
            }
            return $this->generateResponse($response, $userResponse, 200);
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        } 
    } 
    public function getDashboard(Request $request, Response $response){   
        try{
            $jsonResponse = $this->userService->getCurrentUser($request);
            $user = json_decode(json_encode($jsonResponse),true);
            $records = $this->common->getDashboard($user); 
            if(!empty($records)){ 
                $userResponse = ['resCode' => 1,'message' => 'Records found','data' => $records];
            }else{
                $userResponse = ['resCode'=>1,'message' => 'Records not found','data' => []];
            }
            return $this->generateResponse($response, $userResponse, 200);
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        } 
    }
    public function getDataUsage(Request $request, Response $response){
        try{
            $jsonResponse = $this->userService->getCurrentUser($request);
            $user = json_decode(json_encode($jsonResponse),true);
            $records = $this->common->getDataUage($user);  
            if(!empty($records)){ 
                $userResponse = ['resCode' => 1,'message' => 'Records found','data' => $records];
            }else{
                $userResponse = ['resCode'=>1,'message' => 'Records not found','data' => []];
            }
            return $this->generateResponse($response, $userResponse, 200);
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        } 
    } 

   
}

