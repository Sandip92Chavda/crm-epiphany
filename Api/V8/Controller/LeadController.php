<?php
namespace Api\V8\Controller;

use Api\V8\Service\LogoutService;
use League\OAuth2\Server\ResourceServer;
use Slim\Http\Request;
use Slim\Http\Response;
use Api\V8\Service\UserService;
use Api\V8\Service\CommonService; 
use Api\V8\Service\LeadService; 
class LeadController extends BaseController 
{
    protected $leadService;  
    public function __construct(LeadService $leadService,UserService $userService)
    {
        $this->leadService = $leadService; 
        $this->userService = $userService;  
    }
    public function addLead(Request $request, Response $response, array $args) {    
        try {
           
            $jsonResponse = $this->userService->getCurrentUser($request);  
            $user = json_decode(json_encode($jsonResponse),true);
            $data = json_decode($_REQUEST['json'],true); 
            $note = json_decode(json_encode($_FILES['attachment']),true);
            $data['created_by'] = $user['data']['id'];
            $jsonResponse = $this->leadService->addLead($data,$note);   
            if(!empty($jsonResponse)){ 
                $ticketResponse = ['resCode' => 1,'message' => 'Lead add successfully','data' => []];
            }else{
                $ticketResponse = ['resCode' => 0,'message' => 'Lead is not added','data' => []];
            }
            return $this->generateResponse($response, $ticketResponse, 200);  
        } catch (Exception $exception) { 
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }
    } 
    public function getLead(Request $request, Response $response, array $args){
        try{
            $jsonResponse = $this->userService->getCurrentUser($request); 
            $user = json_decode(json_encode($jsonResponse),true);
            $lead = $this->leadService->getLead($user); 
            if(!empty($lead)) {   
                $leadResponse = ['resCode' => 1,'message' => 'Lead get successfully','data' => $lead];
            }else{
                $leadResponse = ['resCode' => 0,'message' => 'There is no recrds found','data' => []]; 
            }  
            return $this->generateResponse($response, $leadResponse, 200);
        } catch (Exception $exception) { 
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400); 
        }
    }
    public function getLeadDetail(Request $request, Response $response, array $args){
        try {
            $data = json_decode(file_get_contents('php://input'), true); 
            $jsonResponse = $this->leadService->getLeadDetail($data);  
            if(!empty($jsonResponse)){ 
                $leadResponse = ['resCode' => 1,'message' => 'Lead details get successfully','data' => $jsonResponse];
            }else{
                $leadResponse = ['resCode' => 0,'message' => 'Lead details not found','data' => []];
            } 
            return $this->generateResponse($response, $leadResponse, 200);
        } catch (Exception $exception) { 
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400); 
        }    
    } 
    public function updateLead(Request $request, Response $response, array $args) { 
        try {   
            // $data = json_decode(file_get_contents('php://input'), true); 
            $data = json_decode($_REQUEST['json'],true); 
            $note = json_decode(json_encode($_FILES['attachment']),true); 
            $jsonResponse = $this->leadService->updateLead($data,$note);  
            if(!empty($jsonResponse)){ 
                $ticketResponse = ['resCode' => 1,'message' => 'Lead update successfully','data' => []];
            }else{
                $ticketResponse = ['resCode' => 0,'message' => 'Lead update is not working','data' => []];
            }
            return $this->generateResponse($response, $ticketResponse, 200);
        } catch (Exception $exception) { 
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }
    }
}