<?php
namespace Api\V8\Controller;

use Api\V8\Service\LogoutService;
use Api\V8\Service\UserService;
use League\OAuth2\Server\ResourceServer;
use Slim\Http\Request;
use Slim\Http\Response;
use Api\V8\Service\TicketService;  

class TicketController extends BaseController 
{
    protected $userService; 
    public function __construct(TicketService $ticketService,UserService $userService)
    {
        $this->ticketService = $ticketService;  
        $this->userService = $userService; 
   
  
    }
    public function getTicket(Request $request, Response $response){  
        try{
           // $data = json_decode(file_get_contents('php://input'), true);
            $jsonResponse = $this->userService->getCurrentUser($request); 
            $user = json_decode(json_encode($jsonResponse),true);
            $ticket = $this->ticketService->getTicket($user);  
            if(!empty($ticket)){
                $ticketResponse = ['resCode' => 1,'message' => 'Ticket get successfully','data' => $ticket];
            }else{
                $ticketResponse = ['resCode' => 1,'message' => 'There is no recrds found','data' => []];
            }
            return $this->generateResponse($response, $ticketResponse, 200);
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }
    }

    public function addTicket(Request $request, Response $response, array $args) {
        try {
            $jsonResponse = $this->userService->getCurrentUser($request); 
            $user = json_decode(json_encode($jsonResponse),true);
            $data = json_decode($_REQUEST['json'],true); 
            $note = json_decode(json_encode($_FILES['attachment']),true);
            // $note = $_FILES['attachment'];
            $data['created_by'] = $user['data']['id'];
            $jsonResponse = $this->ticketService->addTicket($data,$note); 
            if(!empty($jsonResponse)){
                $ticketResponse = ['resCode' => 1,'message' => 'Ticket add successfully','data' => []];
            }else{
                $ticketResponse = ['resCode' => 0,'message' => 'Ticket is not added','data' => []];
            }
            return $this->generateResponse($response, $ticketResponse, 200);  
        } catch (Exception $exception) { 
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }
    }

    public function ticketDetails(Request $request, Response $response, array $args) {
        try {
            $data = json_decode(file_get_contents('php://input'), true); 
            $jsonResponse = $this->ticketService->ticketDetails($data);
            if(!empty($jsonResponse)){
                $ticketResponse = ['resCode' => 1,'message' => 'Ticket details get successfully','data' => $jsonResponse];
            }else{
                $ticketResponse = ['resCode' => 0,'message' => 'Ticket details not found','data' => []];
            }
            return $this->generateResponse($response, $ticketResponse, 200);
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }
    }

    public function updateTicket(Request $request, Response $response, array $args) {
        try {
            $data = json_decode(file_get_contents('php://input'), true); 
            $jsonResponse = $this->ticketService->updateTicket($data);
            if(!empty($jsonResponse)){
                $ticketResponse = ['resCode' => 1,'message' => 'Ticket update successfully','data' => []];
            }else{
                $ticketResponse = ['resCode' => 0,'message' => 'Ticket update is not working','data' => []];
            }
            return $this->generateResponse($response, $ticketResponse, 200);
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }
    }

    public function feedback(Request $request, Response $response, array $args) {
        try {
            $data = json_decode(file_get_contents('php://input'), true); 
            $jsonResponse = $this->ticketService->feedback($data); 
            if(!empty($jsonResponse)){
                $ticketResponse = ['resCode' => 1,'message' => 'Feedback added successfully','data' => []];
            }else{
                $ticketResponse = ['resCode' => 0,'message' => 'Feedback process is not working','data' => []];
            }
            return $this->generateResponse($response, $ticketResponse, 200);  
        } catch (Exception $exception) { 
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }
    }

    public function updateTicketStatus(Request $request, Response $response, array $args) {
        try {
            $data = json_decode(file_get_contents('php://input'), true); 
            $jsonResponse = $this->ticketService->updateTicketStatus($data);
            if(!empty($jsonResponse)){
                $ticketResponse = ['resCode' => 1,'message' => 'Update ticket status successfully','data' => []];
            }else{
                $ticketResponse = ['resCode' => 0,'message' => 'Ticket update status is not working','data' => []];
            }
            return $this->generateResponse($response, $ticketResponse, 200);
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }
    }

    public function addTicketUpdate(Request $request, Response $response, array $args) {
        try {
            $jsonResponse = $this->userService->getCurrentUser($request); 
            $user = json_decode(json_encode($jsonResponse),true);
            $data = json_decode(file_get_contents('php://input'), true); 
            $data['user_id'] = $user['data']['id'];
            $jsonResponse = $this->ticketService->addTicketUpdate($data);
            if(!empty($jsonResponse)){
                $ticketResponse = ['resCode' => 1,'message' => 'Add ticket update successfully','data' => []];
            }else{
                $ticketResponse = ['resCode' => 0,'message' => 'Add ticket update not working','data' => []];
            }
            return $this->generateResponse($response, $ticketResponse, 200);
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }
    }

    public function getAccountData(Request $request, Response $response, array $args) {
        try {
            $data = json_decode(file_get_contents('php://input'), true); 
            $jsonResponse = $this->ticketService->getAccountData($data);
            if(!empty($jsonResponse)){
                $ticketResponse = ['resCode' => 1,'message' => 'Account data get successfully','data' => $jsonResponse];
            }else{
                $ticketResponse = ['resCode' => 0,'message' => 'Records not found','data' => []];
            }
            return $this->generateResponse($response, $ticketResponse, 200);
        } catch (Exception $exception) {
            $response = ['resCode' => 0,'message' => 'Something went wrong','data' => []];
            return $this->generateErrorResponse($response, $exception, 400);
        }
    }

    
}

