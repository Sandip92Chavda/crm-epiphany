<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

namespace Api\V8\Service;
require_once './lib/custom/PushNotification.php';
require_once './modules/Users/User.php';   
include_once('./include/database/DBManagerFactory.php');
require_once('include/SugarPHPMailer.php');               


include_once('./lib/custom/CallOlaApi.php');
include_once('./lib/custom/RegisterOssApi.php');
use Api\V8\BeanDecorator\BeanManager;
use Api\V8\JsonApi\Helper\AttributeObjectHelper;
use Api\V8\JsonApi\Helper\RelationshipObjectHelper;
use Api\V8\JsonApi\Response\AttributeResponse;
use Api\V8\JsonApi\Response\DataResponse;
use Api\V8\JsonApi\Response\DocumentResponse;
use Slim\Http\Request;
use Api\V8\Service\CommonService; 
use BeanFactory;
use TimeDate; 

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * AuthService
 * 
 * @author gyula
 */
class AuthService 
{
    /**
     * @var db
     */
    protected $db;

    /**
     * @var BeanManager
     */
    protected $beanManager;

    /**
     * @var AttributeObjectHelper
     */
    protected $attributeHelper;

    /**
     * @var RelationshipObjectHelper
     */
    protected $relationshipHelper;

    public function __construct(){
        $this->db = \DBManagerFactory::getInstance();
        $this->common = new CommonService();
        $this->call = new \CallOlaApi();
        $this->register = new \RegisterOssApi();
        // $this->notification = new PushNotification();
        
    }

    public function getLogin($data){      
        // $res = $this->notification->sendPushNotification('hi',1);die;
        // $res = \PushNotification::sendPushNotification('hi',2);die;
        
        // $pushnotification = $this->common->sendPushNotification('Hi',3);die;
        // $push->sendPushNotification('Hi', 4);die;
        
        $userData = [];
        $db = \DBManagerFactory::getInstance();
        $GLOBALS['db'];
        $query = 'SELECT * FROM users'; 
        $query .= " WHERE user_name = '{$data['user_name']}'";
        $results = $GLOBALS['db']->query($query);
        $rowUser = $GLOBALS['db']->fetchByAssoc($results); 
        if(!empty($rowUser)){
            $pass = \User::checkPassword($data['password'],$rowUser['user_hash']);
            if($pass){
                $query = 'SELECT * FROM oauth2clients'; 
                $query .= " WHERE assigned_user_id = '{$rowUser['id']}'";
                $results = $GLOBALS['db']->query($query);
                $row = $GLOBALS['db']->fetchByAssoc($results);
                $processBean = BeanFactory::getBean('Users')->retrieve_by_string_fields(array('user
                    _name' => $data['user_name']));
                if($row){
                    $userData['client_id'] = $row['id'];
                    $userData['client_secret'] = $processBean->passwordtext_c;
                    $userData['grant_type'] = 'client_credentials';
                }
            }
        }
        return $userData;
    }
   
    public function updateProfile($data, $loginData) {
        
        try{
            $sql = sprintf('UPDATE users SET first_name = "%s",last_name = "%s",phone_mobile = "%s" ,address_street = "%s",address_postalcode = "%s" WHERE id = "%s"',
                $data["first_name"],$data["last_name"],$data["phone_mobile"],$data["address_street"],$data["address_postalcode"],$loginData['data']['id'] 
            ); 

            // echo $sql;die;
            $results = $this->db->query($sql);
            $sql = sprintf('UPDATE users_cstm SET city_c = "%s",state_c = "%s",country_c = "%s" WHERE id_c = "%s"',
            $data["city_c"],$data["state_c"],$data["country_c"],$loginData['data']['id']);
            // print_r($sql);die;
            $this->db->query($sql);
            return true;
        }
        catch(Exception $exception){
            return false;
        }
        
    }   
    public function updateCuProfile($data,$user){ 
        $sql =  sprintf('select * from accounts where `name` = "%s"',$user['data']['attributes']['user_name']);
        $results = $this->db->query($sql);
        $accountDetail = $this->db->fetchByAssoc($results);
   
        $account = BeanFactory::getBean("Accounts",$accountDetail['id']);
        $account->firstname_c = $data['first_name'];
        $account->lastname_c = $data['last_name'];
        // $account->billing_address_street = $data['address_street'];
        $account->phone_office = $data['phone_mobile'];
        // if(!empty($data['email'])){
        //     $account->emailAddress->addAddress($data['email'], true);
        // }  
        $account->save(); 
        return true;    
    }
    public function getPlanDetail($user){
        $sql =  sprintf('select * from accounts where `name` = "%s"',$user['data']['attributes']['user_name']);
        $results = $this->db->query($sql);
        $accountDetail = $this->db->fetchByAssoc($results);
        $account = BeanFactory::getBean("Accounts",$accountDetail['id']);
        $data = ['user_name' => $account->name,'first_name' => $account->firstname_c,'last_name' => $account->lastname_c,'phone_mobile' => $account->phone_office,'address_street' => $account->billing_address_street,'email1' => $account->email1,'account_number' => $account->accountnumber_c]; 
        $data['plan_data'] = (object)[];
        if(isset($account->plantype_c) && !empty($account->plantype_c)){
            $call = new \CallOlaApi();
            $planData = $call->getPlanDetail($account->plantype_c); 
            $data['plan_data'] = $planData;  
        } 
        return $data;
    }
    public function changePassword($data,$userData) {

        $sql =  sprintf('SELECT email_addresses.email_address FROM email_addr_bean_rel INNER JOIN email_addresses ON email_addresses.id = email_addr_bean_rel.email_address_id where email_addr_bean_rel.bean_id = "%s"',$userData['data']['id'] );
         // echo $sql;die;
        $results = $this->db->query($sql);
        $emailDetail = $this->db->fetchByAssoc($results);  
        
        $password = password_hash(strtolower(md5($data['new_password'])), PASSWORD_DEFAULT);
        $processBean = BeanFactory::getBean('Users')->retrieve_by_string_fields(array('id' => $userData['data']['id']));  
        // echo "<pre>";print_r($processBean);die;   
        $processBean->user_hash = $password; 
        $processBean->passwordtext_c = $data['new_password']; 
        $processBean->save();      
        /** update oauthclients */  
        $client = BeanFactory::newBean('OAuth2Clients')->retrieve_by_string_fields(array('assigned_user_id' => $userData['data']['id']));      
        $client->secret = hash('sha256', $data['new_password']);      
        $client->save(); 
           
        if(!empty($emailDetail['email_address'])){
            /*send mail for password changed*/
            $emailObj = new \Email();
            $defaults = $emailObj->getSystemDefaultEmail();
            $mail = new \SugarPHPMailer();
            $mail->setMailerForSystem();
            $mail->From = "{$defaults['email']}";
            $mail->FromName = "Epiphany Reset Password ";
            $mail->Subject = "Reset Password ";
             // $mail->IsHTML(true);
            $mail->Body = "Your password has been changed. \n Your new password is {$data['new_password']} ";
            $mail->prepForOutbound();  
            $mail->AddAddress($emailDetail['email_address']);
            @$mail->Send();
        }
        return true;
    }
    
    public function checkOldPassword($data,$userData) {
        $query = 'SELECT * FROM users'; 
        $query .= " WHERE id = '{$userData['data']['id']}'";
        $results = $GLOBALS['db']->query($query);
        $rowUser = $GLOBALS['db']->fetchByAssoc($results); 
        if(!empty($rowUser)){
            $pass = \User::checkPassword($data['old_password'],$rowUser['user_hash']);
            if($pass){
               return 1;
            }else{
                return 0;
            }
        }  
    }

    public function location($data) { 
        if(!empty($data['arr'])) {  
            $userId = !empty($data['arr'][0]['userId']) ? $data['arr'][0]['userId'] : 1;  
            $date = $this->common->convertDate($data['arr'][0]['date']);  
            $dateTime = $this->common->convertDateTime($data['arr'][0]['date']);  
            $userData =  $this->common->getUserData($data['arr'][0]['userId']);
            // print_r($userData['user_name']);die;
            $sql = sprintf('select * from  jjwg_areas where assigned_user_id = "%s" AND DATE(date_entered) = "%s"',
            $userId,$date);
            if(!empty($data['arr'])){
                foreach($data['arr'] as $key => $value){
                    // echo $this->common->convertDateTime($value['date']);die;
                    $data['arr'][$key]['date'] = $this->common->convertDateTime($value['date']);
                }
            }
            $results = $this->db->query($sql);
            if(!empty($results) && $results->num_rows == 0){
                $locationData = BeanFactory::newBean('jjwg_Areas');
                $locationData->name = !empty($userData['user_name']) ? $userData['user_name'] : $data['arr'][0]['userId'];
                $locationData->date_entered = $dateTime;
                $locationData->assigned_user_id = $userId;
                $locationData->coordinates = json_encode($data['arr']);
                $locationData->save();
            } else {
                $row  = $GLOBALS['db']->fetchByAssoc($results);
                $oldCor = json_decode(html_entity_decode($row['coordinates']));
                $newCor = json_encode(array_merge($oldCor,$data['arr']));
                $sql = sprintf("UPDATE jjwg_areas SET  coordinates = '%s' WHERE id = '%s'",
                $newCor,$row['id']);
                $this->db->query($sql);
            }
            return $results;
        }
    }

    //Register Device 
	public function registerDevice($args) {
		// \PushNotification::sendPushNotification('HI',4);	
		$currentDateTime=date('Y-m-d H:i:s');
		$this->db->query ( "SET NAMES 'utf8';" );
		//Check If Device Already Register Than Update It Else Create It
        $sql = sprintf('select * from apns_devices where `id` = "%s"',$args['id']);
		$deviceResult=$this->db->query($sql);
       
		if($deviceResult->num_rows > 0){
			//Update
            $sql = sprintf('UPDATE apns_devices SET app_name = "%s", app_version = "%s",
            os = "%s", device_token = "%s", device_name = "%s", device_model = "%s",
            device_version = "%s", push_badge = "%s", push_alert = "%s", push_sound = "%s",
            push_vibrate = "%s", environment = "%s", status = "active", language = "%s", 
            updated_date = "%s" WHERE id = "%s"', $args["app_name"], $args["app_version"], 
            $args["os"], $args["device_token"], $args["device_name"], $args["device_model"],
            $args["device_version"], $args["push_badge"], $args["push_alert"], $args["push_sound"],
            $args["push_vibrate"], $args["environment"], $args["language"], $currentDateTime, $args["id"]); 
            $result = $this->db->query($sql);
            return true;
        } else {
            $sql = sprintf("INSERT INTO apns_devices (client_id, id, device_uid, device_token, app_name, app_version, os, device_name, device_model, device_version, push_badge, push_alert, push_sound, push_vibrate, environment, language, status, created_date, updated_date, badge_count)VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",$args["client_id"], $args["id"], null, $args["device_token"], $args["app_name"], $args["app_version"], $args["os"], $args["device_name"], $args["device_model"], $args["device_version"], $args["push_badge"], $args["push_alert"], $args["push_sound"], $args["push_vibrate"], $args["environment"], $args["language"], 'active', $currentDateTime, $currentDateTime, 0);
            $insert = $this->db->query($sql);
            return true;
		}
	}

    // Get notification list
    public function getNotificationList($data) {
        $notifications = [];
        $db = \DBManagerFactory::getInstance();
        $GLOBALS['db'];   
          
        $sql = sprintf("select * from apns_master WHERE apns_master.id = '{$data['data']['id']}'
        ORDER BY updated_date DESC");
        // print_r($sql);die;
        $results = $this->db->query($sql);
        while ( $res = $GLOBALS["db"]->fetchByAssoc($results) ) {
            $notifications [] = $res;
        }
        return $notifications; 
    }
    public function forgotPassword(array $data){
        $sql = sprintf('select * from users where `user_name` = "%s"',$data['username']);
        $userResult=$this->db->query($sql);
        if($userResult->num_rows > 0){
            $usr = new \User();
            global $timedate;
            $usr_id=$usr->retrieve_user_id($data['username']);
            $usr->retrieve($usr_id);
            $guid=create_guid();
            $url=$GLOBALS['sugar_config']['site_url']."/index.php?entryPoint=Changenewpassword&guid=$guid";
            $time_now = TimeDate::getInstance()->nowDb();
            $q = "INSERT INTO users_password_link (id, username, date_generated) VALUES('".$guid."','".$data['username']."','".$time_now."') ";
            $this->db->query($q);
            $domain = $_SERVER['HTTP_HOST'];
            /*----send forgot password link --*/
            $description = "Please use the below link to reset your password \n  $domain/index.php?entryPoint=Changenewpassword&guid=$guid"; 

            
            $emailObj = new \Email();
            $defaults = $emailObj->getSystemDefaultEmail();
            $mail = new \SugarPHPMailer();
            
            $mail->setMailerForSystem();
            $mail->From = "{$defaults['email']}";
            $mail->FromName = "Epiphany Reset Password ";
            $mail->Subject = "Reset Password ";
             // $mail->IsHTML(true);
            $mail->Body = "{$description}";
            $mail->prepForOutbound();  
            $mail->AddAddress($usr->email1);
            @$mail->Send(); 
            return true;   
        }else{
            return false; 
        }
    }

    public function getCountry(){
        $data = [];
        $country = $this->call->getCountryList();
        if(!empty($country)){
            foreach($country as $key => $value){
                $data['countryList'][] = ['id' => $key,'name' => $value];
            }
        }
        return $data;
    }
    public function getState(){
        $data = [];
        $state = $this->call->getStateList();
        if(!empty($state)){
            foreach($state as $key => $value){
                $data['stateList'][] = ['id' => $key,'name' => $value];
            }
        }
        return $data;
    }
    public function getCity(){
        $data = [];
        $city = $this->call->getCityList();
        if(!empty($city)){
            foreach($city as $key => $value){
                $data['cityList'][] = ['id' => $key,'name' => $value];
            }
        }
        return $data;
    }
    public function getCutomerDtail($userName = ''){
        $data = $this->call->getCustomerDetail($userName);
        $connectionType = !empty($data[0]['connectionType']) ? $data[0]['connectionType'] : 0; 
        if(!empty($data[0])){
            $data[0]['connectionType'] = $this->common->getConnectionType($connectionType);
        }
        $full_addtreess = '';
        if(!empty($data[0]['id'])){
            $fullAddtreess = $this->register->getCustomerAddress($data[0]['id']);
            $data[0]['fullAddress'] = !empty($fullAddtreess[0]['fullAddress']) ? $fullAddtreess[0]['fullAddress'] : '';
        }
       return $data;
    }
}
