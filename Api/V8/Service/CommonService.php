<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

namespace Api\V8\Service;
require_once './modules/Users/User.php';   
include_once('./include/database/DBManagerFactory.php');
include_once('./lib/custom/CallOlaApi.php');
use Api\V8\BeanDecorator\BeanManager;
use Api\V8\JsonApi\Helper\AttributeObjectHelper;
use Api\V8\JsonApi\Helper\RelationshipObjectHelper;
use Api\V8\JsonApi\Response\AttributeResponse;
use Api\V8\JsonApi\Response\DataResponse;
use Api\V8\JsonApi\Response\DocumentResponse;
use Slim\Http\Request;

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * CommonService
 * 
 * @author gyula
 */
class CommonService 
{
    /**
     * @var db
     */
    protected $db;

    /**
     * @var BeanManager
     */
    protected $beanManager;

    /**
     * @var AttributeObjectHelper
     */
    protected $attributeHelper;

    /**
     * @var RelationshipObjectHelper
     */
    protected $relationshipHelper;

    public function __construct(){
        $this->db = \DBManagerFactory::getInstance();
        $this->thirdPartyApi = new \CallOlaApi();
    }  

    /**
     * @author priyankas.etechmavens@gmail.com
     * @param @id
     * @return userdata
     */
    public function getUserData($id) {
        $query = "SELECT users.id, users.user_name, users.first_name, users.last_name, users.phone_mobile, 
        ea.email_address as email1, users.address_street, users.address_city, users.address_state,
        users.address_country, users.address_postalcode FROM users 
        LEFT JOIN email_addr_bean_rel as er ON er.bean_id = users.id and er.primary_address = 1
        LEFT JOIN email_addresses as ea ON ea.id = er.email_address_id 
        WHERE users.id = '{$id}'";
        $results = $this->db->query($query);
        $row = $GLOBALS['db']->fetchByAssoc($results);
        $row['lat'] = 22.309425;
        $row['long'] = 72.136230;
        return $row;
    }   
    
    /**
     * @author priyankas.etechmavens@gmail.com
     * @param @table Name, @id, @where id
     * @return records
     */
    public function updateCreatedBy($tblName, $id, $whereId) {
        $sql = "UPDATE {$tblName} SET";
        $sql.= " modified_user_id = '{$id}'";
        $sql.= ",created_by = '{$id}'";
        if(isset($tblName) && $tblName == 'aop_case_updates'){
            $sql.= ",assigned_user_id = '{$id}'";
        }else{
            $sql.= ",assigned_user_id = null";
        }
        $sql.= " WHERE id = '{$whereId}'";
        $this->db->query($sql);
    }

     /**
     * @author priyankas.etechmavens@gmail.com
     * @param @data
     * @return records
     */
    public function checkType($data) {
        $records = [];
        $arr = array ('case_type_dom' => 'case_type','case_subtype_dom' => 'case_subtype','lead_source_dom' => 'lead_type');
        $key = array_search ($data['type'], $arr);
        $modLabel = translate($key);
        if(!empty($modLabel)){
            foreach($modLabel as $key => $val){
                if($data['type'] == 'case_type') {
                    $records[] = ['id' => $key,'name' => $val];
                }else{
                    $records[] = ['id' => $val,'name' => $val];
                }
            }
        }
        return $records;
    }
    public function checkSubType($data) {  
        $records = [];
        $modLabel = translate('case_subtype_dom');
        if(!empty($modLabel)){
            $count = 1;
            foreach($modLabel as $key => $val){
                $matchKey = $data['type_id'].'_'.$count;
                if($matchKey == $key){
                    $records[] = ['id' => $key,'name' => $val];
                }
                $count++;
            }
        }
        return $records;
    }
    public function getConnectionType($id){
        $records = [];
        $modLabel = translate('connection_type_list');
        return !empty($modLabel[$id]) ? $modLabel[$id] : 'null';
        
    }
    public function getSubTypeDetail($id) {  
        $modLabel = translate('case_subtype_dom');
        return !empty($modLabel[$id]) ? $modLabel[$id] : ''; 
    }
    public function getTypeDetail($id) {  
        $modLabel = translate('case_type_dom');
        return !empty($modLabel[$id]) ? $modLabel[$id] : ''; 
    } 
    public function convertDate($mili = null){
        $mili = $mili;
        $seconds = $mili / 1000;
        return  date("Y-m-d", $seconds);
    }
    
    public function convertDateTime($mili = null){ 
        $mili = $mili; 
        $seconds = $mili / 1000;
        return  date("Y-m-d H:i:s", $seconds);
    }


    public function sendPushNotification($message, $device_id) {
        //API URL of FCM
        $url = 'https://fcm.googleapis.com/fcm/send';

        /*api_key available in:
        Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key*/    $api_key = 'AAAAigZLar8:APA91bESN-D0lHc9YR_F5shwwZnqxnrQ3njGDNce1vcocGo1R2_Q7hYCdRLTjZ3F3HTSWmH9jjRjwrdOaRVVXRf8k5lO1V81e_svrs1NNAubYTYdwTLBwml8f3UzfLJSJobWJwBew04S';
		// print_r($device_id);die;          
        $fields = array (
            'registration_ids' => array (
				'dGNhON3RQ0mUCTXqG_mdiJ:APA91bGgpsLj4GU4Ya0NiG1uHiaGL4BAWAZfLJjGPCiFcU-WJe5ux5CT0XAdHaqS0IV5j4SLP4VgTXwRyC07AhmWR92Gl0wIVjqfqNMg_JyafvMf26TiEyHd6GRp_eo-b0dHDpmn1JqI'
            ),
			
            // 'data' => array (
            //     "message" => $message
            // )
			'notification' => array (
				"title" => "BackCharterApp",
				"body" => $message,
				'sound' => 'default'
			)
        );

        //header includes Content type and api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key='.$api_key
        );
                    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        
    }
    public function getBillList() {
       return $this->thirdPartyApi->getBillList();  
    }
    public function getDashboard($user) {
        $planDetail = '';
        $customerDetail = $this->thirdPartyApi->getCustomerDetail($user['data']['attributes']['user_name']);
        // $customerDetail = $this->thirdPartyApi->getCustomerDetail('ts');
        $response = ['plan_name' => '','start_date' => '', 'end_date' => '','remaining_days' => 0,'download' => 0,'upload' => 0];
        if(!empty($customerDetail[0]['planMappingPojoList'])) {
            foreach($customerDetail[0]['planMappingPojoList'] as $planData){
                if($planData['endDate'] == ''){
                    $planDetail = $this->thirdPartyApi->getPlanDetail($planData['planId']); 
                }    
            } 
        } 
        // echo "<pre>";print_r($customerDetail);print_r($planDetail);die;
        if(!empty($planDetail)) {  
            $response['plan_name'] = $planDetail['name'];
            $response['start_date'] = $customerDetail[0]['planMappingPojoList'][0]['startDate'];
            $response['end_date'] = $customerDetail[0]['planMappingPojoList'][0]['expiryDate']; 
            // $response['start_date'] = $planDetail['startDate'];
            // $response['end_date'] = $planDetail['endDate']; 
            // $response['remaining_days'] = $this->dateDifference($planDetail);
            $response['remaining_days'] = $this->dateDifference($response);
            $response['wallet_balance'] = 0;
            $response['price'] = !empty($planDetail['postpaidPlanChargePojoList'][0]['chargePojo']['price'])?$planDetail['postpaidPlanChargePojoList'][0]['chargePojo']['price']:0;
            $dataUsage = $this->thirdPartyApi->getDataUage($customerDetail[0]['username']); 
            if(!empty($dataUsage)){  
                $response['download'] = $dataUsage['download'];  
                $response['upload'] = $dataUsage['upload']; 
            }
        }    
        return $response; 
    }
    public function dateDifference($planDetail){ 
        $startDate = new \DateTime(date('Y-m-d'));
        $endDate = new \DateTime($planDetail['endDate']); 
        $difference = $endDate->diff($startDate);
        return $difference->format("%a"); 
    }
    public function getDataUage(){
        return $this->thirdPartyApi->getDataUage(); 
    }
	
}

