<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

namespace Api\V8\Service;
require_once './lib/custom/PushNotification.php';
require_once './modules/Users/User.php';   
include_once('./include/database/DBManagerFactory.php');
require_once './include/SugarEmailAddress/SugarEmailAddress.php';               
include_once('./lib/custom/RegisterOssApi.php');

use Api\V8\BeanDecorator\BeanManager;
use Api\V8\JsonApi\Helper\AttributeObjectHelper;
use Api\V8\JsonApi\Helper\RelationshipObjectHelper;
use Api\V8\JsonApi\Response\AttributeResponse;
use Api\V8\JsonApi\Response\DataResponse;
use Api\V8\JsonApi\Response\DocumentResponse;
use Slim\Http\Request;
use Api\V8\Service\CommonService; 
// use APi\V8\Service\PushNotification;
use BeanFactory;

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * AuthService
 * 
 * @author gyula
 */
class LeadService 
{
    public function __construct(){
        $this->db = \DBManagerFactory::getInstance();
        $this->common = new CommonService(); 
     }
     /**
     * @author sandeepc.etechmavens@gmail.com
     * @param @data
     * @return string
     */
    public function addLead(array $data,$note = '') {   
    	 
        $lead = BeanFactory::newBean('Leads');    
        $lead->first_name = !empty($data['first_name']) ? $data['first_name'] : '';  
        $lead->last_name = !empty($data['last_name']) ? $data['last_name'] : '';  
        $lead->phone_mobile = !empty($data['phone_mobile']) ? $data['phone_mobile'] : '';  
        $lead->phone_work = !empty($data['phone_mobile']) ? $data['phone_mobile'] : '';  
        $lead->description = !empty($data['description']) ? $data['description'] : '';  
        $lead->lead_source =  !empty($data['lead_source']) ? $data['lead_source'] : ''; 
        $lead->refered_by =  !empty($data['refered_by']) ? $data['refered_by'] : '';  
        $lead->addressstreet_c =  !empty($data['primary_address_street']) ? $data['primary_address_street'] : ''; 
        $lead->city_c =  !empty($data['city_c']) ? $data['city_c'] : 1; 
        $lead->state_c =  !empty($data['state_c']) ? $data['state_c'] : 1; 
        $lead->pincode_c =  !empty($data['primary_address_postalcode']) ? $data['primary_address_postalcode'] : ''; 
        $lead->country_c =  !empty($data['country_c']) ? $data['country_c'] : 1; 
        $lead->inquiryfor_c = $data["inquiry_for"];   
        if(!empty($data['email'])) { 
            $lead->emailAddress->addAddress($data['email'], true);
        } 
        $lead->save();
        global $sugar_config;
        $fileData = BeanFactory::newBean('Notes'); 
        if(!empty($note['name'])) {     
            $ext_pos = strrpos($note['name'],"."); 
            $file_ext = pathinfo($note['name'], PATHINFO_EXTENSION); 
            $this->uploadFile->file_ext = substr($note['name'], $ext_pos + 1);
            if (in_array($file_ext, $sugar_config['upload_badext'])) { 
                $this->uploadFile->stored_file_name .= ".txt";
                $this->uploadFile->file_ext = "txt"; 
            }    
            $fileData->filename = $note['name'];   
            $fileData->name = $data['first_name'];  
            $fileData->file_mime_type = $note['type'];
            $fileData->parent_id = $lead->id;
            $fileData->parent_type = 'Leads';     
            $return_id = $fileData->save();   
            $uploadFileDir = $_SERVER['DOCUMENT_ROOT'].'/upload/'.$fileData->id;
            if(move_uploaded_file($note['tmp_name'], $uploadFileDir)){ 
            }
        }     
        $this->common->updateCreatedBy('leads',$data['created_by'],$lead->id); 
        return $lead;   
    } 
    /**
     * @author sandeepc.etechmavens@gmail.com
     * @param $user
     * @return $leads
     */
    public function getLead($user) {      
        $leads = [];
        $query = 'SELECT leads.*,CONCAT(leads.first_name," ",leads.last_name) AS FullName,leads_cstm.inquiryfor_c as inquiry_for,ea.email_address as email, 
        users.user_name as assigned_user_name FROM leads  
        LEFT JOIN leads_cstm ON leads.id = leads_cstm.id_c 
        LEFT JOIN users ON users.id = leads.assigned_user_id
        LEFT JOIN email_addr_bean_rel as er ON er.bean_id = leads.id and er.primary_address = 1 and er.bean_module="Leads" and er.deleted = 0 
        LEFT JOIN email_addresses as ea ON ea.id = er.email_address_id';
        $query .= " WHERE leads.created_by = '{$user['data']['id']}' AND leads.deleted = 0  ORDER BY date_modified DESC";
        //print_r($query);die;
        $results =  $this->db->query($query);  
        while ($lead = $this->db->fetchByAssoc($results) ) {
            $lead['full_address'] = $lead['primary_address_street']." ".$lead['primary_address_city']." ".$lead['primary_address_state']." ".$lead['primary_address_postalcode']." ".$lead['primary_address_country'];  
             $leads[] = $lead;
        }
        return $leads;  
    }
    public function getLeadDetail($data){    
        $query = 'SELECT leads.*,CONCAT(leads.first_name," ",leads.last_name) AS FullName,leads_cstm.inquiryfor_c as inquiry_for,leads_cstm.accountnumber_c,leads_cstm.pincode_c,leads_cstm.city_c,leads_cstm.state_c,leads_cstm.country_c,leads_cstm.addressstreet_c,users.user_name, 
        ea.email_address as email,notes.filename, notes.name as attachmentName, notes.id as fileId,
        users.user_name as assigned_user_name FROM leads 
        LEFT JOIN leads_cstm ON leads.id = leads_cstm.id_c 
        LEFT JOIN users ON users.id = leads.assigned_user_id
        LEFT JOIN notes ON leads.id = notes.parent_id and parent_type = "Leads"
        LEFT JOIN email_addr_bean_rel as er ON er.bean_id = leads.id and er.primary_address = 1 and er.bean_module="Leads" and er.deleted = 0 
        LEFT JOIN email_addresses as ea ON ea.id = er.email_address_id 
        WHERE leads.id = ' . $GLOBALS['db']->quoted($data['id']); 
        //print_r($query);die;
        $results = $this->db->query($query);
        $row = $this->db->fetchByAssoc($results); 
        if(!empty($row)){
            $fullAddress = $row['addressstreet_c'].'-'.$row['pincode_c'];
            $call = new \RegisterOssApi(); 
            $custId = $call->getCustomerDetail($row["username"]);
            if(isset($custId) && !empty($custId)){
                $customerAddress = $call->getCustomerAddress($custId);
                if(!empty($customerAddress[0])){
                    $fullAddress = $customerAddress[0]['fullAddress'];
                }
            }
            if (file_exists("upload://{$row['fileId']}")) {
                // $object = json_decode(json_encode(['id' => $row['fileId']]), FALSE);
                $siteURL = $GLOBALS['sugar_config']['site_url'];
                // $fileUrl = $this->uploadFile->get_upload_url($object, $type='Notes');
                $row['File_URL'] = !empty($row['fileId'])? $siteURL.'/upload/'.$row['fileId']:null;
            }
            /*-get user data--*/
            $userData =  $this->common->getUserData($row['created_by']); 
            $row['lead_generate_user_detail'] = !empty($userData)?$userData:[]; 
            $row['full_address'] = $fullAddress;    
        }  
        return $row;
    }
    public function updateLead(array $data,$note = ''){ 
    	// print_r($data);die;
        $lead = BeanFactory::getBean("Leads",$data['id']);
        $lead->first_name = !empty($data['first_name']) ? $data['first_name'] : '';  
        $lead->last_name = !empty($data['last_name']) ? $data['last_name'] : '';  
        $lead->phone_mobile = !empty($data['phone_mobile']) ? $data['phone_mobile'] : '';  
        $lead->phone_work = !empty($data['phone_mobile']) ? $data['phone_mobile'] : '';  
        $lead->description = !empty($data['description']) ? $data['description'] : '';  
        $lead->lead_source =  !empty($data['lead_source']) ? $data['lead_source'] : ''; 
        $lead->refered_by =  !empty($data['refered_by']) ? $data['refered_by'] : ''; 
        $lead->addressstreet_c =  !empty($data['primary_address_street']) ? $data['primary_address_street'] : ''; 
        $lead->city_c =  !empty($data['city_c']) ? $data['city_c'] : ''; 
        $lead->state_c =  !empty($data['state_c']) ? $data['state_c'] : ''; 
        $lead->pincode_c =  !empty($data['primary_address_postalcode']) ? $data['primary_address_postalcode'] : ''; 
        $lead->country_c =  !empty($data['country_c']) ? $data['country_c'] : ''; 
        $lead->inquiryfor_c = $data["inquiry_for"]; 
        $lead->save();
        $fileData = BeanFactory::newBean('Notes');    
        if(!empty($note['name'])) {        
            /** delete existing file from notes */     
            $query  = "DELETE FROM notes WHERE parent_id='{$lead->id}'";
            $this->db->query($query);    
            /*-----------------------------------*/
            $ext_pos = strrpos($note['name'],"."); 
            $file_ext = pathinfo($note['name'], PATHINFO_EXTENSION); 
            $this->uploadFile->file_ext = substr($note['name'], $ext_pos + 1);
            if (in_array($file_ext, $sugar_config['upload_badext'])) { 
                $this->uploadFile->stored_file_name .= ".txt";
                $this->uploadFile->file_ext = "txt"; 
            }     
            $fileData->filename = $note['name'];   
            $fileData->name = $data['first_name'];  
            $fileData->file_mime_type = $note['type'];
            $fileData->parent_id = $lead->id;
            $fileData->parent_type = 'Leads';     
            $return_id = $fileData->save();   
            $uploadFileDir = $_SERVER['DOCUMENT_ROOT'].'/upload/'.$fileData->id;
            if(move_uploaded_file($note['tmp_name'], $uploadFileDir)){ 
            }
        }
        /** update email address **/
        $sea = new \SugarEmailAddress;
        $sea->addAddress($data['email'], true);
        $sea->save($data['id'], "Leads");  
        return $lead;
    }
} 
