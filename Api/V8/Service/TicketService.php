<?php
/**
 *
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 *
 * SuiteCRM is an extension to SugarCRM Community Edition developed by SalesAgility Ltd.
 * Copyright (C) 2011 - 2018 SalesAgility Ltd.
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 *
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 *
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 *
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo and "Supercharged by SuiteCRM" logo. If the display of the logos is not
 * reasonably feasible for technical reasons, the Appropriate Legal Notices must
 * display the words "Powered by SugarCRM" and "Supercharged by SuiteCRM".
 */

namespace Api\V8\Service;
require_once './modules/Users/User.php';   
include_once('./include/database/DBManagerFactory.php');
include_once('./include/UploadFile.php');
include_once('./include/UploadStream.php');
require_once('./include/SugarPHPMailer.php');
require_once './lib/custom/PushNotification.php';

use Api\V8\BeanDecorator\BeanManager;
use Api\V8\JsonApi\Helper\AttributeObjectHelper;
use Api\V8\JsonApi\Helper\RelationshipObjectHelper;
use Api\V8\JsonApi\Response\AttributeResponse;
use Api\V8\JsonApi\Response\DataResponse;
use Api\V8\JsonApi\Response\DocumentResponse;
use Slim\Http\Request;
use BeanFactory;
use Api\V8\Service\CommonService;


if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

/**
 * UserService
 * 
 * @author gyula
 */
class TicketService 
{
    /**
     * @var db
     */
    protected $db;

    /**
     * @var BeanManager
     */
    protected $beanManager;

    /**
     * @var AttributeObjectHelper
     */
    protected $attributeHelper;

    /**
     * @var RelationshipObjectHelper
     */
    protected $relationshipHelper;

    /**
     * @var UploadFile
     */
    protected $uploadFile;
    
     /**
     * @var PushNotification
     */
    protected $pushNotification;

    public function __construct(){
	$this->db = \DBManagerFactory::getInstance();
	$this->common = new CommonService();
	$this->uploadFile = new \UploadFile();
	$this->pushNotification = new \PushNotification();
    }

    /**
     * @author priyankas.etechmavens@gmail.com
     * @param $user
     * @return $tickets
     */
    public function getTicket($user) {  
        $tickets = [];
        $db = \DBManagerFactory::getInstance();
        $GLOBALS['db'];
        
        $query = 'SELECT cases.*,accounts.name as account_name,
        users.user_name as assigned_user_name FROM cases 
        LEFT JOIN accounts ON accounts.id = cases.account_id
        LEFT JOIN users ON users.id = cases.assigned_user_id ';
        $query .= " WHERE cases.assigned_user_id = '{$user['data']['id']}' OR cases.created_by = '{$user['data']['id']}' ORDER BY date_modified DESC";
        $results = $GLOBALS['db']->query($query);
        // $tickets = $GLOBALS['db']->fetchByAssoc($results); 
        while ( $ticket = $GLOBALS["db"]->fetchByAssoc($results) ) {
            if(!empty($ticket)){
                $priority = $ticket['priority'];
                switch ($priority ) { 
                    case "P1":
                        $ticket['priority'] = 'High';
                        break;
                    case "P2":
                        $ticket['priority'] = 'Medium';
                        break;
                    case "P3":
                        $ticket['priority'] = 'Low';
                        break;
                    default:
                    $ticket['priority'] = '';
                }
            }
            $status = $ticket['status'];
            switch ($status ) { 
                case "Open_New":
                    $ticket['status'] = 'New';
                    break;
                case "Open_Assigned":
                    $ticket['status'] = 'Assigned';
                    break;
                case "Open_Pending Input":
                    $ticket['status'] = 'Pending Input';
                    break;
                default:
                $ticket['status'] = '';
            }
            $state = $ticket['state'];
            switch ($state ) { 
                case "Open":
                    $ticket['state'] = 'Open';
                    break;
                case "Closed":
                    $ticket['state'] = 'Closed';
                    break;
                case "Reject":
                    $ticket['state'] = 'Reject';
                    break;
                case "InReassignment":
                     $ticket['state'] = 'InReassignment';
                break;  
                default:
                $ticket['state'] = '';
            }
             $tickets[] = $ticket;
        }
        return $tickets;
    }


    /**
     * @author priyankas.etechmavens@gmail.com
     * @param @data
     * @return message
     */
    public function addTicket($data, $note) {
        $case = BeanFactory::newBean('Cases');
        $case->name = $data['name'];
        $case->description = $data['description'];
        $case->status = 'Open_New';
        $case->priority = !empty($data['priority']) ? $data['priority'] : 'P1';
        $case->type = $data['type'];
        $case->subtype_c = $data['subtype'];
        $case->save();
          
        global $sugar_config;
        $fileData = BeanFactory::newBean('Notes');
        if (!empty($note['name'])) {
            $ext_pos = strrpos($note['name'],".");
            $file_ext = pathinfo($note['name'], PATHINFO_EXTENSION);
            $this->uploadFile->file_ext = substr($note['name'], $ext_pos + 1);
            if (in_array($file_ext, $sugar_config['upload_badext'])) {
                $this->uploadFile->stored_file_name .= ".txt";
                $this->uploadFile->file_ext = "txt";
            } 

            $fileData->filename = $note['name'];
          
            $fileData->name = $data['name'];
            $fileData->file_mime_type = $note['type'];
            $fileData->parent_id = $case->id;
            $fileData->parent_type = 'Cases';
            $fileData->id = $note['id'];
            $return_id = $fileData->save();

        $uploadFileDir = $_SERVER['DOCUMENT_ROOT'].'/upload/'.$fileData->id;
            if(move_uploaded_file($note['tmp_name'], $uploadFileDir))
            {
              
            }
        }
 
        $this->common->updateCreatedBy('cases',$data['created_by'],$case->id);
        $this->common->updateCreatedBy('notes',$data['created_by'],$fileData->id);

        return true;
    }

    /**
     * @author priyankas.etechmavens@gmail.com
     * @param $data array
     * @return $row array 
     */
    public function ticketDetails(array $data) {
        $query = 'SELECT cases.*,cases_cstm.subtype_c,cases_cstm.rating_c, cases_cstm.feedback_c, accounts.name as account_name, 
        users.user_name as assigned_user_name, 
        notes.filename, notes.name as attachmentName, notes.id as fileId
        FROM cases 
        LEFT JOIN notes ON cases.id = notes.parent_id and parent_type = "Cases"
        LEFT JOIN cases_cstm ON cases.id = cases_cstm.id_c 
        LEFT JOIN accounts ON accounts.id = cases.account_id
        LEFT JOIN users ON users.id = cases.assigned_user_id
        WHERE cases.id = ' . $GLOBALS['db']->quoted($data['id']);           
        $results = $this->db->query($query);
        $row = $GLOBALS['db']->fetchByAssoc($results);
       
      
       
        if(!empty($row)){
             $row['type_name'] = $this->common->getTypeDetail($row['type']);
             $row['subtype_name'] = $this->common->getSubTypeDetail($row['subtype_c']);
            // print_r($row);die; 
            /*-- get attachment url --*/
            if (file_exists("upload://{$row['fileId']}")) {
                // $object = json_decode(json_encode(['id' => $row['fileId']]), FALSE);
                $siteURL = $GLOBALS['sugar_config']['site_url'];
                // $fileUrl = $this->uploadFile->get_upload_url($object, $type='Notes');
                $row['File_URL'] = !empty($row['fileId'])? $siteURL.'/upload/'.$row['fileId']:null;
            }
            $row['description'] = strip_tags(html_entity_decode($row['description']));
            $row['resolution'] = strip_tags(html_entity_decode($row['resolution']));

            /*-get user data--*/
            $userData =  $this->common->getUserData($row['created_by']);
            $row['customer_detail'] = !empty($userData)?$userData:[];

            /** get internal ticket update data */
           
            $ticketData =  $this->getInternalTicketData($row['id']);
            $row['ticket_update_thread'] = !empty($ticketData)?$ticketData:[];

            $status = $row['status'];
            switch ($status ) { 
                case "Open_New":
                $row['status'] = 'New';
                break;
                case "Open_Assigned":
                    $row['status'] = 'Assigned';
                    break;
                case "Open_Pending Input":
                    $row['status'] = 'Pending Input';
                    break;
                default:
                $row['status'] = '';
            }
            $priority = $row['priority'];
            switch ($priority ) { 
                case "P1":
                    $row['priority'] = 'High';
                    break;
                case "P2":
                    $row['priority'] = 'Medium';
                    break;
                case "P3":
                    $row['priority'] = 'Low';
                    break;
                default:
                $row['priority'] = '';
            }
            $state = $row['state'];
            switch ($state ) { 
                case "Open":
                    $row['state'] = 'Open';
                    break;
                case "Closed":
                    $row['state'] = 'Closed';
                    break;
                case "Reject":
                    $row['state'] = 'Reject';
                    break;
                case "InReassignment":
                    $row['state'] = 'InReassignment';
                    break;    
                default:
                $row['state'] = '';
            }
        }
        return $row;
    }

    /**
     * @author priyankas.etechmavens@gmail.com
     * @param $data
     * @return message
     */
    public function updateTicket($data) {
        $sql = 'UPDATE cases SET';
        if(!empty($data['name'])){ 
            $sql.= " name = '{$data["name"]}',";
        }
        if(!empty($data['description'])){ 
            $sql.= " description = '{$data["description"]}',";
        }
        if(!empty($data['priority'])){ 
            $sql.= " priority = '{$data["priority"]}',";
        }
        if(!empty($data['status'])){ 
            $sql.= " status = '{$data["status"]}'";
        }
        $sql.= " WHERE id = '{$data['id']}'";
        $results = $this->db->query($sql);
        if(!empty($data['subtype'])) {
            $case = BeanFactory::getBean("Cases",$data['id']);
            $case->subtype_c = $data['subtype'];
            $case->save(); 
        }
        return true; 
    }

    /**
     * @author priyankas.etechmavens@gmail.com
     * @param $data
     * @return message
     */
    public function feedback($data) {
        $sql = sprintf('select * from cases_cstm where `id_c` = "%s"',$data['id_c']);
        $res = $this->db->query($sql);

        if($res->num_rows > 0) {
            $sql = sprintf('UPDATE cases_cstm SET id_c = "%s", feedback_c = "%s",
            rating_c = "%s" WHERE id_c = "%s"', $data["id_c"], $data["feedback_c"], 
            $data["rating_c"], $data["id_c"]); 
            $result = $this->db->query($sql);
            return true;
        } else {
            $sql = sprintf("INSERT INTO cases_cstm (id_c, feedback_c, rating_c)VALUES
            ('%s','%s','%s')",$data["id_c"], $data["feedback_c"], $data["rating_c"]);
            $resultdata= $this->db->query($sql);
            return true;
        }
     }

    /**
     * @author priyankas.etechmavens@gmail.com
     * @param $data
     * @return message
     */
    public function updateTicketStatus($data) {
        $sql = 'UPDATE cases SET';
        $sql.= " state = '{$data["state"]}'";
        if(!empty($data['resolution'])){
            $sql.= ",resolution = '{$data["resolution"]}'";
        } 
        $sql.= " WHERE id = '{$data['id']}'";   
        // $query = "UPDATE cases SET name = '{$data["name"]}'  WHERE id = '{$data['id']}'";
        $results = $this->db->query($sql);
        
        $ticketData =  $this->ticketDetails($data);
        //print_r($ticketData['created_by']);die;
        $message = 'Ticket Status Update';
        $messageType = "push";
        $receiverId = $ticketData['created_by'];
        $id = "{$receiverId}";
        $customArr['push_type']= 'Admin';
        $deliver_by_crone = 'Yes';
        $status = "sent";
        $ticket_id = $ticketData['id'];
        $respo = \PushNotification::sendPushMessage($message,$messageType, $id, $receiverId,
             $customArr, $deliver_by_crone, $status,$ticket_id);
        return true;
        // return ['message' => 'Update ticket status succesfull','data' => ['case_id' => $data['id']]];
    }

    /**
     * @author priyankas.etechmavens@gmail.com
     * @param $data
     * @return message
     */
    public function addTicketUpdate($data) {
        $case = BeanFactory::newBean('AOP_Case_Updates');
        $case->case_id = $data['id'];
        $case->description = $data['description']; 
        $case->name = $data['description']; 
        $case->internal = $data['is_internal'];
        $case->save();
        
        $ticketData =  $this->ticketDetails($data);
        //print_r($ticketData['created_by']);die;
        $message = 'Internal Update Ticket ';
        $messageType = "push";
        $receiverId = $ticketData['created_by'];
        $id = "{$receiverId}";
        $customArr['push_type']= 'Admin';
        $deliver_by_crone = 'Yes';
        $status = "sent";
        $ticket_id = $ticketData['id'];
        $respo = \PushNotification::sendPushMessage($message,$messageType, $id, $receiverId,
             $customArr, $deliver_by_crone, $status,$ticket_id);
             
        $this->common->updateCreatedBy('aop_case_updates',$data['user_id'],$case->id);
        return true; 
    } 

    public function getInternalTicketData($case_id) {
        $updates = [];
        $query = "SELECT aop_case_updates.name, aop_case_updates.date_entered as date,  
        users.user_name as assigned_user_name FROM aop_case_updates 
        LEFT JOIN users ON users.id = aop_case_updates.assigned_user_id
        WHERE aop_case_updates.case_id = '{$case_id}' AND internal != 1";
        $results = $this->db->query($query);
        while ( $ticketUpdate = $GLOBALS["db"]->fetchByAssoc($results) ) {
            $updates [] = $ticketUpdate;
        }
        return $updates;  
    } 
    
    /**
     * @author priyankas.etechmavens@gmail.com
     * @param @data
     * @return message
     */
    public function getAccountData($data) {
        $records = [];
        $query = "SELECT id,name FROM accounts WHERE name LIKE '%{$data['search_text']}%' AND deleted != 1";
        $results = $this->db->query($query);
        while ( $row = $GLOBALS["db"]->fetchByAssoc($results) ) {
            $records [] = $row;
        }
        return $records;
    }

    
}


