

<select name="{$fields.run_when.name}"
        id="{$fields.run_when.name}"
        title=''  tabindex="1"          
        >

    {if isset($fields.run_when.value) && $fields.run_when.value != ''}
        {html_options options=$fields.run_when.options selected=$fields.run_when.value}
    {else}
        {html_options options=$fields.run_when.options selected=$fields.run_when.default}
    {/if}
</select>