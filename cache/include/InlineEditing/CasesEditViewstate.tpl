

<select name="{$fields.state.name}"
        id="{$fields.state.name}"
        title=''  tabindex="1"          
        >

    {if isset($fields.state.value) && $fields.state.value != ''}
        {html_options options=$fields.state.options selected=$fields.state.value}
    {else}
        {html_options options=$fields.state.options selected=$fields.state.default}
    {/if}
</select>