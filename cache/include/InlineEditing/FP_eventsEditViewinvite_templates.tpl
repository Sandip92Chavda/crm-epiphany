

<select name="{$fields.invite_templates.name}"
        id="{$fields.invite_templates.name}"
        title=''  tabindex="1"          
        >

    {if isset($fields.invite_templates.value) && $fields.invite_templates.value != ''}
        {html_options options=$fields.invite_templates.options selected=$fields.invite_templates.value}
    {else}
        {html_options options=$fields.invite_templates.options selected=$fields.invite_templates.default}
    {/if}
</select>