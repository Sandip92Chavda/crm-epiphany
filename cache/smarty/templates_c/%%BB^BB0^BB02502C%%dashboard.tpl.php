<?php /* Smarty version 2.6.31, created on 2022-01-28 06:02:30
         compiled from custom/themes/dashboard.tpl */ ?>
<?php echo '
<style>
  .panel-body {
    height: 60px;
  }
  .main-heading {
   font-weight: 700;
   font-size: medium;
   padding: 4px !important;
   } 
   .heading {
   font-weight: 700;
   font-size: 15px;
   } 
   .panel-default{
   margin-right:3%;
   font-size:30px;
   }
   .content-body{
   margin-bottom: 80%;
   }
   table {
  border: 1px solid #ccc;
  border-collapse: collapse;
  margin: 0;
  padding: 0;
  width: 100%;
  table-layout: fixed;
}

table caption {
  font-size: 1.5em;
  margin: .5em 0 .75em;
}

table tr {
  //background-color: #f8f8f8;
  border: 1px solid #ddd;
  padding: .35em;
}

table th,
table td {
  padding: .625em;
  text-align: center;
}

table th {
  font-size: .85em;
  letter-spacing: .1em;
  text-transform: uppercase;
}

@media screen and (max-width: 600px) {
  table {
    border: 0;
  }

  table caption {
    font-size: 1.3em;
  }
  
  table thead {
    border: none;
    clip: rect(0 0 0 0);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
  }
  
  table tr {
    border-bottom: 3px solid #ddd;
    display: block;
    margin-bottom: .625em;
  }
  
  table td {
    border-bottom: 1px solid #ddd;
    display: block;
    font-size: .8em;
    text-align: right;
  }
  
  table td::before {
    /*
    * aria-label has no advantage, it won\'t be read inside a table
    content: attr(aria-label);
    */
    content: attr(data-label);
    float: left;
    font-weight: bold;
    text-transform: uppercase;
  }
  
  table td:last-child {
    border-bottom: 0;
  }
}
.piechart {
  height : 40px;
  width : 430px;
  background-color : #DDDDDD;
  padding:14px;
  font-weight:700;
}
.barchart {
  height : 40px;
  width : 470px;
  background-color : #DDDDDD;
  padding:10px;
  font-weight:700;
  
}
</style>
<style>
.container{
  width:100%;
  margin:auto;
}
.c-dashboardInfo {
  margin-bottom: 15px;
}
.c-dashboardInfo .wrap {
  background: #ffffff;
  box-shadow: 2px 10px 20px rgba(0, 0, 0, 0.1);
  border-top-left-radius: 7px;
  border-top-right-radius: 7px;
  text-align: center;
  position: relative;
  overflow: hidden;
  padding: 40px 25px 20px;
  height: 100%;
}
.c-dashboardInfo__title,
.c-dashboardInfo__subInfo {
  color: #6c6c6c;
  font-size: 1.18em;
}
.c-dashboardInfo span {
  display: block;
}
.c-dashboardInfo__count {
  font-weight: 600;
  font-size: 2.5em;
  line-height: 64px;
  color: #323c43;
}
.c-dashboardInfo .wrap:after {
  display: block;
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 10px;
  content: "";
}

.c-dashboardInfo:nth-child(1) .wrap:after {
  background: linear-gradient(82.59deg, #00c48c 0%, #00a173 100%);
}
.c-dashboardInfo:nth-child(2) .wrap:after {
  background: linear-gradient(81.67deg, #0084f4 0%, #1a4da2 100%);
}
.c-dashboardInfo:nth-child(3) .wrap:after {
  background: linear-gradient(69.83deg, #0084f4 0%, #00c48c 100%);
}
.c-dashboardInfo:nth-child(4) .wrap:after {
  background: linear-gradient(81.67deg, #ff647c 0%, #1f5dc5 100%);
}
.c-dashboardInfo__title svg {
  color: #d7d7d7;
  margin-left: 5px;
}
.link{
    background: rgb(255 255 255 / 15%);
    position: inherit;
    box-shadow: 2px 10px 20px rgba(0, 0, 0, 0.1);
    overflow: hidden;
    font-weight: 600;
    font-size:1.2em;
    padding:6px;
}
.link > a{
    color:#323c43;
}
.right-tag{
  float:right
}
.assigned_me{
    border: 1px solid #c7c0c0ad;
    border-radius: 15px;
    background: white;
    margin-bottom:17px;
}
.lead_chart{
    border: 2px solid #c7c0c0ad;
    border-radius: 15px;
    background: white;
    margin-bottom:17px;
   
}
.ticket_chart{
    border: 2px solid #c7c0c0ad;
    border-radius: 13px;
    background: white;
    margin-bottom:17px;
}
.date{ 
  background:white
}
.title{
  margin-right:5px;
  color:#6c6c6c;
}
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/js/all.min.js"></script> 
'; ?>

<div id="customer">
  <div class="container pt-5">
    <div class="row align-items-stretch">
      <div class="c-dashboardInfo col-lg-3 col-md-6">
        <div class="wrap">
          <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">Online Customers</h4><span class="hind-font caption-12 c-dashboardInfo__count"><?php echo $this->_tpl_vars['total_online_customer']; ?>
</span> 
        </div> 
        <div class="link"> 
            <a href="index.php?action=index&module=Accounts&filter=online_customer">view more  
              <span class="right-tag"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>
            </a>
          </div>
      </div>  
      <div class="c-dashboardInfo col-lg-3 col-md-6"> 
        <div class="wrap">
          <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">New Customers</h4>
          <span class="hind-font caption-12 c-dashboardInfo__count"></span>
          <!--<span
            class="hind-font caption-12 c-dashboardInfo__subInfo">Last month: €30</span>-->
            <span class="hind-font caption-12 c-dashboardInfo__count"><?php echo $this->_tpl_vars['total_new_customer']; ?>
</span>
            <!-- <span
            class="hind-font caption-12 c-dashboardInfo__subInfo">Last month: €30</span>-->
        </div> 
          <div class="link"> 
            <a href="index.php?action=index&module=Accounts&filter=new_customer">view more  
              <span class="right-tag"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>
            </a>
          </div> 
        </div>
      <div class="c-dashboardInfo col-lg-3 col-md-6">
        <div class="wrap">
          <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">New & Open Tickets</h4><span class="hind-font caption-12 c-dashboardInfo__count"><?php echo $this->_tpl_vars['new_tickets']; ?>
</span>
        </div>
        <div class="link">
            <a href="index.php?action=index&module=Cases&filter=new">view more  
              <span class="right-tag"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>
            </a>
          </div>
      </div>
          </div>
  </div> 
</div>
<div id="ticket">
  <div class="container pt-5">
    <div class="row align-items-stretch">
      <div class="c-dashboardInfo col-lg-3 col-md-6">
        <div class="wrap"> 
          <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">New Tickets</h4><span class="hind-font caption-12 c-dashboardInfo__count"><?php echo $this->_tpl_vars['new_tickets']; ?>
</span> 
        </div> 
        <div class="link">  
            <a href="index.php?action=index&module=Cases&filter=new">view more  
              <span class="right-tag"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>
            </a>
                      </div>
      </div> 
      <div class="c-dashboardInfo col-lg-3 col-md-6">  
        <div class="wrap">
          <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">Working Progress</h4>
          <span class="hind-font caption-12 c-dashboardInfo__count"></span>
          <!--<span
            class="hind-font caption-12 c-dashboardInfo__subInfo">Last month: €30</span>-->
            <span class="hind-font caption-12 c-dashboardInfo__count"><?php echo $this->_tpl_vars['working_tickets']; ?>
</span>
            <!-- <span
            class="hind-font caption-12 c-dashboardInfo__subInfo">Last month: €30</span>-->
        </div>   
        <div class="link">  
          <a href="index.php?action=index&module=Cases&filter=working">view more  
            <span class="right-tag"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>
          </a>  
        </div>  
      </div>
      <div class="c-dashboardInfo col-lg-3 col-md-6">
        <div class="wrap">
          <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">Resolved Tickets</h4><span class="hind-font caption-12 c-dashboardInfo__count"><?php echo $this->_tpl_vars['resolved_tickets']; ?>
</span>
        </div> 
        <div class="link">
            <a href="index.php?action=index&module=Cases&searchFormTab=advanced_search&query=true&state=closed">view more  
              <span class="right-tag"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>
            </a>
          </div>
      </div>
      <div class="c-dashboardInfo col-lg-3 col-md-6">
        <div class="wrap">
          <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">In Reassignment</h4><span class="hind-font caption-12 c-dashboardInfo__count"><?php echo $this->_tpl_vars['reassignment_tickets']; ?>
</span> 
        </div>
        <div class="link">
            <a href="index.php?action=index&module=Cases&searchFormTab=advanced_search&query=true&state=InReassignment">view more  
              <span class="right-tag"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>
            </a>   
          </div>
      </div>
    </div>
  </div>
</div>
<div id="leads">
  <div class="container pt-5">
    <div class="row align-items-stretch">
      <div class="c-dashboardInfo col-lg-3 col-md-6">
        <div class="wrap">
          <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">New Lead</h4><span class="hind-font caption-12 c-dashboardInfo__count"><?php echo $this->_tpl_vars['new_leads']; ?>
</span> 
        </div> 
        <div class="link"> 
            <a href="index.php?action=index&module=Leads&filter=new_lead">view more  
              <span class="right-tag"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>
            </a>
          </div>
      </div>  
      <div class="c-dashboardInfo col-lg-3 col-md-6"> 
        <div class="wrap">
          <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">Active Lead</h4>
          <span class="hind-font caption-12 c-dashboardInfo__count"></span>
          <!--<span
            class="hind-font caption-12 c-dashboardInfo__subInfo">Last month: €30</span>-->
            <span class="hind-font caption-12 c-dashboardInfo__count"><?php echo $this->_tpl_vars['active_leads']; ?>

            </span> 
            <!-- <span
            class="hind-font caption-12 c-dashboardInfo__subInfo">Last month: €30</span>-->
        </div> 
          <div class="link"> 
            <a href="index.php?action=index&module=Leads&filter=active_lead">view more  
              <span class="right-tag"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>
            </a>
          </div> 
        </div>
      <div class="c-dashboardInfo col-lg-3 col-md-6">
        <div class="wrap">
          <h4 class="heading heading5 hind-font medium-font-weight c-dashboardInfo__title">Deal </h4><span class="hind-font caption-12 c-dashboardInfo__count"><?php echo $this->_tpl_vars['convert_leads']; ?>
</span>
        </div>
        <div class="link">
            <a href="index.php?action=index&module=Leads&searchFormTab=advanced_search&query=true&status=Converted">view more  
              <span class="right-tag"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>
            </a> 
          </div>
      </div>
    </div>
  </div> 
</div>
<div class="container">
  <div class="row"> 
    <div class="col-md-6 assigned_me"> 
      <div> 
        <h4>Assigned to me (Tickets)<span style="float:right;margin-bottom:10px">Status
            <select class="form-control" onClick="getMyTicket(this,'assignMe')">
                <option value="all">ALL</option>
                <option value="Open">Open</option>
                <option value="Closed">Closed</option>
                <option value="Reject">Reject</option>
                <option value="InReassignment">InReassignment</option>
            </select>  
          </span></h4>
      </div>  
      <table id="example" class="table table-striped table-bordered table_id">
          <thead> 
              <tr>
                  <th>ID</th>
                  <th>Title</th>
                  <th>Status</th>
                  <th>Priority</th>
              </tr> 
          </thead>
          <tbody id="assign_me"> 
          <?php  $i = 1;  ?>
             <?php unset($this->_sections['myTicket']);
$this->_sections['myTicket']['name'] = 'myTicket';
$this->_sections['myTicket']['loop'] = is_array($_loop=($this->_tpl_vars['myTicket'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['myTicket']['show'] = true;
$this->_sections['myTicket']['max'] = $this->_sections['myTicket']['loop'];
$this->_sections['myTicket']['step'] = 1;
$this->_sections['myTicket']['start'] = $this->_sections['myTicket']['step'] > 0 ? 0 : $this->_sections['myTicket']['loop']-1;
if ($this->_sections['myTicket']['show']) {
    $this->_sections['myTicket']['total'] = $this->_sections['myTicket']['loop'];
    if ($this->_sections['myTicket']['total'] == 0)
        $this->_sections['myTicket']['show'] = false;
} else
    $this->_sections['myTicket']['total'] = 0;
if ($this->_sections['myTicket']['show']):

            for ($this->_sections['myTicket']['index'] = $this->_sections['myTicket']['start'], $this->_sections['myTicket']['iteration'] = 1;
                 $this->_sections['myTicket']['iteration'] <= $this->_sections['myTicket']['total'];
                 $this->_sections['myTicket']['index'] += $this->_sections['myTicket']['step'], $this->_sections['myTicket']['iteration']++):
$this->_sections['myTicket']['rownum'] = $this->_sections['myTicket']['iteration'];
$this->_sections['myTicket']['index_prev'] = $this->_sections['myTicket']['index'] - $this->_sections['myTicket']['step'];
$this->_sections['myTicket']['index_next'] = $this->_sections['myTicket']['index'] + $this->_sections['myTicket']['step'];
$this->_sections['myTicket']['first']      = ($this->_sections['myTicket']['iteration'] == 1);
$this->_sections['myTicket']['last']       = ($this->_sections['myTicket']['iteration'] == $this->_sections['myTicket']['total']);
?>  
                <tr>   
                    <td><?php  echo $i++;  ?></td> 
                    <td><?php echo $this->_tpl_vars['myTicket'][$this->_sections['myTicket']['index']]['name']; ?>
</td>  
                    <td><?php echo $this->_tpl_vars['myTicket'][$this->_sections['myTicket']['index']]['state']; ?>
</td> 
                    <td>
                       <?php if ($this->_tpl_vars['myTicket'][$this->_sections['myTicket']['index']]['priority'] == 'P1'): ?>
                        <span class="p1">High</span>
                        <?php elseif ($this->_tpl_vars['myTicket'][$this->_sections['myTicket']['index']]['priority'] == 'P2'): ?>
                        <span class="p2">Medium</span>
                        <?php elseif ($this->_tpl_vars['myTicket'][$this->_sections['myTicket']['index']]['priority'] == 'P3'): ?>
                        <span class="p3">Low</span> 
                        <?php endif; ?>
                    </td>
                </tr>  
              <?php endfor; endif; ?>
          </tbody>
      </table> 
    </div> 
    <?php if ($this->_tpl_vars['is_manager'] == '0'): ?>
    <div class="col-md-6 assigned_me"> 
      <h4>Assigned to administrator (Tickets)<span style="float:right;margin-bottom:10px">Status
            <select class="form-conrol" onClick="getMyTicket(this,'administrator')">
                <option value="all">ALL</option>
                <option value="Open">Open</option>
                <option value="Closed">Closed</option>
                <option value="Reject">Reject</option>
                <option value="InReassignment">InReassignment</option>
            </select> 
          </span></h4>
      <table id="example" class="table table-striped table-bordered table_id">
          <thead> 
              <tr>
                  <th>Assignee</th>
                  <th>Count</th>
                  <th>Percentage</th>
              </tr>
          </thead>
          <tbody id="administrator">   
              <tr> 
                  <td>Unassigned</td>  
                  <td><?php echo $this->_tpl_vars['new_tickets']; ?>
</td>
                  <td><progress id="file" value="<?php echo $this->_tpl_vars['unassignPercentage']; ?>
" max="100"> <?php echo $this->_tpl_vars['unassignPercentage']; ?>
% </progress> 
              </tr>
          </tbody>  
      </table> 
    </div>
    <?php endif; ?>
  </div>
</div>
<div class="row">     
  <div class="col-md-6  ticket_chart">       
    <div class="chart-view" style="background:#FFFFFF">
    <div class="row"> 
      <div class="col-md-3"> 
        <div class="form-group" style="margin-left:3px">
          <label>From</label>
          <input class="form-control date" type="date" value="" id="from_date" placeholder="from date"/> 
        </div> 
      </div>
      <div class="col-md-3">
        <div class="form-group" style="margin-left:3px">
        <label>To</label>
          <input class="form-control date" type="date" value="" id="to_date" placeholder="to date"/> 
        </div> 
      </div>
      <div class="col-md-6">
       <h4 class="pull-right title">Ticket Statistics</h4>
      </div>
      <div class="col-md-8" style="margin-top:2%">
            <button class="btn btn-info" onClick="ticket_filter('created')">Created</button> 
            <button class="btn btn-primary" onClick="ticket_filter('Closed')">Resolved</button> 
            <button class="btn btn-warning" onClick="ticket_filter('InReassignment')">In Reassignment</button> 
            <!--<button class="btn btn-light">All</button> -->
      </div>
    </div>
        <canvas id="myChart" ></canvas>
    </div> 
  </div>
  <div class="col-md-5 col-md-offset-1 lead_chart" style="background:#FFFFFF">    
    <div class="chart-view" >
    <div class="row"> 
      <div class="col-md-12" style="margin-top:2%">
            <button class="btn btn-info" onClick="leadFilter('day')">Day</button> 
            <button class="btn btn-primary" onClick="leadFilter('week')">Week</button> 
            <button class="btn btn-warning" onClick="leadFilter('month')">Month</button> 
            <h4 class="pull-right title">Lead Statistics</h4>
      </div>
    </div>
        <canvas id="myLeadChart"></canvas>
    </div> 
  </div>
</div>
<div class="row">
 <div class="col-md-8 lead_chart" style="background:#FFFFFF">    
    <div class="chart-view" >
    <div class="row"> 
      <div class="col-md-12" style="margin-top:2%">
            <h4 class="pull-left title">Average Revenue Per User</h4>
            <div class="pull-right">
            <button class="btn btn-info">Day</button> 
            <button class="btn btn-primary">Week</button> 
            <button class="btn btn-warning">Month</button>
            <button class="btn btn-secondary">Yearly</button> 
            </div>
      </div> 
    </div>
    <?php if ($this->_tpl_vars['is_manager'] == '0'): ?>
     <div class="row"> 
      <div class="col-md-12" style="margin-top:2%">
      
            <div class="pull-right">
            <lable class="title">Partners</lable>
                <select class="form-control">
                  <option>All Selected</option>
                </select><br>
                <lable class="title">Locations</lable>
                <select class="form-control">
                  <option>All Selected</option>
                </select>
            </div> 
         
    </div>
    <?php endif; ?>
    </div>
        <canvas id="myRevenueChart"></canvas>
    </div> 
  </div>
</div>
<!--<div class="row content-body">
   <div class="col-md-12">
      <div class="col-md-6">
         <div class="piechart">
          <div><p style="float:left;font-size:x-large;">Tickets : 2</p></div>
        </div>
        <div 
          style="padding: 0px;
            margin: 0px 0px 5px 0px;
            height: 300px; 
            width: 430px;
            background-color: #DDDDDD";
        >
            <div id="chart_div">
            </div>
            <div style="display:flex">
                <div class="bar-box-blue" style="border-radius:14px;margin-left:12%;margin-top:4px;height:10px;width:10px;background:#DD4210;"></div> &nbsp;High
                <div class="bar-box-red" style="border-radius:14px;margin-left:12%;margin-top:4px;height:10px;width:10px;background:#F59804;"></div> &nbsp;Medium 
                <div class="bar-box-blue" style="border-radius:14px;margin-left:12%;margin-top:4px;height:10px;width:10px;background:blue;"></div> &nbsp;Low 
            </div>    
         </div>
      </div>
       <div class="col-md-6">
       <div class="barchart">
          <div><p style="float:left;font-size:x-large;">Leads : <?php echo $this->_tpl_vars['total_lead']; ?>
</p><p style="float:right;font-size:x-large;">Converted : <?php echo $this->_tpl_vars['total_convert_lead']; ?>
</p></div>
       </div>
          <p style="background-color : #DDDDDD;font-weight:700;width : 470px;font-size:larger;"> Convert Leads (Last 6 Months)</p>
       <div
          style="
            padding: 0px;
            margin: 0px 0px 5px 0px;
            height: 280px;
            width: 470px; 
            background-color: #DDDDDD;
        "
        >
         <div id="barchart_values" style="margin-left:1px"></div>
         <div style="display:flex">
            <div class="bar-box-red" style="margin-left:24%;margin-top:4px;height:10px;width:10px;background:#DD4210;"></div> &nbsp;Converted 
            <div class="bar-box-blue" style="margin-left:12%;margin-top:4px;height:10px;width:10px;background:blue;"></div> &nbsp;Created
         </div>
         </div>
       </div>
      </div>
      <div class="panel panel-default col-md-5" style="background-color:#17a2b8!important;height:250px;overflow-y: scroll;">
         <div class="panel-heading heading text-center" style="background-color:#348A9D!important"> <span style="float:left">Recent Accounts (Last 30 Days)</span>
         <span style="float:right">Total Accounts : <?php echo $this->_tpl_vars['accounts']; ?>
</span>
         </div>   
         <div class="panel-body text-center">
         <span style="font-size:x-large;color:white" class="">
           <table>
            <thead>
            <tr>
                  <th>Name</th>
                  <th>Mobile</th>
                  <th>Created At</th>
               </tr>
            <?php unset($this->_sections['getAccount']);
$this->_sections['getAccount']['name'] = 'getAccount';
$this->_sections['getAccount']['loop'] = is_array($_loop=($this->_tpl_vars['account_data'])) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['getAccount']['show'] = true;
$this->_sections['getAccount']['max'] = $this->_sections['getAccount']['loop'];
$this->_sections['getAccount']['step'] = 1;
$this->_sections['getAccount']['start'] = $this->_sections['getAccount']['step'] > 0 ? 0 : $this->_sections['getAccount']['loop']-1;
if ($this->_sections['getAccount']['show']) {
    $this->_sections['getAccount']['total'] = $this->_sections['getAccount']['loop'];
    if ($this->_sections['getAccount']['total'] == 0)
        $this->_sections['getAccount']['show'] = false;
} else
    $this->_sections['getAccount']['total'] = 0;
if ($this->_sections['getAccount']['show']):

            for ($this->_sections['getAccount']['index'] = $this->_sections['getAccount']['start'], $this->_sections['getAccount']['iteration'] = 1;
                 $this->_sections['getAccount']['iteration'] <= $this->_sections['getAccount']['total'];
                 $this->_sections['getAccount']['index'] += $this->_sections['getAccount']['step'], $this->_sections['getAccount']['iteration']++):
$this->_sections['getAccount']['rownum'] = $this->_sections['getAccount']['iteration'];
$this->_sections['getAccount']['index_prev'] = $this->_sections['getAccount']['index'] - $this->_sections['getAccount']['step'];
$this->_sections['getAccount']['index_next'] = $this->_sections['getAccount']['index'] + $this->_sections['getAccount']['step'];
$this->_sections['getAccount']['first']      = ($this->_sections['getAccount']['iteration'] == 1);
$this->_sections['getAccount']['last']       = ($this->_sections['getAccount']['iteration'] == $this->_sections['getAccount']['total']);
?>
               <tr>
                  <td><?php echo $this->_tpl_vars['account_data'][$this->_sections['getAccount']['index']]['name']; ?>
</td>
                  <td><?php echo $this->_tpl_vars['account_data'][$this->_sections['getAccount']['index']]['phone_office']; ?>
</td>
                  <td><?php echo $this->_tpl_vars['account_data'][$this->_sections['getAccount']['index']]['date_entered']; ?>
</td>
               </tr>
             <?php endfor; endif; ?>
             </thead>   
           </table> 
         </span>
         </div>
      </div>
   </div>
   <div class="col-md-12" style = "margin-top:0px">
      
   </div>
</div>-->

<script type="text/javascript" src="themes/default/js/chart.js"></script> 
<?php echo '       
      <script type="text/javascript">
   /* const result = { 
      datasets: [{ 
            type: \'bar\',
            label: \'Bar Dataset\',
            data: [10, 20, 30, 40]
        }, {
            type: \'bar\',
            label: \'Line Dataset\',
            data: [50, 50, 50, 50],
        }], 
        labels: [\'January\', \'February\', \'March\', \'April\']    
    };  */
  var chart =  new Chart("myChart", { 
            type: "bar",
            data: {},
            options: {
              title: {
                display: false,
                text: \'Ticket Statistics\',
                fontSize:25
              },
              legend: {
                  display: true,
                  labels: {
                      fontSize: 15
                  }
              },
              scales: {
                  xAxes: [{
                      barThickness: 10,  // number (pixels) or \'flex\'
                      maxBarThickness: 10 // number (pixels)
                  }]
              }
            } 
    });
    function ticket_filter(type){   
      var from_date = $(\'#from_date\').val();
      var to_date = $(\'#to_date\').val();
      if(from_date != \'\' && to_date != \'\'){
          $.ajax({  
            url: "index.php?entryPoint=AjaxCall",
            data:{\'type\':type,\'from_date\':from_date,\'to_date\':to_date}, 
            dataType : \'JSON\', 
            success: function(response){  
              var result = { 
                  datasets: [{ 
                        type: \'bar\',
                        label: response.data.lable,
                        data:response.data.count,
                        backgroundColor: response.data.background
                    }], 
                    labels: response.data.range    
                };
                chart.data = result;
                chart.update();
            } 
          });
            
      }else{
        alert("Please make sure both date is fillable");
      }
    }
    var leadChart =  new Chart("myLeadChart", { 
            type: "bar",
            data: {},
            options: { 
              title: {
                display: false,
                text: \'Lead Statistics\',
                fontSize:25
              },
              legend: {
                  display: true,
                  labels: {
                      fontSize: 15
                  }
              },
              scales: {
                  xAxes: [{
                      barThickness: 10,  // number (pixels) or \'flex\'
                      maxBarThickness: 10 // number (pixels)
                  }]
              }
            } 
    });
    function leadFilter(type){ 
      $.ajax({  
        url: "index.php?entryPoint=AjaxCall",
        data:{\'type\':type},
        dataType : \'JSON\',
        success: function(response){  
            var result = { 
              datasets: [{ 
                    type: \'bar\',
                    label: response.data.lable,
                    data:response.data.count,
                    backgroundColor: response.data.background
                }], 
                labels: response.data.range    
            };
            leadChart.data = result;
            leadChart.update();
        } 
      }); 
    }
    var revenueChart =  new Chart("myRevenueChart", { 
            type: "bar",
            data: {},
            options: { 
              title: {
                display: false,
                text: \'Lead Statistics\',
                fontSize:25
              },
              legend: {
                  display: true,
                  labels: {
                      fontSize: 15
                  }
              },
              scales: {
                  xAxes: [{
                      barThickness: 10,  // number (pixels) or \'flex\'
                      maxBarThickness: 10 // number (pixels)
                  }]
              }
            } 
    });
    </script> 
'; ?>
    
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.0/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.11.0/js/jquery.dataTables.min.js"></script> 
<?php echo ' 
<script>       
  $(document).ready(function(){  
      $(\'.table_id\').DataTable({
          "bLengthChange": false,
          "bInfo": true,
          "bFilter": true
      });
  }); 
  function getMyTicket(el,type){ 
    var status = $(el).val(); 
    if(status == \'all\'){ 
      location.reload();
    } 
    $.ajax({  
        url: "index.php?entryPoint=AjaxCall",
        data:{\'type\':type,\'status\':status},   
        dataType : \'JSON\', 
        success: function(response){  
          if(type == \'assignMe\'){
            $("#assign_me").html(response.html);
          }    
          if(type == \'administrator\'){ 
            $("#administrator").html(response.html);
          } 
        } 
    });
  } 
  
</script>
'; ?>


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!--Load the AJAX API-->

<?php if ($this->_tpl_vars['low'] != 0 || $this->_tpl_vars['high'] != 0 || $this->_tpl_vars['medium'] != 0): ?>

   <?php echo '
    
    <script type="text/javascript">
      var currentYear = new Date().getFullYear();
       // Load the Visualization API and the corechart package.
      google.charts.load(\'current\', {\'packages\':[\'corechart\']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart); 

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn(\'string\', \'Topping\');
        data.addColumn(\'number\', \'Slices\');
        data.addRows([
          [\'Low\', '; ?>
<?php echo $this->_tpl_vars['low']; ?>
<?php echo '],
          [\'High\', '; ?>
<?php echo $this->_tpl_vars['high']; ?>
<?php echo '],
          [\'Medium\','; ?>
<?php echo $this->_tpl_vars['medium']; ?>
<?php echo '],
        ]);

        // Set chart options
        var options = {
                     titleTextStyle: {
                        fontSize: 20,
                        color:"#747474"
                     },
                     legend: {
                        position: \'none\'
                     },
                    chartArea: {width: 420, height: 270}, 
                     backgroundColor: \'#DDDDDD\',
                     // pieStartAngle: 100,
                     //\'title\':\'Tickets : (Year - \'+currentYear+\')\',
                     //\'title\':\'Tickets : '; ?>
<?php echo $this->_tpl_vars['total_ticket']; ?>
<?php echo '\',
                      //is3D: true,
                       \'width\':420,
                       \'height\':270
                       };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById(\'chart_div\'));
        chart.draw(data, options);
          $("text:contains(" + options.title + ")").attr({
            \'x\': \'5\',
            \'y\': \'20\'
         });
      }
  
    </script>
    '; ?>

   <?php endif; ?>
   <?php if ($this->_tpl_vars['low'] == 0 && $this->_tpl_vars['high'] == 0 && $this->_tpl_vars['medium'] == 0): ?>

   <?php echo ' 
    
    <script type="text/javascript">
      var currentYear = new Date().getFullYear();
       // Load the Visualization API and the corechart package.
      google.charts.load(\'current\', {\'packages\':[\'corechart\']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart); 

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn(\'string\', \'Topping\');
        data.addColumn(\'number\', \'Slices\');
        data.addRows([
          [\'No Data\',0],
        ]);

        // Set chart options
        var options = {
                     titleTextStyle: {
                        fontSize: 20,
                        color:"#747474"
                     },
                     legend: {
                        position: \'bottom\'
                     },
                     chartArea: {width: 420, height: 270}, 
                     backgroundColor: \'#DDDDDD\',
                      pieStartAngle: 100,
                     //\'title\':\'Tickets : (Year - \'+currentYear+\')\',
                     \'title\':\'Tickets : (No Data Found)\',
                      is3D: true,
                      \'width\':420,
                       \'height\':270
                       };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById(\'chart_div\'));
        chart.draw(data, options);
          $("text:contains(" + options.title + ")").attr({
            \'x\': \'10\',
            \'y\': \'30\'
         });
      }
  
    </script>
    '; ?>

   <?php endif; ?> 
    
    
    <?php echo '
    <script>
      google.charts.load(\'current\', {\'packages\':[\'bar\']});
      google.charts.setOnLoadCallback(drawChart1);
      const obj = JSON.parse(\''; ?>
<?php echo $this->_tpl_vars['leadConvertArray']; ?>
<?php echo '\');
    
      function drawChart1() {
        
         dataArray = [];
         dataArray[0] = [\'Month\', \'Created\', \'Converted\']; 
         var j=0;
         $.each(obj, function(index, value) {
            dataArray[++j] = value; 
         });
         var data = google.visualization.arrayToDataTable(dataArray); 
         var options = {
            \'width\':450,
            \'height\':260,
             backgroundColor: \'#DDDDDD\',
             legend: {
                        position: \'none\'
                     },
            //title: "Leads : 2      Converted : 1",
            //subtitle: "Convert Leads (Last 6 Months )",
          //bar: {groupWidth: "95%"},
            chartArea: {
               left:50,top:2,width:"50%",height:"50%",
               \'backgroundColor\': {
                  \'fill\': \'#F4F4F4\',
                  \'opacity\': 50,
               },
            }
        };
        var chart = new google.charts.Bar(document.getElementById(\'barchart_values\'));
        chart.draw(data, google.charts.Bar.convertOptions(options));
        $("text:contains(" + options.title + ")").attr({
            \'x\': \'10\',
            \'y\': \'30\'
         });
      }
    </script>
    '; ?>

    
  