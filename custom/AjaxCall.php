<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
include_once('./include/database/DBManagerFactory.php');
include_once('./custom/ManagerTicketList.php'); 
use BeanFactory;global $current_user;

## intilize manager ticket ## 
$managerTicket = new ManagerTicketList();

##get current user role ##
$role = new ACLRole();
$roles = $role->getUserRoleNames($current_user->id);

## intilize db ##
$db = \DBManagerFactory::getInstance();
$status = $_GET['status'];
$response = []; 
/********************************* Assigned / Administator ************************/
if($_GET['type'] == 'assignMe'){
    if(in_array('manager',$roles)){
        $ticketRecords = $managerTicket->getTicketList();
        $assignedTickets = "select * from cases where cases.created_by in ({$ticketRecords} AND deleted = 0 AND state = '{$status}'";   
    }else{
        $assignedTickets = "select * from cases where assigned_user_id = {$current_user->id} AND deleted = 0 AND state = '{$status}'";   
    }
    
    $assignedTicktResults = $db->query($assignedTickets);     
    $assignedTicketArray = [];   
    while($row = $db->fetchByAssoc($assignedTicktResults)) {
        $assignedTicketArray[] = $row; 
    } 
    $html = '';$priority = '';$i = 1;
    if(count($assignedTicketArray)>0){
        foreach($assignedTicketArray as $value){ 
            if($value['priority'] == 'P1'){
                $priority='High';
            }
            else if($value['priority'] == 'P2'){
                $priority='Medium';
            }
            else if($value['priority'] == 'P3'){
                $priority='Low';
            }
            $html.= "<tr> 
            <td>".$i++."</td>
            <td>".$value['name']."</td>
            <td>".$value['state']."</td>
            <td>".$priority."</td>
            </tr>"; 
        } 
        $response = ['status' => 1 , 'html' => $html];
    }else{
        $html .= '<tr><td colspan="4">Records are not found</td></tr>';
        $response = ['status' => 0 , 'html' => $html];
    }
    echo json_encode($response);  die;  
}
if($_GET['type'] == 'administrator'){
    $adminTickets = "select count(*) as total_count from cases where (cases.assigned_user_id IS NULL OR cases.assigned_user_id = '') and deleted = 0 AND state = '{$status}'";  
    $adminTicketResults = $db->query($adminTickets);     
    $adminTicketList = $db->fetchByAssoc($adminTicketResults);     
    $unassignPercentage = $adminTicketList['total_count'] / 100;
    $html = '';
    $html .="<tr><td>Unassigned</td>
    <td>".$adminTicketList['total_count']."</td>
    <td><progress  value='".$unassignPercentage."' max='100'> ".$unassignPercentage."% </progress> 
    </tr>";
    $response = ['status' => 1 , 'html' => $html];
    echo json_encode($response);  die;
} 

/************************Ticket Statistics *************************************/
$to_date = $_GET['to_date'];
$from_date = $_GET['from_date'];
$monthName = [];$date = '';
$period = new DatePeriod(
    new DateTime($from_date),
    new DateInterval('P1D'),
    new DateTime($to_date)
);
$background = [];$type= $_GET['type'];
$dataRange = []; $date = '';$totalTicketCount = [];$dateRange = [];$totalLeadCount=[];
if(in_array($type,['created','Closed','InReassignment'])){
    
    foreach ($period as $key => $value) { 
        $date = $value->format('Y-m-d'); 
        if($type == 'created'){
            $ticketBean = "select count(*) as total_count from cases where DATE(date_entered) = '{$date}'";
        } else if(in_array($type,['Closed','InReassignment'])){  
            $ticketBean = "select count(*) as total_count from cases where DATE(date_entered) = '{$date}' AND state = '{$type}'";   
        } 
        $createdTicket = $db->query($ticketBean);     
        $createdTicketList = $db->fetchByAssoc($createdTicket); 
        if($createdTicketList['total_count'] != 0){  
            $totalTicketCount[] =  $createdTicketList['total_count']; 
            $dateRange[] = date('d-m',strtotime($date));
            if($type == 'created'){
                $background[] = '#9589B3';
            }
            if($type == 'Closed'){
                $background[] = '#778591';
            }
            if($type == 'InReassignment'){
                $background[] = '#e6d5a5';
            }
        } 
    } 
    $response = ['lable' => $type,'count' => $totalTicketCount,'range' => $dateRange,'background' => $background];  
    echo json_encode(['data' => $response]); die;  
}   
/**************************Lead Statistics*************************************/
if(in_array($type,['day','week','month'])){
    $dataRange = []; $date = '';$totalTicketCount = [];$dateRange = [];
    $currentDay = date('Y-m-d');
    
    if($type == 'day') {
        $leadBean = "select count(*) as total_count from leads where DATE(date_entered) = '{$currentDay}'";
        $createdLead = $db->query($leadBean);     
        $createdLeadList = $db->fetchByAssoc($createdLead);
        if($createdLeadList['total_count'] != 0 && $type == 'day'){   
            $totalLeadCount[] =  $createdLeadList['total_count']; 
            $dateRange[] = date('d-m',strtotime($currentDay));
            if($type == 'day'){ 
                $background[] = '#9589B3';
            } 
        }
    } else if(in_array($type,['week','month'])) {
        $from_date = ($type == 'week') ? date("Y-m-d",strtotime('monday this week')): date("Y-m-01");
        $to_date = ($type == 'week') ?  date("Y-m-d",strtotime("sunday this week")) : date("Y-m-t");
        $period = new DatePeriod( 
            new DateTime($from_date),
            new DateInterval('P1D'),
            new DateTime($to_date)  
        );
        foreach ($period as $key => $value) {   
            $date = $value->format('Y-m-d'); 
            $leadBean = "select count(*) as total_count from leads where DATE(date_entered) = '{$date}'";
            $createdLead = $db->query($leadBean);     
            $createdLeadList = $db->fetchByAssoc($createdLead); 
            if($createdLeadList['total_count'] != 0){  
                $totalLeadCount[] =  $createdLeadList['total_count']; 
                $dateRange[] = date('d-m',strtotime($date)); 
                if($type == 'week'){  
                    $background[] = '#778591';
                }
                if($type == 'month'){ 
                    $background[] = '#e6d5a5';
                } 
            } 
        }      
    } 
     
    $response = ['lable' => $type,'count' => $totalLeadCount,'range' => $dateRange,'background' => $background];   
    echo json_encode(['data' => $response]); die;  
}
?>