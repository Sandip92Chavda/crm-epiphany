<?php
// created: 2021-10-19 08:52:01
$dictionary["User"]["fields"]["users_tbl_area_1"] = array (
  'name' => 'users_tbl_area_1',
  'type' => 'link',
  'relationship' => 'users_tbl_area_1',
  'source' => 'non-db',
  'module' => 'tbl_Area',
  'bean_name' => 'tbl_Area',
  'side' => 'right',
  'vname' => 'LBL_USERS_TBL_AREA_1_FROM_TBL_AREA_TITLE',
);
