<?php
// created: 2021-05-27 15:04:46
$dictionary["tbl_Location"]["fields"]["tbl_location_tbl_area"] = array (
  'name' => 'tbl_location_tbl_area',
  'type' => 'link',
  'relationship' => 'tbl_location_tbl_area',
  'source' => 'non-db',
  'module' => 'tbl_Area',
  'bean_name' => 'tbl_Area',
  'side' => 'right',
  'vname' => 'LBL_TBL_LOCATION_TBL_AREA_FROM_TBL_AREA_TITLE',
);
