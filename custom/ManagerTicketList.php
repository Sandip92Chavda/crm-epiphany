<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once('include/MVC/View/views/view.list.php');
include_once('./include/database/DBManagerFactory.php');
class ManagerTicketList{
    public function __construct(){
        $this->db = \DBManagerFactory::getInstance();
    }
    function getTicketList(){
        global $current_user;
        $customerId = 0;
        if(isset($current_user->is_customer_c) && $current_user->is_customer_c === '0'){
            $sql = "SELECT acl_roles.name from acl_roles_users
                    JOIN acl_roles ON acl_roles.id = acl_roles_users.role_id
                     where `user_id` = '{$current_user->id}' and acl_roles_users.deleted = 0 ORDER BY acl_roles_users.date_modified DESC";
            $response = $this->db->query($sql);
            
            if($response->num_rows > 0) {
                $result = $this->db->fetchByAssoc($response); 
                if(isset($result) && !empty($result['name']) && $result['name'] == 'Manager' || $result['name'] == 'manager') {
                    /**
                     * check manager area location with customer
                     */
                    $managerAreaId = $this->getManagerArea($current_user->id);
                    if(isset($managerAreaId) && count($managerAreaId)>0){
                        $cuId =  $this->getCustomerId($managerAreaId);
                        if(isset($cuId) && count($cuId)>0){ 
                            $customerId = "'" . implode ( "', '", $cuId ) . "'";
                            return $customerId;
                        }  
                    }
                    return $customerId;
                }
                return $customerId;
            } 
            return $customerId;       
        }
        return $customerId; 
    }
    public function getManagerArea($userId){   
        $result = [];
        $sql = "SELECT users_tbl_area_1_c.users_tbl_area_1tbl_area_idb from users_tbl_area_1_c
                      where users_tbl_area_1users_ida = '{$userId}' and deleted != 1";
        $response = $this->db->query($sql);   
        if($response->num_rows > 0) {    
            while ($users = $this->db->fetchByAssoc($response)) { 
                $result[] = $users['users_tbl_area_1tbl_area_idb'];
            }    
            return $result;   
        }    
        return $result;           
    }
    public function getCustomerId($id){  
        $customerId = "'" . implode ( "', '", $id ) . "'";
        $result = [];
        $sql = "SELECT users_tbl_area_1_c.users_tbl_area_1users_ida from users_tbl_area_1_c
        where users_tbl_area_1tbl_area_idb in ({$customerId}) and deleted != 1 GROUP BY users_tbl_area_1users_ida";
        $response = $this->db->query($sql);   
        if($response->num_rows > 0) {
            while ($accounts = $this->db->fetchByAssoc($response)) {
                $result[] = $accounts['users_tbl_area_1users_ida'];
            }  
            return $result;  
        }   
    }
}
?>