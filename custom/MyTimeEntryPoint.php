<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
include_once('./include/database/DBManagerFactory.php');
include_once('./lib/custom/RegisterOssApi.php'); 
use BeanFactory;

Class MyTimeEntryPoint{
	public function __construct() {  
        $this->db = \DBManagerFactory::getInstance();
    } 
	public function getCustomerList(){
		$url = "http://192.168.0.26:8080/api/v1/customers";
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
        "authorization: Basic YWRtaW46YWRtaW5AMTIz",
        "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only! 
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($curlHandle, CURLOPT_CONNECTTIMEOUT, 0); 
        $resp = curl_exec($curl); 
        curl_close($curl); 
        if(!empty($resp)){  
            $data =  json_decode($resp, true);
            if(!empty($data['customerList']) && count($data['customerList'])>0){
                foreach($data['customerList'] as $key => $value){  
                    $this->addUserData($value); 
                }   
                echo "done"; 
            }   
        }   
	}
	public function addUserData($data) {
		$call = new RegisterOssApi(); 
        $custId = $call->getCustomerDetail($data["username"]);
        $customerAddress = $call->getCustomerAddress($custId);
        /** check username in our system */
        $sql = sprintf('SELECT * FROM users WHERE user_name = "%s"',$data["username"]); 
        $result = $this->db->query($sql);
        if(isset($result->num_rows) && $result->num_rows === 0) { 
        	$name = $data["username"]; 
            $bean = BeanFactory::newBean('Accounts');   //Create bean  using module name 
            $bean->name = $name;   //Populate bean fields
            $bean->firstname_c = $data["firstname"];   //Populate bean fields
            $bean->lastname_c = $data["lastname"];   //Populate bean fields
            $bean->phone_office  = $data["phone"];   //Populate bean fields
            $bean->addressstreet_c  = !empty($customerAddress[0]["address1"])?$customerAddress[0]["address1"]:'';
            $bean->pincode_c  = !empty($customerAddress[0]["pincode"])?$customerAddress[0]["pincode"]:''; 
            $bean->city_c = !empty($customerAddress[0]["city"])?$customerAddress[0]["city"]:1;
            $bean->state_c = !empty($customerAddress[0]["state_c"])?$customerAddress[0]["state_c"]:1;
            $bean->country_c = !empty($customerAddress[0]["country_c"])?$customerAddress[0]["country_c"]:1;
            $bean->assigned_user_id = 1;   //Populate bean fields 
            $bean->plantype_c =  !empty($data['planMappingPojoList'][0]['planId']) ? $data['planMappingPojoList'][0]['planId']:0;  
            $bean->emailAddress->addAddress($data['email'], true);
            $bean->save();   //Save   
            
            $user = BeanFactory::newBean('Users')->retrieve_by_string_fields(array('user
                _name' => $name ));
            $user->phone_mobile  = $data["phone"];  
            $user->emailAddress->addAddress($data['email']); 
            $user->address_street = !empty($customerAddress[0]["address1"])?$customerAddress[0]["address1"]:''; 
            $user->address_postalcode = !empty($customerAddress[0]["pincode"])?$customerAddress[0]["pincode"]:''; 
            $user->city_c = !empty($customerAddress[0]["city"])?$customerAddress[0]["city"]:1;
            $user->state_c = !empty($customerAddress[0]["state_c"])?$customerAddress[0]["state_c"]:1;
            $user->country_c = !empty($customerAddress[0]["country_c"])?$customerAddress[0]["country_c"]:1; 
            $user->save();   
                 
        }	
	}
}
/** call customer api */
$call = new MyTimeEntryPoint();
$call->getCustomerList(); 