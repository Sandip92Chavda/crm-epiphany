<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry
Point');
include_once('./include/database/DBManagerFactory.php');
require_once('include/SugarPHPMailer.php');
include_once('./lib/custom/RegisterOssApi.php');  
require_once './include/SugarEmailAddress/SugarEmailAddress.php';  
include_once('./lib/custom/CommonService.php');   
use BeanFactory;
use TimeDate; 
class Accounts_Save  
{ 
    private $db;
    protected $mail;
    function __construct() {  
        $this->mail = new SugarPHPMailer();
        $this->common = new CommonService();  
    } 
    function QueueJob(&$bean, $event, $arguments)    
    { 
         /**Time */
        $time_now = TimeDate::getInstance()->nowDb(); 
        /**initiate db connection */
        $db = \DBManagerFactory::getInstance();
        
        /**generate password and accountnumber*/ 
        $password_text = $this->common->generatePassword(); 
        // $accNumber = $this->common->generateAccountNumber();

        /** check user is exist */
        $userSql = sprintf('SELECT * FROM users WHERE user_name = "%s"',
            $bean->name);     
        $userResults =  $db->query($userSql); 

        if(isset($bean->firstname_c) && !empty($bean->firstname_c)){  
            if($userResults->num_rows == 0) {   
                // $password = password_hash(strtolower(md5($bean->name)), PASSWORD_DEFAULT);  
                $password = password_hash(strtolower(md5($password_text)), PASSWORD_DEFAULT);  
              
                $user = BeanFactory::newBean('Users');   
                $user->user_name =  $bean->name;
                $user->first_name =  $bean->firstname_c;
                $user->last_name =  $bean->lastname_c;
                $user->status = 'Active';
                $user->employee_status = 'Active'; 
                $user->phone_mobile  = $bean->phone_office;
                $user->user_hash = $password; 
                $user->passwordtext_c = $password_text; 
                $user->emailAddress->addAddress($bean->email1, true);
                $user->is_customer_c = 1; /* 1 represent the customer */
                $user->address_street = $bean->addressstreet_c; 
                $user->address_postalcode = $bean->pincode_c; 
                $user->city_c = $bean->city_c;
                $user->state_c = $bean->state_c;
                $user->country_c = $bean->country_c;    
                $user->save();    
                 /** send mail */ 
                if(!empty($bean->email1)){  
                    $emailObj = new Email();
                    $defaults = $emailObj->getSystemDefaultEmail();
                    $description = "Your password is ".$password_text."";
                    $this->mail->setMailerForSystem();
                    $this->mail->From = "{$defaults['email']}";
                    $this->mail->FromName = "{$defaults['name']}";
                    $this->mail->Subject = "Customer Registration";
                    $this->mail->Body = "{$description}";
                    $this->mail->prepForOutbound();
                    $this->mail->AddAddress($bean->email1);
                    @$this->mail->Send();   
    
                }
                /*-- Add Oauth token --*/ 
                $client = BeanFactory::newBean('OAuth2Clients');
                $client->name = $bean->name;
                // $client->secret = hash('sha256', $bean->name);
                $client->secret = hash('sha256', $password_text);
                $client->allowed_grant_type = 'client_credentials';
                $client->duration_value = 60;
                $client->duration_amount = 1;
                $client->duration_unit = 'minute'; 
                $client->assigned_user_id =  $user->id; 
                $client->save(); 
                
                /** update account number */
                // $acSql = sprintf("UPDATE accounts_cstm SET accountnumber_c = '%s' WHERE id_c = '%s'",$accNumber,$bean->id);   
                // $db->query($acSql); 
                // echo $acSql;die;
                
                /** add user area */
                $guid=create_guid();
                $insertUserArea = "INSERT INTO users_tbl_area_1_c (id,date_modified,users_tbl_area_1users_ida, users_tbl_area_1tbl_area_idb) VALUES('".$guid."','".$time_now."','".$user->id."','".$bean->accounts_tbl_area_1tbl_area_idb."') ";
                $db->query($insertUserArea);

                /** add detail to oss/bss */ 
                // $bean->accountnumber_c =  $accNumber;  
                $bean->passwordtext_c =  $password_text;

                /** add detail to oss/bss */
                $call = new RegisterOssApi();
                $call->addDataToOss($bean);      
            }else{  
                /** update cutomer detail */

                $call = new RegisterOssApi(); 
                $custId = $call->getCustomerDetail($bean->name);

                    $processBean = BeanFactory::getBean('Users')->retrieve_by_string_fields(array('user
                    _name' => $bean->name));  
                // print_r($custId);print_r($processBean);die; 
                    $processBean->first_name =  $bean->firstname_c;
                    $processBean->last_name =  $bean->lastname_c;
                    $processBean->status = 'Active';
                    $processBean->employee_status = 'Active'; 
                    $processBean->phone_mobile  = $bean->phone_office; 
                    $processBean->is_customer_c = 1; /* 1 represent the customer */
                    $processBean->address_street = $bean->addressstreet_c; 
                    $processBean->address_postalcode = $bean->pincode_c; 
                    $processBean->city_c = $bean->city_c;
                    $processBean->state_c = $bean->state_c;
                    $processBean->country_c = $bean->country_c;   
                        $emailDetail = $bean->email1;
                    if(!empty($bean->email1)){ 
                        $emailSql = sprintf('SELECT * FROM email_addresses WHERE email_address = "%s"',
                            $bean->email1);    
                        $emailResults =  $db->query($emailSql);
                        $emailData = $db->fetchByAssoc($emailResults);
                        if(!empty($emailData['email_address'])){
                            $emaildSql = sprintf('DELETE FROM email_addresses WHERE email_address = "%s"',
                            $emailDetail);
                            $db->query($emaildSql);
                            $emailDeSql = sprintf('DELETE FROM email_addr_bean_rel WHERE email_address_id = "%s"',
                            $emailData['id']);
                            $db->query($emailDeSql);
                        }
    
                    } 
                    $processBean->save();
    
                    /** update email address **/
                    $sea = new \SugarEmailAddress;      
                    $sea->addAddress($emailDetail, true);
                    $sea->save($processBean->id, "Users");
    
                    $seaAc = new \SugarEmailAddress;      
                    $seaAc->addAddress($emailDetail, true);
                    $seaAc->save($bean->id, "Accounts"); 

                    /** Delete old area entry */
                    $areaSql = sprintf('DELETE FROM users_tbl_area_1_c WHERE users_tbl_area_1users_ida = "%s"',
                    $processBean->id);  
                    $db->query($areaSql);
                    /** update user area */
                    $guid=create_guid();
                    $insertUserArea = "INSERT INTO users_tbl_area_1_c (id,date_modified,users_tbl_area_1users_ida, users_tbl_area_1tbl_area_idb) VALUES('".$guid."','".$time_now."','".$processBean->id."','".$bean->accounts_tbl_area_1tbl_area_idb."') ";
                    $db->query($insertUserArea);
                    /*-- get password --*/ 
                    $bean->passwordtext_c =  $processBean->passwordtext_c;  
                    $bean->accountnumber_c =  $bean->accountnumber_c;    
                if(!empty($custId)){   
                     $call->updateDataToOss($bean,$custId);
                }else{
                    /*--add data --*/
                     $call->addDataToOss($bean);   
                }
                    // /*-----update email ---*/ 
                    // if(!empty($bean->email1)) {  
                    //     $emailObj = new Email();
                    //     $defaults = $emailObj->getSystemDefaultEmail();
                    //     $description = "Your detail has been updated.";
                    //     $this->mail->setMailerForSystem();
                    //     $this->mail->From = "{$defaults['email']}";
                    //     $this->mail->FromName = "{$defaults['name']}";
                    //     $this->mail->Subject = "Customer Update";
                    //     $this->mail->Body = "{$description}";
                    //     $this->mail->prepForOutbound();
                    //     $this->mail->AddAddress($bean->email1);
                    //     @$this->mail->Send();   
        
                    // }
            } 
        } 
            
    }

    //send email notification 
    public function sendEmailNotification(&$bean, $event, $arguments){ 
        // echo "<pre>";print_r($bean);die;
        /**initiate db connection */
        $db = \DBManagerFactory::getInstance(); 
        /** check user is exist */
        $userSql = sprintf('SELECT * FROM accounts_cstm WHERE id_c = "%s"',
            $bean->id);      
        $accountResults =  $db->query($userSql); 
        $result = $db->fetchByAssoc($accountResults);
        if(!empty($result)){ 
            $description = '';
            $description = "Your account detail has been updated. \n";
            if($result['firstname_c'] != $bean->firstname_c){
                $description .= 'First Name ,';
            }
            if($result['lastname_c'] != $bean->lastname_c){
                $description .= 'Last Name , ';
            }
            if($result['customer_type_list_c'] != $bean->customer_type_list_c){
                $description .= 'Customer Type , ';
            }
            if($result['country_c'] != $bean->country_c){ 
                $description .= 'Country , ';
            }
            if($result['city_c'] != $bean->city_c){
                $description .= 'City ,';
            }
            if($result['state_c'] != $bean->state_c){
                $description .= 'State , ';
            }
            if($result['pincode_c'] != $bean->pincode_c){
                $description .= 'Pincode , ';
            }
            if($result['invoiceoption_c'] != $bean->invoiceoption_c){
                $description .= 'Invoice Option , ';
            }
            if($result['billentity_c'] != $bean->billentity_c){
                $description .= 'Bill Entity Name , ';
            }
            if($result['billday_c'] != $bean->billday_c){
                $description .= 'Bill Day , ';
            }
            if($result['plantype_c'] != $bean->plantype_c){
                $description .= 'Plan , ';
            }
            if($result['addressstreet_c'] != $bean->addressstreet_c){
                $description .= 'Address , ';
            } 
            /** check email is exist */
            $emailSql = sprintf('SELECT * FROM email_addresses WHERE email_address = "%s" AND confirm_opt_in = "%s"',
            $bean->email1,'confirmed-opt-in');  
            $emailResults =  $db->query($emailSql); 
            $emailResult = $db->fetchByAssoc($emailResults);      
            if(empty($emailResult)){
                $description .= 'Email  ';
            }
            $description .= ' are updated';
            /*-----update email ---*/ 
            if(!empty($bean->email1)) {    
                $emailObj = new Email();
                $defaults = $emailObj->getSystemDefaultEmail();
                // $description = "Your detail has been updated. Your new password is ".$bean->name."";
                $this->mail->setMailerForSystem();
                $this->mail->From = "{$defaults['email']}";
                $this->mail->FromName = "{$defaults['name']}";
                $this->mail->Subject = "Customer Update";
                $this->mail->Body = "{$description}";
                $this->mail->prepForOutbound();
                $this->mail->AddAddress($bean->email1);
                @$this->mail->Send();   
            }
        } 
    }
}
?>