<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once('include/MVC/View/views/view.detail.php');
include_once('./include/database/DBManagerFactory.php');
include_once('./lib/custom/CallOlaApi.php');  
use BeanFactory;
class CustomAccountsViewDetail extends ViewDetail {

    public function display(){
        global $sugar_config;
        global $mod_strings;
        // $this->ss->assign("CURRENTAGE", '1211111'); 
        /**initiate db connection */
        $db = \DBManagerFactory::getInstance();
        // $accountSql = sprintf('SELECT * FROM accounts_cstm WHERE accountnumber_c = "%s"',
        // $this->bean->accountnumber_c);   
        $accountSql = sprintf('SELECT * FROM accounts WHERE name = "%s"',
        $this->bean->name);   
        $accResults =  $db->query($accountSql); 
        $accData = $db->fetchByAssoc($accResults);
        $call = new \CallOlaApi();   
        if(!empty($accData)){   
            // $account_id = $accData['id_c'];
            $account_id = $accData['id'];
            $user_name = $this->bean->name;
            $custData = $call->getCustomerDetail($user_name);
            if(!empty($custData[0]) && !empty($account_id)){ 
                $this->updateData($custData[0],$account_id);
            } 
        }  
        $dataUsage = $call->getDataUage($this->bean->name);
        $sugar_smarty = new Sugar_Smarty();    
        $sugar_smarty->assign('download',$dataUsage['download']);  
        $sugar_smarty->assign('upload',$dataUsage['upload']);  
        parent::display();  
        echo $sugar_smarty->fetch('custom/themes/account.tpl');  
    }
    public function updateData($data,$account_id){ 
        $db = \DBManagerFactory::getInstance();
        $sql = sprintf('
                UPDATE accounts_cstm SET 
                 invoiceoption_c = "%s",
                parentcustomer_c = "%s",
                billentity_c = "%s",
                authprotocol_c = "%s",
                ipaddress_c = "%s",
                billday_c = "%s",
                building_c = "%s",
                cablelength_c = "%s",
                networkfilterid_c = "%s",
                ipmode_c = "%s",
                ippool_c = "%s",
                latitude_c = "%s",
                longitude_c = "%s",
                macbinding_c = "%s",
                nasportbinding_c = "%s",
                nasportid_c = "%s",
                node_c = "%s",
                pop_c = "%s",
                switch_c = "%s",
                switchport_c = "%s",
                accountnumber_c = "%s"
                WHERE id_c = "%s"', 
                $data["invoiceOption"],
                $data["parentCustomersId"],
                $data["billentityname"],
                $data["authProtocol"],
                $data["allowedIPAddress"],
                $data["billday"],
                $data["building"],
                $data["cableLength"],
                $data["filterId"],
                $data["ipMode"],
                $data["ipPool"],
                $data["latitude"],
                $data["longitude"],
                $data["macBinding"],
                $data["nasportBind"],
                $data["nasportId"],
                $data["node"],
                $data["pop"],
                $data["switchName"],
                $data["switchPortNo"],
                $data['acctno'],
                $account_id
    ); 
    
    // print_r($sql);die;  
    $db->query($sql);
    }
}
?>