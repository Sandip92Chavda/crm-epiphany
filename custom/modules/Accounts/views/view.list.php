<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once('include/MVC/View/views/view.list.php');
include_once('./include/database/DBManagerFactory.php');
    // name of class match module 
session_start();
class AccountsViewList extends ViewList{  
    // where clause that will be inserted in sql query
    // var $where = "source_c not like 'Social media'";   //this is my where clause 
    //var $where = ""; 
    public function __construct(){
        $this->db = \DBManagerFactory::getInstance();
    }
    function listViewProcess() {    
        global $current_user;
        $role = new ACLRole();
        $roles = $role->getUserRoleNames($current_user->id);

        // $this->where = " cases.assigned_user_id IS NULL";    
        $this->processSearchForm();
        $this->lv->searchColumns = $this->searchForm->searchColumns;
        if (!$this->headers) {  
            return;
        }
        if (empty($_REQUEST['search_form_only']) || $_REQUEST['search_form_only'] == false) {
            $this->lv->ss->assign("SEARCH", true);
            $this->lv->ss->assign('savedSearchData', $this->searchForm->getSavedSearchData());
            
            if (!empty($this->where)) {   
                // $this->where .= " AND";  
            }
            $last7Day = date('Y-m-d', strtotime('today - 7 days'));
            $currentDay = date('Y-m-d');       
            if(isset($_SESSION['filter']) && $_SESSION['filter'] === 'new_customer'){ 
                $this->where .= " DATE(accounts.date_entered) >= '$last7Day' and DATE(accounts.date_entered) <= '$currentDay'";      
            } 
            if(in_array('manager',$roles)){
                if(isset($_SESSION['filter'])) { 
                    // $this->where .= " and accounts.assigned_user_id = '{$current_user->id}'"; 
                }else{
                    // $this->where .= " accounts.assigned_user_id = '{$current_user->id}'"; 
                }
            }     
            $this->lv->setup($this->seed, 'include/ListView/ListViewGeneric.tpl', $this->where, $this->params); 
            $savedSearchName = empty($_REQUEST['saved_search_select_name']) ? '' : (' - ' . $_REQUEST['saved_search_select_name']);
            echo $this->lv->display();
        } 
    } 
    public function preDisplay(){ 
        $squery= new StoreQuery();
        $squery->query=array('module'=>'Accounts','searchFormTab'=>'advanced_search','query'=>'true','action'=>'index');
        $squery->SaveQuery('Accounts');   
        if($_REQUEST['filter'] == 'new_customer'){   
            $_SESSION['filter'] = 'new_customer';
        } 
        if($_REQUEST['parentTab'] == 'All' || $_REQUEST['parentTab'] == 'online_customer' || $_REQUEST['filter'] == 'online_customer'){  
            unset($_SESSION['filter']);   
        }     
        parent::preDisplay();
    } 
} 