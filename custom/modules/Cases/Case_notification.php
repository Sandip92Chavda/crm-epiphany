<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

include_once('./include/database/DBManagerFactory.php');
require_once './lib/custom/PushNotification.php';
require_once('include/SugarPHPMailer.php');
require_once './include/SugarEmailAddress/SugarEmailAddress.php';
use BeanFactory;

class Case_notification 
{
    /**
     * @var PushNotification
     */
    protected $pushNotification;

    public function __construct(){
        $this->mail = new SugarPHPMailer();
        $this->pushNotification = new \PushNotification();
    }

    public function SendNotification(&$bean, $event, $arguments)   
    {   $createByrecords = [];
        // $processBean = BeanFactory::getBean('Users')->retrieve_by_string_fields(array('id' => $bean->created_by));  
        
         $db = \DBManagerFactory::getInstance();
         // echo "<pre>";print_r($bean);die;
         $createEmailAddress = $this->getEmailAddress($bean->created_by);
        
         /** send mail */ 
         if(!empty($createEmailAddress) && ($bean->assigned_user_id == null || $bean->assigned_user_id == '')){

            $emailObj = new Email();
            $defaults = $emailObj->getSystemDefaultEmail();
            $description = "Ticket has been created ";
            $description .= " Subject : ".$bean->name;
            $this->mail->setMailerForSystem();
            $this->mail->From = "{$defaults['email']}";
            $this->mail->FromName = "{$defaults['name']}";
            $this->mail->Subject = "Ticket Creation";
            $this->mail->Body = "{$description}";
            $this->mail->prepForOutbound();
            $this->mail->AddAddress("{$createEmailAddress}");
            @$this->mail->Send(); 
         }
         // echo $createEmailAddress;die;
          
         $sql = sprintf('select * from apns_devices where `id` = "%s"',$bean->assigned_user_id); 
         $result = $db->query($sql);

         if(!empty($result) && $result->num_rows > 0){
            $data = $db->fetchByAssoc($result);

            // echo"<pre>";print_r($data);die;
            $message = 'Ticket assigned';
            $messageType = "push";
            $receiverId = $data['id'];
            $id = "{$receiverId}";
            $customArr['push_type']= 'Admin';
            $deliver_by_crone = 'Yes';
            $status = "sent";
            $ticket_id = $bean->id;

            $receiveEmailAddress = $this->getEmailAddress($bean->assigned_user_id);

            if(!empty($receiveEmailAddress)){

                // $emailObj = new Email();
                // $defaults = $emailObj->getSystemDefaultEmail();
                // $description = "Ticket has been assigened";
                // $description .= " Subject : ".$bean->name;
                // $this->mail->setMailerForSystem();
                // $this->mail->From = "{$defaults['email']}";
                // $this->mail->FromName = "{$defaults['name']}";
                // $this->mail->Subject = "Ticket Assigend";
                // $this->mail->Body = "{$description}";
                // $this->mail->prepForOutbound();
                // $this->mail->AddAddress("{$receiveEmailAddress}");
                // @$this->mail->Send(); 
             }    
         

             $respo = \PushNotification::sendPushMessage($message,$messageType, $id, $receiverId,
             $customArr, $deliver_by_crone, $status,$ticket_id); 
             
             $query = sprintf('select * from apns_devices where `id` = "%s"',$bean->created_by);
            //  print_r($query);die;
             $results = $db->query($query);
             $datas = $db->fetchByAssoc($results);
             $receiverId = $datas['id'];
             $id = "{$receiverId}";
            
             $respo = \PushNotification::sendPushMessage($message,$messageType, $id, $receiverId,
             $customArr, $deliver_by_crone, $status,$ticket_id); 

        }
   
    }
    public function getEmailAddress($id = ''){
         $db = \DBManagerFactory::getInstance();
         $createBySql = sprintf('select * from email_addr_bean_rel where `bean_id` = "%s" AND `primary_address` = "%s" AND deleted = "%s" ',$id,1,0);  
        $createByResult = $db->query($createBySql);
        while ( $row = $db->fetchByAssoc($createByResult) ) {
            $createByrecords[] = $row;
        }
        if(!empty($createByrecords)){
            $sql = sprintf('select * from email_addresses where `id` = "%s"',$createByrecords[0]['email_address_id']);  
            $result = $db->query($sql); 
            while ( $row1 = $db->fetchByAssoc($result) ) {
                $emailecords[] = $row1;
            }
        }
        return !empty($emailecords[0]) ? $emailecords[0]['email_address'] : null;
    }
}    
