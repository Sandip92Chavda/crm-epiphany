<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2021-07-21 22:32:49
$dictionary['Case']['fields']['type']['len']=100;
$dictionary['Case']['fields']['type']['inline_edit']=true;
$dictionary['Case']['fields']['type']['comments']='The type of issue (ex: issue, feature)';
$dictionary['Case']['fields']['type']['merge_filter']='disabled';

 

 // created: 2021-04-29 06:41:38
$dictionary['Case']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2021-09-06 13:02:36
$dictionary['Case']['fields']['state']['inline_edit']=true;
$dictionary['Case']['fields']['state']['comments']='The state of the case (i.e. open/closed)';

 

 // created: 2021-06-07 03:27:17
$dictionary['Case']['fields']['rating_c']['inline_edit']='';
$dictionary['Case']['fields']['rating_c']['labelValue']='Rating';

 

 // created: 2021-09-15 05:23:14
$dictionary['Case']['fields']['subtype_c']['inline_edit']='';
$dictionary['Case']['fields']['subtype_c']['labelValue']='subtype';

 

 // created: 2021-04-29 06:41:38
$dictionary['Case']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2021-04-29 06:41:38
$dictionary['Case']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2021-06-07 03:27:55
$dictionary['Case']['fields']['feedback_c']['inline_edit']='';
$dictionary['Case']['fields']['feedback_c']['labelValue']='Feedback';

 

 // created: 2021-04-29 06:41:38
$dictionary['Case']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 
?>