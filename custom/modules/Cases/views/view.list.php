<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once('include/MVC/View/views/view.list.php');
include_once('./include/database/DBManagerFactory.php');
    // name of class match module
    session_start();
class CasesViewList extends ViewList{ 
 
    public function __construct(){
        $this->db = \DBManagerFactory::getInstance();
    }
    function listViewProcess() {    
        global $current_user;
        if(isset($current_user->is_customer_c) && $current_user->is_customer_c === '0'){
          
            $sql = "SELECT acl_roles.name from acl_roles_users
                    JOIN acl_roles ON acl_roles.id = acl_roles_users.role_id
                     where `user_id` = '{$current_user->id}' and acl_roles_users.deleted = 0 ORDER BY acl_roles_users.date_modified DESC";
            $response = $this->db->query($sql);
            if($response->num_rows > 0) {
                $result = $this->db->fetchByAssoc($response); 

                if(isset($result) && !empty($result['name']) && ($result['name'] == 'Manager' || $result['name'] == 'manager')){

                    $this->processSearchForm();
                    $this->lv->searchColumns = $this->searchForm->searchColumns;
                    if (!$this->headers) {  
                        return;
                    }
                    if (empty($_REQUEST['search_form_only']) || $_REQUEST['search_form_only'] == false) {
                        $this->lv->ss->assign("SEARCH", true);
                        $this->lv->ss->assign('savedSearchData', $this->searchForm->getSavedSearchData());
                        /**
                         * check manager area location with customer
                         */
                        $managerAreaId = $this->getManagerArea($current_user->id);
                       
                        $customerId = 0;
                        if($managerAreaId){
                            $cuId =  $this->getCustomerId($managerAreaId);
                            
                            if(isset($cuId) && count($cuId)>0){
                                $customerId = "'" . implode ( "', '", $cuId ) . "'";
                            }  
                            // echo "<pre>";print_r($cuId);print_r($customerId);die;
                        }
                        if (!empty($this->where)) {   
                            // $this->where .= " AND"; 
                        }   
                       
                        if($customerId === 0){
                            $this->where .= " cases.created_by = '0'";
                        }  else{
                            $this->where .= " cases.created_by in ({$customerId}) ";
                        } 
                      
                        if(isset($_SESSION['filter']) && $_SESSION['filter'] === 'new'){
                            $this->params['custom_where'] = " AND (cases.assigned_user_id IS NULL OR cases.assigned_user_id = '')";   
                        }
                        if(isset($_SESSION['filter']) && $_SESSION['filter'] === 'working'){
                            $this->params['custom_where'] = " AND cases.assigned_user_id IS NOT NULL AND cases.state != 'Closed' ";  
                        }
                        $this->lv->setup($this->seed, 'include/ListView/ListViewGeneric.tpl', $this->where, $this->params); 
                        $savedSearchName = empty($_REQUEST['saved_search_select_name']) ? '' : (' - ' . $_REQUEST['saved_search_select_name']);
                        echo $this->lv->display();
                    }
                }else{
                    parent::listViewProcess();
                }
            } else{
                parent::listViewProcess();
            }
        }else{  
            
            if(isset($_SESSION['filter']) && $_SESSION['filter'] === 'new'){
                $this->params['custom_where'] = " AND (cases.assigned_user_id IS NULL OR cases.assigned_user_id = '')";   
            }
            if(isset($_SESSION['filter']) && $_SESSION['filter'] === 'working'){
                $this->params['custom_where'] = " AND cases.assigned_user_id IS NOT NULL AND cases.state != 'Closed' ";  
            }  
            parent::listViewProcess(); 
        }
       
        // $this->params['custom_where'] = ' AND cases.id in ("6f43cf96-5db9-10d3-a267-614db50c1328") ';
         
        // parent::listViewProcess(); 
        
    }
    public function preDisplay(){ 
        $squery= new StoreQuery();
        $squery->query=array('module'=>'Cases','searchFormTab'=>'advanced_search','query'=>'true','action'=>'index');
        $squery->SaveQuery('Cases');
        if($_REQUEST['filter'] == 'new'){
            $_SESSION['filter'] = 'new';
        }
        if($_REQUEST['filter'] == 'working'){ 
            $_SESSION['filter'] = 'working';
        }
        if($_REQUEST['parentTab'] == 'All'){
            unset($_SESSION['filter']); 
        }
        parent::preDisplay();
    }
    public function getManagerArea($userId){
        $sql = "SELECT users_tbl_area_1_c.users_tbl_area_1tbl_area_idb from users_tbl_area_1_c
                      where users_tbl_area_1users_ida = '{$userId}' and deleted != '1'";
        $response = $this->db->query($sql);
        if($response->num_rows > 0) {
            while ($area = $this->db->fetchByAssoc($response)) {
                $result[] = $area['users_tbl_area_1tbl_area_idb'];
            }  
            return $result;
        }   
        return false;           
    }
    public function getCustomerId($id){ 
         // echo "<pre>";print_r($id);die;
        $areaId = "'" . implode ( "', '", $id ) . "'";
        $result = [];
        // $sql = "SELECT users_tbl_area_1_c.users_tbl_area_1users_ida from users_tbl_area_1_c
        // where `users_tbl_area_1tbl_area_idb` = '{$id}' and deleted != 1";
        $sql = "SELECT users_tbl_area_1_c.users_tbl_area_1users_ida from users_tbl_area_1_c
        where users_tbl_area_1tbl_area_idb in ({$areaId}) and deleted != 1 GROUP BY users_tbl_area_1users_ida";
       
        $response = $this->db->query($sql);   
        if($response->num_rows > 0) {
            while ($accounts = $this->db->fetchByAssoc($response)) {
                $result[] = $accounts['users_tbl_area_1users_ida'];
            }  
            // print_r($result);die;
            return $result;   
        }    
    }
}