<?php
// created: 2021-10-13 10:58:46
$mod_strings = array (
  'LBL_INQUIRYFOR' => 'InquiryFor',
  'LBL_PLANTYPE' => 'PlanType',
  'LBL_ACCOUNTNUMBER_C' => 'AccountNumber',
  'LBL_EMAIL_ADDRESS' => 'Email Address:',
  'LBL_PINCODE' => 'Pincode',
  'LBL_ADDRESSSTREET' => 'AddressStreet',
  'LBL_COUNTRY' => 'Country',
  'LBL_STATE' => 'State',
  'LBL_CITY' => 'City',
);