<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
require_once('include/MVC/View/views/view.list.php');
include_once('./include/database/DBManagerFactory.php');
    // name of class match module 
session_start();
class LeadsViewList extends ViewList{  
    // where clause that will be inserted in sql query
    // var $where = "source_c not like 'Social media'";   //this is my where clause 
    //var $where = ""; 
    public function __construct(){
        $this->db = \DBManagerFactory::getInstance();
    }
    function listViewProcess() {    
        global $current_user;
        // $this->where = " cases.assigned_user_id IS NULL";    
                $this->processSearchForm();
                $this->lv->searchColumns = $this->searchForm->searchColumns;
                if (!$this->headers) {  
                    return;
                }
                if (empty($_REQUEST['search_form_only']) || $_REQUEST['search_form_only'] == false) {
                    $this->lv->ss->assign("SEARCH", true);
                    $this->lv->ss->assign('savedSearchData', $this->searchForm->getSavedSearchData());
                    
                    if (!empty($this->where)) {   
                        // $this->where .= " AND"; 
                    }
                          
                    if(isset($_SESSION['filter']) && $_SESSION['filter'] === 'new_lead'){  
                        $this->where .= " (leads.assigned_user_id IS NULL OR leads.assigned_user_id = '') and leads.converted = 0";       
                    }
                    if(isset($_SESSION['filter']) && $_SESSION['filter'] === 'active_lead'){  
                        $this->where .= " leads.converted = 0 AND  (leads.assigned_user_id IS NOT NULL OR leads.assigned_user_id != '')";       
                    }      
                    // echo $this->where;die;
                    $this->lv->setup($this->seed, 'include/ListView/ListViewGeneric.tpl', $this->where, $this->params); 
                    $savedSearchName = empty($_REQUEST['saved_search_select_name']) ? '' : (' - ' . $_REQUEST['saved_search_select_name']);
                    echo $this->lv->display();
                } 
    }
    public function preDisplay(){
        $squery= new StoreQuery();
        $squery->query=array('module'=>'Leads','searchFormTab'=>'advanced_search','query'=>'true','action'=>'index');
        $squery->SaveQuery('Leads'); 
        if($_REQUEST['filter'] == 'new_lead' || $_REQUEST['filter'] == 'active_lead'){
            unset($_SESSION['filter']);
            $_SESSION['filter'] = $_REQUEST['filter']; 
        } 
        if($_REQUEST['parentTab'] == 'All' || $_REQUEST['parentTab'] == 'online_customer'){
            unset($_SESSION['filter']);
        } 
        parent::preDisplay();
    } 
}