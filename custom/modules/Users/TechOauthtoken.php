<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry
Point');
include_once('./include/database/DBManagerFactory.php');
include_once('./lib/custom/CommonService.php'); 
require_once './include/SugarEmailAddress/SugarEmailAddress.php';   
require_once('include/SugarPHPMailer.php');
use BeanFactory;
class TechOauthtoken extends CommonService 
{ 
    function __construct() {   
        $this->mail = new SugarPHPMailer(); 
    } 
    /**
     * Check the customer type 
     * @param $bean
     *
     */
    function updateToken(&$bean, $event, $arguments)       
    { 
        if(!empty($bean->is_customer_c) || $bean->is_customer_c == 0) {   
            $processBean = BeanFactory::getBean('OAuth2Clients')->retrieve_by_string_fields(array('name' => $bean->user_name));
            $userBean = BeanFactory::getBean('Users')->retrieve_by_string_fields(array('user_name' => $bean->user_name));
            if($userBean->passwordtext_c == ''){ 
                
                /**generate random password */ 
                $password_text = $this->generatePassword();    
                /*-- Add Oauth token --*/ 
                if(!isset($processBean->name)){
                    $client = BeanFactory::newBean('OAuth2Clients');
                   $client->name = $bean->user_name;
                   $client->secret = hash('sha256', $password_text);    
                   $client->allowed_grant_type = 'client_credentials';
                   $client->duration_value = 60;
                   $client->duration_amount = 1;
                   $client->duration_unit = 'minute'; 
                   $client->assigned_user_id =  $bean->id;  
                   $client->save();  
                }
                  
                   /** update password string into user cutm table */ 
                   /**initiate db connection */   
                   $db = \DBManagerFactory::getInstance();    
                    // echo "<pre>";print_r($db);die;  
                   $updatedPassword = password_hash(strtolower(md5($password_text)), PASSWORD_DEFAULT);  
                   
                   $acSql = sprintf("UPDATE users_cstm SET  passwordtext_c = '%s' WHERE id_c = '%s'",$password_text,$bean->id);   
                   $db->query($acSql);  


                   $userSql = sprintf("UPDATE users SET  user_hash = '%s' WHERE id = '%s'",$updatedPassword,$bean->id); 
                   $db->query($userSql);    
                    if(!isset($processBean->name)){  
                   $emailObj = new Email();
                    $defaults = $emailObj->getSystemDefaultEmail();
                    $description = "Your password is ".$password_text."";
                    $this->mail->setMailerForSystem();
                    $this->mail->From = "{$defaults['email']}";
                    $this->mail->FromName = "{$defaults['name']}";
                    $this->mail->Subject = "User Registration";
                    $this->mail->Body = "{$description}";
                    $this->mail->prepForOutbound();
                    $this->mail->AddAddress($bean->email1);
                    @$this->mail->Send();
                }

            } 
        } 
    }

}