<?php
// created: 2021-06-25 06:21:25
$mod_strings = array (
  'LBL_COORDINATES' => 'Coordinates',
  'LNK_NEW_RECORD' => 'Create Areas',
  'LNK_LIST' => 'View Areas',
  'LBL_LIST_FORM_TITLE' => 'Areas List',
  'LBL_SEARCH_FORM_TITLE' => 'Search Areas',
  'LBL_HOMEPAGE_TITLE' => 'My Areas',
);