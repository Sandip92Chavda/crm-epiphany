<?php
$module_name = 'tbl_Area';
$viewdefs [$module_name] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 'FIND_DUPLICATES',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
        ),
        1 => 
        array (
          0 => 'description',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'pincode',
            'label' => 'LBL_PINCODE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'officeaddress',
            'studio' => 'visible',
            'label' => 'LBL_OFFICEADDRESS',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'tbl_location_tbl_area_name',
          ),
          1 => '',
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'leads_tbl_area_1_name',
          ),
          1 => 
          array (
            'name' => 'accounts_tbl_area_1_name',
          ),
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'users_tbl_area_1_name',
          ),
        ),
      ),
    ),
  ),
);
;
?>
