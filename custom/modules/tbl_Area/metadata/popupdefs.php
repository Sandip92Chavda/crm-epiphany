<?php
$popupMeta = array (
    'moduleMain' => 'tbl_Area',
    'varName' => 'tbl_Area',
    'orderBy' => 'tbl_area.name',
    'whereClauses' => array (
  'name' => 'tbl_area.name',
  'tbl_location_tbl_area_name' => 'tbl_area.tbl_location_tbl_area_name',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'tbl_location_tbl_area_name',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'tbl_location_tbl_area_name' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_TBL_LOCATION_TBL_AREA_FROM_TBL_LOCATION_TITLE',
    'id' => 'TBL_LOCATION_TBL_AREATBL_LOCATION_IDA',
    'width' => '10%',
    'name' => 'tbl_location_tbl_area_name',
  ),
),
);
