<!DOCTYPE html>  
        <html>
        <body>
        {literal}       
        <style>  
        canvas{
            width:1150px !important;
            height:520px !important; 
        } 
        </style>
        {/literal}       
        <div class="chart-view" style="background:#FFFFFF">
        <canvas id="myChart" ></canvas> 
        </div>
        {literal}       
          <script type="text/javascript">
       const data = { 
          labels: [
            'Download ({/literal}{$download}{literal} MB)',
            'Upload ({/literal}{$upload}{literal} MB)',
          ], 
          datasets: [{
                label: 'My First Dataset',
                data: [{/literal}{$download}{literal}, {/literal}{$upload}{literal}],
                backgroundColor: [
                  'rgb(255, 99, 132)',
                  'rgb(54, 162, 235)',
                ],
                hoverOffset: 4,
              }],
        };  
        new Chart("myChart", { 
                type: "doughnut",
                data: data,
                options: {
                  title: {
                    display: true,
                    text: 'Data Usage',
                    fontSize:35
                  },
                  legend: {
                      display: true,
                      labels: {
                          fontSize: 20
                      }
                  }
                }
            });
      
        </script>
        {/literal}
        </body>
        </html>