{literal}
<style>
   .panel-body {
    height: 60px;
   }
   .main-heading {
   font-weight: 700;
   font-size: medium;
   padding: 4px !important;
   } 
   .heading {
   font-weight: 700;
   font-size: 15px;
   } 
   .panel-default{
   margin-right:3%;
   font-size:30px;
   }
   .content-body{
   margin-bottom: 80%;
   }
   table {
  border: 1px solid #ccc;
  border-collapse: collapse;
  margin: 0;
  padding: 0;
  width: 100%;
  table-layout: fixed;
}

table caption {
  font-size: 1.5em;
  margin: .5em 0 .75em;
}

table tr {
  //background-color: #f8f8f8;
  border: 1px solid #ddd;
  padding: .35em;
}

table th,
table td {
  padding: .625em;
  text-align: center;
}

table th {
  font-size: .85em;
  letter-spacing: .1em;
  text-transform: uppercase;
}

@media screen and (max-width: 600px) {
  table {
    border: 0;
  }

  table caption {
    font-size: 1.3em;
  }
  
  table thead {
    border: none;
    clip: rect(0 0 0 0);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
  }
  
  table tr {
    border-bottom: 3px solid #ddd;
    display: block;
    margin-bottom: .625em;
  }
  
  table td {
    border-bottom: 1px solid #ddd;
    display: block;
    font-size: .8em;
    text-align: right;
  }
  
  table td::before {
    /*
    * aria-label has no advantage, it won't be read inside a table
    content: attr(aria-label);
    */
    content: attr(data-label);
    float: left;
    font-weight: bold;
    text-transform: uppercase;
  }
  
  table td:last-child {
    border-bottom: 0;
  }
}
.piechart {
  height : 40px;
  width : 430px;
  background-color : #DDDDDD;
  padding:14px;
  font-weight:700;
}
.barchart {
  height : 40px;
  width : 470px;
  background-color : #DDDDDD;
  padding:10px;
  font-weight:700;
  
}
</style>
{/literal}
<div class="row content-body">
   <div class="col-md-12">
      <div class="col-md-6">
         <div class="piechart">
          <div><p style="float:left;font-size:x-large;">Tickets : 2</p></div>
        </div>
        <div 
          style="padding: 0px;
            margin: 0px 0px 5px 0px;
            height: 300px; 
            width: 430px;
            background-color: #DDDDDD";
        >
            <div id="chart_div">
            </div>
            <div style="display:flex">
                <div class="bar-box-blue" style="border-radius:14px;margin-left:12%;margin-top:4px;height:10px;width:10px;background:#DD4210;"></div> &nbsp;High
                <div class="bar-box-red" style="border-radius:14px;margin-left:12%;margin-top:4px;height:10px;width:10px;background:#F59804;"></div> &nbsp;Medium 
                <div class="bar-box-blue" style="border-radius:14px;margin-left:12%;margin-top:4px;height:10px;width:10px;background:blue;"></div> &nbsp;Low 
            </div>    
         </div>
      </div>
       <div class="col-md-6">
       <div class="barchart">
          <div><p style="float:left;font-size:x-large;">Leads : {$total_lead}</p><p style="float:right;font-size:x-large;">Converted : {$total_convert_lead}</p></div>
       </div>
          <p style="background-color : #DDDDDD;font-weight:700;width : 470px;font-size:larger;"> Convert Leads (Last 6 Months)</p>
       <div
          style="
            padding: 0px;
            margin: 0px 0px 5px 0px;
            height: 280px;
            width: 470px; 
            background-color: #DDDDDD;
        "
        >
         <div id="barchart_values" style="margin-left:1px"></div>
         <div style="display:flex">
            <div class="bar-box-red" style="margin-left:24%;margin-top:4px;height:10px;width:10px;background:#DD4210;"></div> &nbsp;Converted 
            <div class="bar-box-blue" style="margin-left:12%;margin-top:4px;height:10px;width:10px;background:blue;"></div> &nbsp;Created
         </div>
         </div>
       </div>
      </div>
      <div class="panel panel-default col-md-5" style="background-color:#17a2b8!important;height:250px;overflow-y: scroll;">
         <div class="panel-heading heading text-center" style="background-color:#348A9D!important"> <span style="float:left">Recent Accounts (Last 30 Days)</span>
         <span style="float:right">Total Accounts : {$accounts}</span>
         </div>   
         <div class="panel-body text-center">
         <span style="font-size:x-large;color:white" class="">
           <table>
            <thead>
            <tr>
                  <th>Name</th>
                  <th>Mobile</th>
                  <th>Created At</th>
               </tr>
            {section name="getAccount" loop="$account_data"}
               <tr>
                  <td>{$account_data[getAccount].name}</td>
                  <td>{$account_data[getAccount].phone_office}</td>
                  <td>{$account_data[getAccount].date_entered}</td>
               </tr>
             {/section}
             </thead>   
           </table> 
         </span>
         </div>
      </div>
   </div>
   <div class="col-md-12" style = "margin-top:0px">
      
   </div>
</div>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!--Load the AJAX API-->
{if $low != 0 || $high != 0 || $medium != 0 }

   {literal}
    
    <script type="text/javascript">
      var currentYear = new Date().getFullYear();
       // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart); 

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Low', {/literal}{$low}{literal}],
          ['High', {/literal}{$high}{literal}],
          ['Medium',{/literal}{$medium}{literal}],
        ]);

        // Set chart options
        var options = {
                     titleTextStyle: {
                        fontSize: 20,
                        color:"#747474"
                     },
                     legend: {
                        position: 'none'
                     },
                    chartArea: {width: 420, height: 270}, 
                     backgroundColor: '#DDDDDD',
                     // pieStartAngle: 100,
                     //'title':'Tickets : (Year - '+currentYear+')',
                     //'title':'Tickets : {/literal}{$total_ticket}{literal}',
                      //is3D: true,
                       'width':420,
                       'height':270
                       };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
          $("text:contains(" + options.title + ")").attr({
            'x': '5',
            'y': '20'
         });
      }
  
    </script>
    {/literal}
   {/if}
   {if $low == 0 && $high == 0 && $medium == 0 }

   {literal} 
    
    <script type="text/javascript">
      var currentYear = new Date().getFullYear();
       // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart); 

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['No Data',0],
        ]);

        // Set chart options
        var options = {
                     titleTextStyle: {
                        fontSize: 20,
                        color:"#747474"
                     },
                     legend: {
                        position: 'bottom'
                     },
                     chartArea: {width: 420, height: 270}, 
                     backgroundColor: '#DDDDDD',
                      pieStartAngle: 100,
                     //'title':'Tickets : (Year - '+currentYear+')',
                     'title':'Tickets : (No Data Found)',
                      is3D: true,
                      'width':420,
                       'height':270
                       };

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
          $("text:contains(" + options.title + ")").attr({
            'x': '10',
            'y': '30'
         });
      }
  
    </script>
    {/literal}
   {/if} 
    
    
    {literal}
    <script>
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart1);
      const obj = JSON.parse('{/literal}{$leadConvertArray}{literal}');
    
      function drawChart1() {
        
         dataArray = [];
         dataArray[0] = ['Month', 'Created', 'Converted']; 
         var j=0;
         $.each(obj, function(index, value) {
            dataArray[++j] = value; 
         });
         var data = google.visualization.arrayToDataTable(dataArray); 
         var options = {
            'width':450,
            'height':260,
             backgroundColor: '#DDDDDD',
             legend: {
                        position: 'none'
                     },
            //title: "Leads : 2      Converted : 1",
            //subtitle: "Convert Leads (Last 6 Months )",
          //bar: {groupWidth: "95%"},
            chartArea: {
               left:50,top:2,width:"50%",height:"50%",
               'backgroundColor': {
                  'fill': '#F4F4F4',
                  'opacity': 50,
               },
            }
        };
        var chart = new google.charts.Bar(document.getElementById('barchart_values'));
        chart.draw(data, google.charts.Bar.convertOptions(options));
        $("text:contains(" + options.title + ")").attr({
            'x': '10',
            'y': '30'
         });
      }
    </script>
    {/literal}
    
  
