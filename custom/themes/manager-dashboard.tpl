<link rel="stylesheet" href="https://cdn.datatables.net/1.11.0/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.11.0/js/jquery.dataTables.min.js"></script> 
{literal} 
<script>       
    $(document).ready(function(){  
        $('.table_id').DataTable({
            "bLengthChange": false,
            "bInfo": false,
            "bFilter": false
        });
    });     
</script> 
<style>
.main-heading { 
    font-weight: 700;
    font-size: medium;
    padding: 4px !important;
}  
.p1{
    color:red;
}
.p2{
    color:#FFFF00;
}
.p3{
    color:blue;
}
</style>           
{/literal} 
<div class="row content-body">
   <div class="panel panel-default"><div class="panel-heading main-heading text-center">Epiphany     
    &nbsp;&nbsp;Dashboard</div> 
</div>
<div class="row">    
    <div class="tab-content col-md-6">     
        <div class="panel panel-default">   
            <div class="panel-heading main-heading text-center">Open Tickets</div>
            <table id="table_id" class="display table_id col-md-4">         
            <thead> 
                <tr>  
                    <th>Name</th> 
                    <th>Customer Name</th> 
                    <th>Priority</th> 
                    <th>Created At</th> 
                </tr> 
            </thead>     
            <tbody> 
                {foreach from=$ticketList item=name}   
                <tr>  
                    <td>{$name->name}</td> 
                    <td>{$name->account_name}</td> 
                    <td>
                        {if $name->priority eq 'P1'}
                        <span class="p1">High</span>
                        {elseif $name->priority eq 'P2'}
                        <span class="p2">Medium</span>
                        {elseif $name->priority eq 'P3'}
                        <span class="p3">Low</span>
                        {/if}
                    </td>
                    <td>{$name->date_entered}</td> 
                </tr>  
                {/foreach} 
            </tbody>
        </table>
        </div> 
    </div> 
    <div class="tab-content col-md-6">      
        <div class="panel panel-default">   
            <div class="panel-heading main-heading text-center">Leads</div> 
            <table id="table_id" class="display table_id col-md-4">         
            <thead>    
                <tr>  
                    <th>Name</th> 
                    <th>Status</th>  
                    <th>Mobile</th>  
                    <th>Created At</th>  
                </tr>  
            </thead>       
            <tbody> 
                {foreach from=$leadList item=name} 
                <tr> 
                    <td>{$name->full_name}</td>
                    <td>{$name->status}</td>
                    <td>{$name->phone_mobile}</td> 
                    <td>{$name->date_entered}</td>
                </tr> 
                {/foreach}
            </tbody>
        </table>
        </div> 
    </div> 
</div> 
 <div class="tab-content col-md-6">      
        <div class="panel panel-default">   
            <div class="panel-heading main-heading text-center">Converted Leads</div> 
            <table id="table_id" class="display table_id col-md-4">         
            <thead>    
                <tr>  
                    <th>Name</th> 
                    <th>Status</th>  
                    <th>Mobile</th>  
                    <th>Created At</th>  
                </tr>  
            </thead>       
            <tbody> 
                {foreach from=$convertLeadList item=name}  
                <tr> 
                    <td>{$name->full_name}</td> 
                    <td>{$name->status}</td>
                    <td>{$name->phone_mobile}</td> 
                    <td>{$name->date_entered}</td>
                </tr> 
                {/foreach}
            </tbody>
        </table>
        </div> 
    </div>  
