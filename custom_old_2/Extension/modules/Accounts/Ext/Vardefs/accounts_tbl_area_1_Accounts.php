<?php
// created: 2021-09-23 07:43:36
$dictionary["Account"]["fields"]["accounts_tbl_area_1"] = array (
  'name' => 'accounts_tbl_area_1',
  'type' => 'link',
  'relationship' => 'accounts_tbl_area_1',
  'source' => 'non-db',
  'module' => 'tbl_Area',
  'bean_name' => 'tbl_Area',
  'vname' => 'LBL_ACCOUNTS_TBL_AREA_1_FROM_TBL_AREA_TITLE',
  'id_name' => 'accounts_tbl_area_1tbl_area_idb',
);
$dictionary["Account"]["fields"]["accounts_tbl_area_1_name"] = array (
  'name' => 'accounts_tbl_area_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_TBL_AREA_1_FROM_TBL_AREA_TITLE',
  'save' => true,
  'id_name' => 'accounts_tbl_area_1tbl_area_idb',
  'link' => 'accounts_tbl_area_1',
  'table' => 'tbl_area',
  'module' => 'tbl_Area',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["accounts_tbl_area_1tbl_area_idb"] = array (
  'name' => 'accounts_tbl_area_1tbl_area_idb',
  'type' => 'link',
  'relationship' => 'accounts_tbl_area_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_ACCOUNTS_TBL_AREA_1_FROM_TBL_AREA_TITLE',
);
