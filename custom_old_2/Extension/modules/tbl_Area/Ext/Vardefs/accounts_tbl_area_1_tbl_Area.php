<?php
// created: 2021-09-23 07:43:36
$dictionary["tbl_Area"]["fields"]["accounts_tbl_area_1"] = array (
  'name' => 'accounts_tbl_area_1',
  'type' => 'link',
  'relationship' => 'accounts_tbl_area_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_TBL_AREA_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_tbl_area_1accounts_ida',
);
$dictionary["tbl_Area"]["fields"]["accounts_tbl_area_1_name"] = array (
  'name' => 'accounts_tbl_area_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_TBL_AREA_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_tbl_area_1accounts_ida',
  'link' => 'accounts_tbl_area_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["tbl_Area"]["fields"]["accounts_tbl_area_1accounts_ida"] = array (
  'name' => 'accounts_tbl_area_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_tbl_area_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_ACCOUNTS_TBL_AREA_1_FROM_ACCOUNTS_TITLE',
);
