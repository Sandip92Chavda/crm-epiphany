<?php
// created: 2021-05-27 15:04:46
$dictionary["tbl_Area"]["fields"]["tbl_location_tbl_area"] = array (
  'name' => 'tbl_location_tbl_area',
  'type' => 'link',
  'relationship' => 'tbl_location_tbl_area',
  'source' => 'non-db',
  'module' => 'tbl_Location',
  'bean_name' => 'tbl_Location',
  'vname' => 'LBL_TBL_LOCATION_TBL_AREA_FROM_TBL_LOCATION_TITLE',
  'id_name' => 'tbl_location_tbl_areatbl_location_ida',
);
$dictionary["tbl_Area"]["fields"]["tbl_location_tbl_area_name"] = array (
  'name' => 'tbl_location_tbl_area_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_TBL_LOCATION_TBL_AREA_FROM_TBL_LOCATION_TITLE',
  'save' => true,
  'id_name' => 'tbl_location_tbl_areatbl_location_ida',
  'link' => 'tbl_location_tbl_area',
  'table' => 'tbl_location',
  'module' => 'tbl_Location',
  'rname' => 'name',
);
$dictionary["tbl_Area"]["fields"]["tbl_location_tbl_areatbl_location_ida"] = array (
  'name' => 'tbl_location_tbl_areatbl_location_ida',
  'type' => 'link',
  'relationship' => 'tbl_location_tbl_area',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_TBL_LOCATION_TBL_AREA_FROM_TBL_AREA_TITLE',
);
