<?php
include_once('./include/database/DBManagerFactory.php');
require_once('include/SugarPHPMailer.php');

Class SendCron {
    /**
     * @var db
     */
    protected $db;
    protected $mail;
    public function __construct() { 
        $this->db = \DBManagerFactory::getInstance();
        $this->mail = new SugarPHPMailer();
    } 
    /**
     * @author priyankas.etechmavens@gmail.com
     * @param @id
     * @return userdata
     */
    public function getUserData($id) {
        $query = "SELECT users.id, users.user_name, users.first_name, users.last_name, users.phone_mobile, 
        ea.email_address as email1, users.address_street, users.address_city, users.address_state,
        users.address_country, users.address_postalcode FROM users 
        LEFT JOIN email_addr_bean_rel as er ON er.bean_id = users.id and er.primary_address = 1
        LEFT JOIN email_addresses as ea ON ea.id = er.email_address_id 
        WHERE users.id = '{$id}'";
        $results = $this->db->query($query);
        $row = $this->db->fetchByAssoc($results);
        return $row;
    }   
    /**
     * @author sandeepc.etechmavens@gmail.com
     * 
     */
    public function sendEvent(){  
        $result = [];
        $query = 'SELECT fs.*,fu.fp_events_users_1users_idb AS user_id FROM fp_events fs 
          INNER JOIN fp_events_cstm fc ON fc.id_c = fs.id
          INNER JOIN fp_events_users_1_c fu ON fu.fp_events_users_1fp_events_ida = fs.id
          where fc.is_send_c = 0 AND fu.deleted = 0'; 
        // echo $query;die;
        $results = $this->db->query($query);
        while ( $event  = $this->db->fetchByAssoc($results) ) { 
            $userData = $this->getUserData($event['user_id']);
            try { 
                // echo "<pre>";print_r($event);die;
                if(!empty($userData['email1'])){
                    $emailObj = new Email();
                    $defaults = $emailObj->getSystemDefaultEmail();
                    // $mail = new SugarPHPMailer();
                    $this->mail->setMailerForSystem();
                    $this->mail->From = "{$defaults['email']}";
                    $this->mail->FromName = "{$defaults['name']}";
                    $this->mail->Subject = "{$event['name']}";
                    $this->mail->Body = "{$event['description']}";
                    $this->mail->prepForOutbound();
                    $this->mail->AddAddress($userData['email1']);
                    @$this->mail->Send();   
                    /** update cron status **/ 
                    $eventId = $event['id'];
                    $sqlQuery = "UPDATE fp_events_cstm SET is_send_c = '1' WHERE id_c = '{$eventId}'";
                    $this->db->query($sqlQuery); 

                }
            } catch (Exception $e) {
              echo $e->getMessage(); //Boring error messages from anything else!
            }  
        }
        echo "send successfull";
    }
}
$cron = new SendCron();
$cron->sendEvent();

?>  