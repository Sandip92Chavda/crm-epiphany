<?php
/** get plan list */
include_once('./lib/custom/CallOlaApi.php'); 
$call = new \CallOlaApi();
$planApiData = $call->getPlanList(); 
$countryApiData = $call->getCountryList();
$stateApiData = $call->getStateList();
$cityApiData = $call->getCityList();
$customerApiData = $call->getCustomers();
// echo "<pre>";print_r($customerApiData);die;
$app_list_strings['moduleList']['Cases']='Tickets';
$app_list_strings['moduleListSingular']['Cases']='Ticket';
$app_list_strings['record_type_display']['Cases']='Ticket';
$app_list_strings['parent_type_display']['Cases']='Ticket';
$app_list_strings['record_type_display_notes']['Cases']='Ticket';
$GLOBALS['app_list_strings']['type_list']=array (
  'FiberCut' => 'FiberCut',
);
$app_list_strings['moduleList']['jjwg_Areas']='Location -Trackings';
$app_list_strings['moduleListSingular']['jjwg_Areas']='Location -Tracking';
// $GLOBALS['app_list_strings']['account_plan_list ']=array (
// );
$GLOBALS['app_list_strings']['account_plan_list']=$planApiData;
// $GLOBALS['app_list_strings']['account_plan_list']=array (
//   4 => 'PostPaid1000',
//   6 => 'Prepaid100',
//   7 => 'Postpaid-10Mbps',
//   8 => 'Postpaid-20Mbps',
//   9 => 'Postpaid-30Mbps',
//   10 => 'Postpaid-40Mbps',
//   11 => 'Postpaid-50Mbps',
//   12 => 'Prepaid-10Mbps',
//   13 => 'Prepaid-20Mbps',
//   14 => 'Prepaid-30Mbps',
//   15 => 'Prepaid-40Mbps',
//   16 => 'Prepaid-50Mbps',
// );
// $GLOBALS['app_list_strings']['city_list']=array (
// );
// $GLOBALS['app_list_strings']['country_list']=array (
// );
// $GLOBALS['app_list_strings']['state_list']=array (
// );
$GLOBALS['app_list_strings']['country_list']=$countryApiData;
$GLOBALS['app_list_strings']['state_list']=$stateApiData;
$GLOBALS['app_list_strings']['city_list']=$cityApiData; 
$GLOBALS['app_list_strings']['case_state_dom']=array (
  'Open' => 'Open',
  'Closed' => 'Closed',
  'Reject' => 'Reject',
  'InReassignment' => 'InReassignment',
);

$GLOBALS['app_list_strings']['lead_source_dom']=array (
  'None' => 'None',
  'Cold Call' => 'Cold Call',
  'Existing Customer' => 'Existing Customer',
  'Self Generated' => 'Self Generated',
  'Employee' => 'Employee',
  'Partner' => 'Partner',
  'Public Relations' => 'Public Relations',
  'Direct Mail' => 'Direct Mail',
  'Conference' => 'Conference',
  'Trade Show' => 'Trade Show',
  'Web Site' => 'Web Site',
  'Word of mouth' => 'Word of mouth',
  'Email' => 'Email',
  'Campaign' => 'Campaign',
  'Other' => 'Other',
);

$GLOBALS['app_list_strings']['case_type_dom']=array (
  1 => 'Billing',
  2 => 'Payment',
  3 => 'Service',
);
$GLOBALS['app_list_strings']['case_subtype_dom']=array (
  '1_1' => 'Not receving bills',
  '1_2' => 'Temporary disconnection',
  '1_3' => 'Incorrect billing',
  '1_4' => 'Others',
  '2_5' => 'Unable to pay online',
  '2_6' => 'Payment done but service not restored',
  '2_7' => 'Others',
  '3_8' => 'Not able to use the internet',
  '3_9' => 'Slow speed',
  '3_10' => 'Router configuration',
);
$GLOBALS['app_list_strings']['bill_day_list']=array (
  1 => '1',
  2 => '2',
  3 => '3',
  4 => '4',
  5 => '5',
  6 => '6',
  7 => '7',
  8 => '8',
  9 => '9',
  10 => '10',
  11 => '11',
  12 => '12',
  13 => '13',
  14 => '14',
  15 => '15',
  16 => '16',
  17 => '17',
  18 => '18',
  19 => '19',
  20 => '20',
  21 => '21',
  22 => '22',
  23 => '23',
  24 => '24',
  25 => '25',
  26 => '26',
  27 => '27',
  28 => '28',
);
$GLOBALS['app_list_strings']['parent_customer_list']=$customerApiData;
$GLOBALS['app_list_strings']['invoice_option_list']=array (
  0 => ' ',
  1 => 'Consolidated',
  2 => 'Single',
);
$GLOBALS['app_list_strings']['protocol_list ']=array (
  0 => ' ',
  1 => 'IPOE',
  2 => 'PPPOE',
);
$GLOBALS['app_list_strings']['generat_type_list ']=array (
  1 => 'yes',
  0 => 'no',
);
$GLOBALS['app_list_strings']['ip_address_mode_list']=array (
  0 => ' ',
  1 => 'Static',
  2 => 'DHCP',
  3 => 'Ip Pool',
  4 => 'Pool + Static Ip',
);
$GLOBALS['app_list_strings']['connection_type_list']=array (
  1 => 'Fiber Connection',
  2 => 'Cat5',
  3 => 'Wireless',
);