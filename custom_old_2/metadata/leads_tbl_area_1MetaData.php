<?php
// created: 2021-09-23 07:35:58
$dictionary["leads_tbl_area_1"] = array (
  'true_relationship_type' => 'one-to-one',
  'from_studio' => true,
  'relationships' => 
  array (
    'leads_tbl_area_1' => 
    array (
      'lhs_module' => 'Leads',
      'lhs_table' => 'leads',
      'lhs_key' => 'id',
      'rhs_module' => 'tbl_Area',
      'rhs_table' => 'tbl_area',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'leads_tbl_area_1_c',
      'join_key_lhs' => 'leads_tbl_area_1leads_ida',
      'join_key_rhs' => 'leads_tbl_area_1tbl_area_idb',
    ),
  ),
  'table' => 'leads_tbl_area_1_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'leads_tbl_area_1leads_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'leads_tbl_area_1tbl_area_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'leads_tbl_area_1spk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'leads_tbl_area_1_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'leads_tbl_area_1leads_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'leads_tbl_area_1_idb2',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'leads_tbl_area_1tbl_area_idb',
      ),
    ),
  ),
);