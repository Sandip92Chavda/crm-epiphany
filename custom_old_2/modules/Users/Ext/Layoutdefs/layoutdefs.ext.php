<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2021-10-19 08:52:01
$layout_defs["Users"]["subpanel_setup"]['users_tbl_area_1'] = array (
  'order' => 100,
  'module' => 'tbl_Area',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_USERS_TBL_AREA_1_FROM_TBL_AREA_TITLE',
  'get_subpanel_data' => 'users_tbl_area_1',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>