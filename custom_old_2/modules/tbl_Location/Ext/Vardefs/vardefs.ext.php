<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2021-05-27 15:04:46
$dictionary["tbl_Location"]["fields"]["tbl_location_tbl_area"] = array (
  'name' => 'tbl_location_tbl_area',
  'type' => 'link',
  'relationship' => 'tbl_location_tbl_area',
  'source' => 'non-db',
  'module' => 'tbl_Area',
  'bean_name' => 'tbl_Area',
  'side' => 'right',
  'vname' => 'LBL_TBL_LOCATION_TBL_AREA_FROM_TBL_AREA_TITLE',
);


// created: 2021-05-27 15:04:46
$dictionary["tbl_Location"]["fields"]["tbl_location_users"] = array (
  'name' => 'tbl_location_users',
  'type' => 'link',
  'relationship' => 'tbl_location_users',
  'source' => 'non-db',
  'module' => 'Users',
  'bean_name' => 'User',
  'side' => 'right',
  'vname' => 'LBL_TBL_LOCATION_USERS_FROM_USERS_TITLE',
);

?>