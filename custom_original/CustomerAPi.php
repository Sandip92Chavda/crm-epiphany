<?php
include_once('./include/database/DBManagerFactory.php');
include_once('./lib/custom/RegisterOssApi.php'); 
use BeanFactory;
/** call customer api */
$call = new CustomerAPi();
$call->getCustomerList();  
Class CustomerAPi {
    public function __construct() {  
        $this->db = \DBManagerFactory::getInstance();
    } 
    public function getCustomerList(){
        $url = "http://192.168.0.26:8080/api/v1/customers";
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
        "authorization: Basic YWRtaW46YWRtaW5AMTIz",
        "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only! 
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($curlHandle, CURLOPT_CONNECTTIMEOUT, 0); 
        $resp = curl_exec($curl); 
        // echo "<pre>";print_r($resp);die;
        curl_close($curl); 
        $response = [];
        if(!empty($resp)){  
            $data =  json_decode($resp, true);
            if(!empty($data['customerList']) && count($data['customerList'])>0){
                foreach($data['customerList'] as $key => $value){  
                    $this->addUserData($value); 
                }   
                echo "done"; 
            }   
        }   
    }
    public function addUserData($data) { 
        $call = new RegisterOssApi(); 
        $custId = $call->getCustomerDetail($data["username"]);
        $customerAddress = $call->getCustomerAddress($custId);
        
        /** check username in our system */
        $sql = sprintf('SELECT * FROM users WHERE user_name = "%s"',$data["username"]); 
        $result = $this->db->query($sql);
        echo "<pre>";print_r($data["username"]));print_r($result);die;
        if(isset($result->num_rows) && $result->num_rows === 0) {   
            $name = $data["username"]; 
            $bean = BeanFactory::newBean('Accounts');   //Create bean  using module name 
            $bean->name = $name;   //Populate bean fields
            $bean->firstname_c = $data["firstname"];   //Populate bean fields
            $bean->lastname_c = $data["lastname"];   //Populate bean fields
            $bean->phone_office  = $data["phone"];   //Populate bean fields
            $bean->addressstreet_c  = !empty($customerAddress[0]["address1"])?$customerAddress[0]["address1"]:'';
            $bean->postalcode_c  = !empty($customerAddress[0]["pincode"])?$customerAddress[0]["pincode"]:''; 
            $bean->city_c = !empty($customerAddress[0]["city"])?$customerAddress[0]["city"]:1;
            $bean->state_c = !empty($customerAddress[0]["state_c"])?$customerAddress[0]["state_c"]:1;
            $bean->country_c = !empty($customerAddress[0]["country_c"])?$customerAddress[0]["country_c"]:1;
            $bean->assigned_user_id = 1;   //Populate bean fields 
            $bean->plantype_c =  !empty($data['planMappingPojoList'][0]['planId']) ? $data['planMappingPojoList'][0]['planId']:0;  
            $bean->emailAddress->addAddress($data['email'], true);
            $bean->save();   //Save   
            // $password = password_hash(strtolower(md5($name)), PASSWORD_DEFAULT);  
            $user = BeanFactory::newBean('Users')->retrieve_by_string_fields(array('user
                _name' => $name ));  
            // $user->user_name =  $data["username"];
            // $user->first_name =  $data["firstname"];
            // $user->last_name =  $data["lastname"]; 
            // $user->status = 'Active';
            // $user->employee_status = 'Active';  
            $user->phone_mobile  = $data["phone"];  
            $user->emailAddress->addAddress($data['email']); 
            $user->address_street = !empty($customerAddress[0]["address1"])?$customerAddress[0]["address1"];''; 
            $user->address_postalcode = !empty($customerAddress[0]["pincode"])?$customerAddress[0]["pincode"]:''; 
            $user->city_c = !empty($customerAddress[0]["city"])?$customerAddress[0]["city"]:1;
            $user->state_c = !empty($customerAddress[0]["state_c"])?$customerAddress[0]["state_c"]:1;
            $user->country_c = !empty($customerAddress[0]["country_c"])?$customerAddress[0]["country_c"]:1; 
            $user->save();   
            // echo $user->user_name; 
            // // echo "done";
            /*-- Add Oauth token --*/ 
            // $client = BeanFactory::newBean('OAuth2Clients');
            // $client->name = $name;  
            // $client->secret = hash('sha256', $name);
            // $client->allowed_grant_type = 'client_credentials';
            // $client->duration_value = 60; 
            // $client->duration_amount = 1;
            // $client->duration_unit = 'minute'; 
            // $client->assigned_user_id =  $user->id; 
            // $client->save();  
            // echo $client->name;
            // die;
             // echo "<pre>";print_r($bean->id);print_r($user->id);print_r($client->id);die;
        }   
    }
    
} 
