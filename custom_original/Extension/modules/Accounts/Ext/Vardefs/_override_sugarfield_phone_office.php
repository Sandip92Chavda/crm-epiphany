<?php
 // created: 2021-09-09 10:10:41
$dictionary['Account']['fields']['phone_office']['required']=true;
$dictionary['Account']['fields']['phone_office']['audited']=false;
$dictionary['Account']['fields']['phone_office']['inline_edit']='';
$dictionary['Account']['fields']['phone_office']['comments']='The office phone number';
$dictionary['Account']['fields']['phone_office']['importable']='false';
$dictionary['Account']['fields']['phone_office']['merge_filter']='disabled';

 ?>