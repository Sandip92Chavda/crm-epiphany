<?php
 // created: 2021-07-21 22:32:49
$dictionary['Case']['fields']['type']['len']=100;
$dictionary['Case']['fields']['type']['inline_edit']=true;
$dictionary['Case']['fields']['type']['comments']='The type of issue (ex: issue, feature)';
$dictionary['Case']['fields']['type']['merge_filter']='disabled';

 ?>