<?php
// created: 2021-05-27 15:04:46
$dictionary["User"]["fields"]["tbl_location_users"] = array (
  'name' => 'tbl_location_users',
  'type' => 'link',
  'relationship' => 'tbl_location_users',
  'source' => 'non-db',
  'module' => 'tbl_Location',
  'bean_name' => 'tbl_Location',
  'vname' => 'LBL_TBL_LOCATION_USERS_FROM_TBL_LOCATION_TITLE',
  'id_name' => 'tbl_location_userstbl_location_ida',
);
$dictionary["User"]["fields"]["tbl_location_users_name"] = array (
  'name' => 'tbl_location_users_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_TBL_LOCATION_USERS_FROM_TBL_LOCATION_TITLE',
  'save' => true,
  'id_name' => 'tbl_location_userstbl_location_ida',
  'link' => 'tbl_location_users',
  'table' => 'tbl_location',
  'module' => 'tbl_Location',
  'rname' => 'name',
);
$dictionary["User"]["fields"]["tbl_location_userstbl_location_ida"] = array (
  'name' => 'tbl_location_userstbl_location_ida',
  'type' => 'link',
  'relationship' => 'tbl_location_users',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_TBL_LOCATION_USERS_FROM_USERS_TITLE',
);
