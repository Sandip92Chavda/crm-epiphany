<?php
// created: 2021-07-07 00:26:12
$dictionary["FP_events"]["fields"]["fp_events_users_1"] = array (
  'name' => 'fp_events_users_1',
  'type' => 'link',
  'relationship' => 'fp_events_users_1',
  'source' => 'non-db',
  'module' => 'Users',
  'bean_name' => 'User',
  'side' => 'right',
  'vname' => 'LBL_FP_EVENTS_USERS_1_FROM_USERS_TITLE',
);
