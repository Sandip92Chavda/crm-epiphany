<?php
// created: 2021-09-23 07:35:58
$dictionary["Lead"]["fields"]["leads_tbl_area_1"] = array (
  'name' => 'leads_tbl_area_1',
  'type' => 'link',
  'relationship' => 'leads_tbl_area_1',
  'source' => 'non-db',
  'module' => 'tbl_Area',
  'bean_name' => 'tbl_Area',
  'vname' => 'LBL_LEADS_TBL_AREA_1_FROM_TBL_AREA_TITLE',
  'id_name' => 'leads_tbl_area_1tbl_area_idb',
);
$dictionary["Lead"]["fields"]["leads_tbl_area_1_name"] = array (
  'name' => 'leads_tbl_area_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_TBL_AREA_1_FROM_TBL_AREA_TITLE',
  'save' => true,
  'id_name' => 'leads_tbl_area_1tbl_area_idb',
  'link' => 'leads_tbl_area_1',
  'table' => 'tbl_area',
  'module' => 'tbl_Area',
  'rname' => 'name',
);
$dictionary["Lead"]["fields"]["leads_tbl_area_1tbl_area_idb"] = array (
  'name' => 'leads_tbl_area_1tbl_area_idb',
  'type' => 'link',
  'relationship' => 'leads_tbl_area_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_LEADS_TBL_AREA_1_FROM_TBL_AREA_TITLE',
);
