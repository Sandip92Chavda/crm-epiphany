<?php
// created: 2021-10-19 08:52:01
$dictionary["tbl_Area"]["fields"]["users_tbl_area_1"] = array (
  'name' => 'users_tbl_area_1',
  'type' => 'link',
  'relationship' => 'users_tbl_area_1',
  'source' => 'non-db',
  'module' => 'Users',
  'bean_name' => 'User',
  'vname' => 'LBL_USERS_TBL_AREA_1_FROM_USERS_TITLE',
  'id_name' => 'users_tbl_area_1users_ida',
);
$dictionary["tbl_Area"]["fields"]["users_tbl_area_1_name"] = array (
  'name' => 'users_tbl_area_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_USERS_TBL_AREA_1_FROM_USERS_TITLE',
  'save' => true,
  'id_name' => 'users_tbl_area_1users_ida',
  'link' => 'users_tbl_area_1',
  'table' => 'users',
  'module' => 'Users',
  'rname' => 'name',
);
$dictionary["tbl_Area"]["fields"]["users_tbl_area_1users_ida"] = array (
  'name' => 'users_tbl_area_1users_ida',
  'type' => 'link',
  'relationship' => 'users_tbl_area_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_USERS_TBL_AREA_1_FROM_TBL_AREA_TITLE',
);
