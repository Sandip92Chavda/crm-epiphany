<?php
// created: 2021-09-23 07:35:58
$dictionary["tbl_Area"]["fields"]["leads_tbl_area_1"] = array (
  'name' => 'leads_tbl_area_1',
  'type' => 'link',
  'relationship' => 'leads_tbl_area_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_TBL_AREA_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_tbl_area_1leads_ida',
);
$dictionary["tbl_Area"]["fields"]["leads_tbl_area_1_name"] = array (
  'name' => 'leads_tbl_area_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_TBL_AREA_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_tbl_area_1leads_ida',
  'link' => 'leads_tbl_area_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["tbl_Area"]["fields"]["leads_tbl_area_1leads_ida"] = array (
  'name' => 'leads_tbl_area_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_tbl_area_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_LEADS_TBL_AREA_1_FROM_LEADS_TITLE',
);
