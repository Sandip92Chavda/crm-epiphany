<?php
// created: 2021-05-27 15:04:46
$dictionary["tbl_location_users"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'tbl_location_users' => 
    array (
      'lhs_module' => 'tbl_Location',
      'lhs_table' => 'tbl_location',
      'lhs_key' => 'id',
      'rhs_module' => 'Users',
      'rhs_table' => 'users',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'tbl_location_users_c',
      'join_key_lhs' => 'tbl_location_userstbl_location_ida',
      'join_key_rhs' => 'tbl_location_usersusers_idb',
    ),
  ),
  'table' => 'tbl_location_users_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'tbl_location_userstbl_location_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'tbl_location_usersusers_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'tbl_location_usersspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'tbl_location_users_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'tbl_location_userstbl_location_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'tbl_location_users_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'tbl_location_usersusers_idb',
      ),
    ),
  ),
);