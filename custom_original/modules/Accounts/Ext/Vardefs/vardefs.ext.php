<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2021-09-23 07:43:36
$dictionary["Account"]["fields"]["accounts_tbl_area_1"] = array (
  'name' => 'accounts_tbl_area_1',
  'type' => 'link',
  'relationship' => 'accounts_tbl_area_1',
  'source' => 'non-db',
  'module' => 'tbl_Area',
  'bean_name' => 'tbl_Area',
  'vname' => 'LBL_ACCOUNTS_TBL_AREA_1_FROM_TBL_AREA_TITLE',
  'id_name' => 'accounts_tbl_area_1tbl_area_idb',
);
$dictionary["Account"]["fields"]["accounts_tbl_area_1_name"] = array (
  'name' => 'accounts_tbl_area_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_TBL_AREA_1_FROM_TBL_AREA_TITLE',
  'save' => true,
  'id_name' => 'accounts_tbl_area_1tbl_area_idb',
  'link' => 'accounts_tbl_area_1',
  'table' => 'tbl_area',
  'module' => 'tbl_Area',
  'rname' => 'name',
);
$dictionary["Account"]["fields"]["accounts_tbl_area_1tbl_area_idb"] = array (
  'name' => 'accounts_tbl_area_1tbl_area_idb',
  'type' => 'link',
  'relationship' => 'accounts_tbl_area_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_ACCOUNTS_TBL_AREA_1_FROM_TBL_AREA_TITLE',
);


 // created: 2021-09-09 10:00:30
$dictionary['Account']['fields']['lastname_c']['inline_edit']='';
$dictionary['Account']['fields']['lastname_c']['labelValue']='LastName';

 

 // created: 2021-09-09 10:00:19
$dictionary['Account']['fields']['firstname_c']['inline_edit']='';
$dictionary['Account']['fields']['firstname_c']['labelValue']='FirstName';

 

 // created: 2021-04-29 06:41:38
$dictionary['Account']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2021-04-29 06:41:38
$dictionary['Account']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2021-09-09 10:01:44
$dictionary['Account']['fields']['plantype_c']['inline_edit']='';
$dictionary['Account']['fields']['plantype_c']['labelValue']='PlanType';

 

 // created: 2021-04-29 06:41:38
$dictionary['Account']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2021-04-29 06:41:37
$dictionary['Account']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2021-09-06 04:21:26
$dictionary['Account']['fields']['accountnumber_c']['inline_edit']='';
$dictionary['Account']['fields']['accountnumber_c']['labelValue']='AccountNumber';

 

 // created: 2021-09-09 09:50:34
$dictionary['Account']['fields']['country_c']['inline_edit']='';
$dictionary['Account']['fields']['country_c']['labelValue']='Country';

 

 // created: 2021-10-13 10:54:35
$dictionary['Account']['fields']['state_c']['inline_edit']='';
$dictionary['Account']['fields']['state_c']['labelValue']='State';

 

 // created: 2021-10-13 10:56:39
$dictionary['Account']['fields']['city_c']['inline_edit']='';
$dictionary['Account']['fields']['city_c']['labelValue']='City';

 

 // created: 2021-09-09 09:53:17
$dictionary['Account']['fields']['addressstreet_c']['inline_edit']='';
$dictionary['Account']['fields']['addressstreet_c']['labelValue']='AddressStreet';

 

 // created: 2021-09-09 10:01:23
$dictionary['Account']['fields']['email1']['required']=true;
$dictionary['Account']['fields']['email1']['inline_edit']='';
$dictionary['Account']['fields']['email1']['merge_filter']='disabled';

 

 // created: 2021-09-09 10:04:21
$dictionary['Account']['fields']['pincode_c']['inline_edit']='';
$dictionary['Account']['fields']['pincode_c']['labelValue']='PinCode';

 

 // created: 2021-09-09 10:10:41
$dictionary['Account']['fields']['phone_office']['required']=true;
$dictionary['Account']['fields']['phone_office']['audited']=false;
$dictionary['Account']['fields']['phone_office']['inline_edit']='';
$dictionary['Account']['fields']['phone_office']['comments']='The office phone number';
$dictionary['Account']['fields']['phone_office']['importable']='false';
$dictionary['Account']['fields']['phone_office']['merge_filter']='disabled';

 

 // created: 2021-10-13 10:38:43
$dictionary['Account']['fields']['invoiceoption_c']['inline_edit']='';
$dictionary['Account']['fields']['invoiceoption_c']['labelValue']='invoiceoption';

 

 // created: 2021-10-13 10:39:44
$dictionary['Account']['fields']['parentcustomer_c']['inline_edit']='';
$dictionary['Account']['fields']['parentcustomer_c']['labelValue']='parentcustomer';

 

 // created: 2021-10-13 10:40:19
$dictionary['Account']['fields']['billentity_c']['inline_edit']='';
$dictionary['Account']['fields']['billentity_c']['labelValue']='billentity';

 

 // created: 2021-10-13 10:41:05
$dictionary['Account']['fields']['authprotocol_c']['inline_edit']='';
$dictionary['Account']['fields']['authprotocol_c']['labelValue']='authprotocol';

 

 // created: 2021-10-13 10:41:28
$dictionary['Account']['fields']['ipaddress_c']['inline_edit']='';
$dictionary['Account']['fields']['ipaddress_c']['labelValue']='ipaddress';

 

 // created: 2021-10-13 10:41:57
$dictionary['Account']['fields']['billday_c']['inline_edit']='';
$dictionary['Account']['fields']['billday_c']['labelValue']='billday';

 

 // created: 2021-10-13 10:42:18
$dictionary['Account']['fields']['building_c']['inline_edit']='';
$dictionary['Account']['fields']['building_c']['labelValue']='building';

 

 // created: 2021-10-13 10:43:16
$dictionary['Account']['fields']['cablelength_c']['inline_edit']='';
$dictionary['Account']['fields']['cablelength_c']['labelValue']='cablelength';

 

 // created: 2021-10-13 10:43:32
$dictionary['Account']['fields']['networkfilterid_c']['inline_edit']='';
$dictionary['Account']['fields']['networkfilterid_c']['labelValue']='networkfilterid';

 

 // created: 2021-10-13 10:44:04
$dictionary['Account']['fields']['ipmode_c']['inline_edit']='';
$dictionary['Account']['fields']['ipmode_c']['labelValue']='ipmode';

 

 // created: 2021-10-13 10:44:35
$dictionary['Account']['fields']['ippool_c']['inline_edit']='';
$dictionary['Account']['fields']['ippool_c']['labelValue']='ippool';

 

 // created: 2021-10-13 10:44:48
$dictionary['Account']['fields']['latitude_c']['inline_edit']='';
$dictionary['Account']['fields']['latitude_c']['labelValue']='latitude';

 

 // created: 2021-10-13 10:45:00
$dictionary['Account']['fields']['longitude_c']['inline_edit']='';
$dictionary['Account']['fields']['longitude_c']['labelValue']='longitude';

 

 // created: 2021-10-13 10:45:31
$dictionary['Account']['fields']['macbinding_c']['inline_edit']='';
$dictionary['Account']['fields']['macbinding_c']['labelValue']='macbinding';

 

 // created: 2021-10-13 10:45:50
$dictionary['Account']['fields']['nasportbinding_c']['inline_edit']='';
$dictionary['Account']['fields']['nasportbinding_c']['labelValue']='nasportbinding';

 

 // created: 2021-10-13 10:46:14
$dictionary['Account']['fields']['nasportid_c']['inline_edit']='';
$dictionary['Account']['fields']['nasportid_c']['labelValue']='nasportid';

 

 // created: 2021-10-13 10:46:28
$dictionary['Account']['fields']['node_c']['inline_edit']='';
$dictionary['Account']['fields']['node_c']['labelValue']='node';

 

 // created: 2021-10-13 10:46:40
$dictionary['Account']['fields']['pop_c']['inline_edit']='';
$dictionary['Account']['fields']['pop_c']['labelValue']='pop';

 

 // created: 2021-10-13 10:46:54
$dictionary['Account']['fields']['switch_c']['inline_edit']='';
$dictionary['Account']['fields']['switch_c']['labelValue']='switch';

 

 // created: 2021-10-13 10:47:09
$dictionary['Account']['fields']['switchport_c']['inline_edit']='';
$dictionary['Account']['fields']['switchport_c']['labelValue']='switchport';

 

 // created: 2021-10-13 10:53:31
$dictionary['Account']['fields']['connectiontype_c']['inline_edit']='';
$dictionary['Account']['fields']['connectiontype_c']['labelValue']='connectiontype';

 

 // created: 2021-10-13 11:27:44
$dictionary['Account']['fields']['iparea_c']['inline_edit']='';
$dictionary['Account']['fields']['iparea_c']['labelValue']='IpArea';

 
?>