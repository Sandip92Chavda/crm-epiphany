<?php
// created: 2021-09-15 05:23:14
$mod_strings = array (
  'LNK_NEW_CASE' => 'Create Ticket',
  'LNK_CASE_LIST' => 'View Tickets',
  'LNK_IMPORT_CASES' => 'Import Tickets',
  'LBL_LIST_FORM_TITLE' => 'Ticket List',
  'LBL_SEARCH_FORM_TITLE' => 'Ticket Search',
  'LBL_LIST_MY_CASES' => 'My Open Tickets',
  'LBL_MODULE_NAME' => 'Tickets',
  'LBL_RATING' => 'Rating',
  'LBL_FEEDBACK' => 'Feedback',
  'LBL_TYPE' => 'Type',
  'LBL_STATE' => 'Status:',
  'LBL_RESOLUTION' => 'Comment:',
  'LBL_SUBTYPE' => 'subtype',
);