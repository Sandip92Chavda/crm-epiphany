<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2021-09-23 07:35:58
$dictionary["Lead"]["fields"]["leads_tbl_area_1"] = array (
  'name' => 'leads_tbl_area_1',
  'type' => 'link',
  'relationship' => 'leads_tbl_area_1',
  'source' => 'non-db',
  'module' => 'tbl_Area',
  'bean_name' => 'tbl_Area',
  'vname' => 'LBL_LEADS_TBL_AREA_1_FROM_TBL_AREA_TITLE',
  'id_name' => 'leads_tbl_area_1tbl_area_idb',
);
$dictionary["Lead"]["fields"]["leads_tbl_area_1_name"] = array (
  'name' => 'leads_tbl_area_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_TBL_AREA_1_FROM_TBL_AREA_TITLE',
  'save' => true,
  'id_name' => 'leads_tbl_area_1tbl_area_idb',
  'link' => 'leads_tbl_area_1',
  'table' => 'tbl_area',
  'module' => 'tbl_Area',
  'rname' => 'name',
);
$dictionary["Lead"]["fields"]["leads_tbl_area_1tbl_area_idb"] = array (
  'name' => 'leads_tbl_area_1tbl_area_idb',
  'type' => 'link',
  'relationship' => 'leads_tbl_area_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_LEADS_TBL_AREA_1_FROM_TBL_AREA_TITLE',
);


 // created: 2021-04-29 06:41:38
$dictionary['Lead']['fields']['jjwg_maps_lat_c']['inline_edit']=1;

 

 // created: 2021-04-29 06:41:38
$dictionary['Lead']['fields']['jjwg_maps_geocode_status_c']['inline_edit']=1;

 

 // created: 2021-04-29 06:41:38
$dictionary['Lead']['fields']['jjwg_maps_address_c']['inline_edit']=1;

 

 // created: 2021-07-26 05:40:39
$dictionary['Lead']['fields']['inquiryfor_c']['inline_edit']='1';
$dictionary['Lead']['fields']['inquiryfor_c']['labelValue']='InquiryFor';

 

 // created: 2021-04-29 06:41:38
$dictionary['Lead']['fields']['jjwg_maps_lng_c']['inline_edit']=1;

 

 // created: 2021-09-08 03:12:18
$dictionary['Lead']['fields']['plantype_c']['inline_edit']='';
$dictionary['Lead']['fields']['plantype_c']['labelValue']='PlanType';

 

 // created: 2021-09-20 06:37:44
$dictionary['Lead']['fields']['accountnumber_c']['inline_edit']='';
$dictionary['Lead']['fields']['accountnumber_c']['labelValue']='AccountNumber';

 

 // created: 2021-09-08 03:15:53
$dictionary['Lead']['fields']['email1']['required']=true;
$dictionary['Lead']['fields']['email1']['inline_edit']='';
$dictionary['Lead']['fields']['email1']['merge_filter']='disabled';

 

 // created: 2021-09-09 10:19:38
$dictionary['Lead']['fields']['pincode_c']['inline_edit']='';
$dictionary['Lead']['fields']['pincode_c']['labelValue']='Pincode';

 

 // created: 2021-09-09 10:20:33
$dictionary['Lead']['fields']['addressstreet_c']['inline_edit']='';
$dictionary['Lead']['fields']['addressstreet_c']['labelValue']='AddressStreet';

 

 // created: 2021-09-09 10:21:38
$dictionary['Lead']['fields']['country_c']['inline_edit']='';
$dictionary['Lead']['fields']['country_c']['labelValue']='Country';

 

 // created: 2021-10-13 10:58:20
$dictionary['Lead']['fields']['state_c']['inline_edit']='';
$dictionary['Lead']['fields']['state_c']['labelValue']='State';

 

 // created: 2021-10-13 10:58:46
$dictionary['Lead']['fields']['city_c']['inline_edit']='';
$dictionary['Lead']['fields']['city_c']['labelValue']='City';

 
?>