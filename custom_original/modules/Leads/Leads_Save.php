<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry
Point');
include_once('./include/database/DBManagerFactory.php');
require_once('include/SugarPHPMailer.php');
include_once('./lib/custom/RegisterOssApi.php');  
require_once './include/SugarEmailAddress/SugarEmailAddress.php';   
include_once('./lib/custom/CommonService.php'); 
use BeanFactory;
use TimeDate; 
class Leads_Save extends CommonService 
{ 
    private $db;
    protected $mail;
    function __construct() {  
        $this->mail = new SugarPHPMailer(); 
    } 
    function QueueJob(&$bean, $event, $arguments)     
    {
        if($bean->status == 'Converted') {  
              /**Time */
            $time_now = TimeDate::getInstance()->nowDb();  
            /**initiate db connection */
            $db = \DBManagerFactory::getInstance();
            /** Update auto generate account number */ 
            $leadBean = BeanFactory::getBean('Leads')->retrieve_by_string_fields(array('id' => $bean->id));
            $accNumber = $this->generateAccountNumber();
            if(empty($leadBean->accountnumber_c)){
                $acSql = sprintf("UPDATE leads_cstm SET accountnumber_c = '%s' WHERE id_c = '%s'",$accNumber,$bean->id);  
                $db->query($acSql);  
            } 
            $noteBean = BeanFactory::getBean('Notes')->retrieve_by_string_fields(array('parent_id' => $bean->id));
            if(!empty($noteBean->id)){  
                $fileData =  BeanFactory::getBean('Notes');
                $fileData->filename = $noteBean->filename;
                $fileData->name = $noteBean->name; 
                $fileData->file_mime_type = $noteBean->file_mime_type;
                $fileData->parent_id = $bean->account_id;
                $fileData->parent_type = 'Accounts';
                $fileData->save();
            } 
             /**generate password */ 
            $password_text = $this->generatePassword();   
            
            $processBean = BeanFactory::getBean('Accounts',$bean->account_id); 
            if(!empty($processBean->id)){     
                $acSql = sprintf("UPDATE accounts_cstm SET accountnumber_c = '%s',  firstname_c = '%s', lastname_c = '%s', addressstreet_c = '%s' ,pincode_c = '%s', city_c = '%s', state_c = '%s', country_c = '%s' WHERE id_c = '%s'",$accNumber,$bean->first_name,$bean->last_name,$bean->addressstreet_c,$bean->pincode_c,$bean->city_c,$bean->state_c,$bean->country_c,$processBean->id);  
                $db->query($acSql);  
                  /** add area location */ 
                $guid=create_guid();
                $insertAccountArea = "INSERT INTO accounts_tbl_area_1_c (id,date_modified,accounts_tbl_area_1accounts_ida, accounts_tbl_area_1tbl_area_idb) VALUES('".$guid."','".$time_now."','".$processBean->id."','".$bean->leads_tbl_area_1tbl_area_idb."') ";
                $db->query($insertAccountArea);       
            }
            $data = BeanFactory::getBean('Accounts',$bean->account_id);   
            /** add users */ 
            $userSql = sprintf('SELECT * FROM users WHERE user_name = "%s"',
            $data->name);       
            $userResults =  $db->query($userSql);  
            if(isset($bean->first_name) && !empty($bean->first_name)){   
                if($userResults->num_rows == 0) {     
                    // $password = password_hash(strtolower(md5($data->name)), PASSWORD_DEFAULT);  
                    $password = password_hash(strtolower(md5($password_text)), PASSWORD_DEFAULT);  
                    $user = BeanFactory::newBean('Users');    
                    $user->user_name =  $data->name;
                    $user->first_name =  $bean->first_name;
                    $user->last_name =  $bean->last_name; 
                    $user->status = 'Active';
                    $user->employee_status = 'Active'; 
                    $user->phone_mobile  = $data->phone_office;
                    $user->user_hash = $password; 
                    $user->passwordtext_c = $password_text;
                    $user->emailAddress->addAddress($data->email1, true);
                    $user->is_customer_c = 1; /* 1 represent the customer */
                    $user->address_street = $data->addressstreet_c;  
                    $user->address_postalcode = $data->pincode_c; 
                    $user->city_c = $bean->city_c;
                    $user->state_c = $bean->state_c;
                    $user->country_c = $bean->country_c; 
                    $user->save();     
                     /** send mail */ 
                    if(!empty($bean->email1)) {       
                        $emailObj = new Email();
                        $defaults = $emailObj->getSystemDefaultEmail();
                        $description = "Your password is ".$password_text."";
                        $this->mail->setMailerForSystem();
                        $this->mail->From = "{$defaults['email']}";
                        $this->mail->FromName = "{$defaults['name']}";
                        $this->mail->Subject = "User Registration";
                        $this->mail->Body = "{$description}";
                        $this->mail->prepForOutbound();
                        $this->mail->AddAddress($data->email1);
                        @$this->mail->Send();    
                    }   
                    /*-- Add Oauth token --*/ 
                    $client = BeanFactory::newBean('OAuth2Clients');
                    $client->name = $data->name;
                    // $client->secret = hash('sha256', $data->name); 
                    $client->secret = hash('sha256', $password_text); 
                    $client->allowed_grant_type = 'client_credentials';
                    $client->duration_value = 60;
                    $client->duration_amount = 1;
                    $client->duration_unit = 'minute'; 
                    $client->assigned_user_id =  $user->id; 
                    $client->save();
                     /** add user area */
                    $guid=create_guid();
                    $insertUserArea = "INSERT INTO users_tbl_area_1_c (id,date_modified,users_tbl_area_1users_ida, users_tbl_area_1tbl_area_idb) VALUES('".$guid."','".$time_now."','".$user->id."','".$bean->leads_tbl_area_1tbl_area_idb."') ";
                    $db->query($insertUserArea); 
                } 
            }    

            $dataArray = [ 
                'username' => $data->name,
                // 'password' => $data->name,
                'password' => $password_text,
                'firstname' => $bean->first_name,
                'lastname' => $bean->last_name,
                'email' => $data->email1,
                'status' => 'Active',
                'failcount' => 0,
                'acctno' => $data->accountnumber_c,
                'custType' => "Prepaid", 
                'phone' => $data->phone_office,
                'billday' => 0,
                'partnerid' => 1,
                'addresstype' => "home",
                'address1' => $data->addressstreet_c,
                'address2' => '',
                'city' => $this->getCity($bean->city_c),
                'state' =>  $this->getState($bean->state_c),
                'country' => $bean->country_c,
                'pincode' => $bean->pincode_c,
                'planMappingPojoList' => [   
                    [
                        'planId' => $data->plantype_c
                    ]
                ],
                "flashMsg" => null,
                "invoiceOption" => "0",
                "parentCustomersId" => null,
                // "billentityname" => "Billing Entity" 
            ];   
            $postdata = json_encode($dataArray); 
            $url = "http://192.168.0.26:8080/api/v1/customers";
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1); 
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            $headers = array( 
                "authorization: Basic YWRtaW46YWRtaW5AMTIz",
                "Content-Type: application/json",
            ); 
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
            $resp = curl_exec($ch); 
            curl_close($ch);  


        }         
    }
    public function getCity($city){
       $city = explode('_',$city);
        return !empty($city[2]) ? $city[2] : 1;
    } 
    public function getState($state){
        $state = explode('_',$state);
        return !empty($state[1]) ? $state[1] : 1;
    } 
}