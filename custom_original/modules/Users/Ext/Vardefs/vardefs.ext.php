<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2021-05-27 15:04:46
$dictionary["User"]["fields"]["tbl_location_users"] = array (
  'name' => 'tbl_location_users',
  'type' => 'link',
  'relationship' => 'tbl_location_users',
  'source' => 'non-db',
  'module' => 'tbl_Location',
  'bean_name' => 'tbl_Location',
  'vname' => 'LBL_TBL_LOCATION_USERS_FROM_TBL_LOCATION_TITLE',
  'id_name' => 'tbl_location_userstbl_location_ida',
);
$dictionary["User"]["fields"]["tbl_location_users_name"] = array (
  'name' => 'tbl_location_users_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_TBL_LOCATION_USERS_FROM_TBL_LOCATION_TITLE',
  'save' => true,
  'id_name' => 'tbl_location_userstbl_location_ida',
  'link' => 'tbl_location_users',
  'table' => 'tbl_location',
  'module' => 'tbl_Location',
  'rname' => 'name',
);
$dictionary["User"]["fields"]["tbl_location_userstbl_location_ida"] = array (
  'name' => 'tbl_location_userstbl_location_ida',
  'type' => 'link',
  'relationship' => 'tbl_location_users',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_TBL_LOCATION_USERS_FROM_USERS_TITLE',
);


// created: 2021-07-07 00:26:12
$dictionary["User"]["fields"]["fp_events_users_1"] = array (
  'name' => 'fp_events_users_1',
  'type' => 'link',
  'relationship' => 'fp_events_users_1',
  'source' => 'non-db',
  'module' => 'FP_events',
  'bean_name' => 'FP_events',
  'vname' => 'LBL_FP_EVENTS_USERS_1_FROM_FP_EVENTS_TITLE',
  'id_name' => 'fp_events_users_1fp_events_ida',
);
$dictionary["User"]["fields"]["fp_events_users_1_name"] = array (
  'name' => 'fp_events_users_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_FP_EVENTS_USERS_1_FROM_FP_EVENTS_TITLE',
  'save' => true,
  'id_name' => 'fp_events_users_1fp_events_ida',
  'link' => 'fp_events_users_1',
  'table' => 'fp_events',
  'module' => 'FP_events',
  'rname' => 'name',
);
$dictionary["User"]["fields"]["fp_events_users_1fp_events_ida"] = array (
  'name' => 'fp_events_users_1fp_events_ida',
  'type' => 'link',
  'relationship' => 'fp_events_users_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_FP_EVENTS_USERS_1_FROM_USERS_TITLE',
);


// created: 2021-10-19 08:52:01
$dictionary["User"]["fields"]["users_tbl_area_1"] = array (
  'name' => 'users_tbl_area_1',
  'type' => 'link',
  'relationship' => 'users_tbl_area_1',
  'source' => 'non-db',
  'module' => 'tbl_Area',
  'bean_name' => 'tbl_Area',
  'side' => 'right',
  'vname' => 'LBL_USERS_TBL_AREA_1_FROM_TBL_AREA_TITLE',
);


 // created: 2021-08-09 04:44:01
$dictionary['User']['fields']['iscustomer_c']['inline_edit']='1';
$dictionary['User']['fields']['iscustomer_c']['labelValue']='IsCustomer';

 

 // created: 2021-08-09 04:46:03
$dictionary['User']['fields']['is_customer_c']['inline_edit']='1';
$dictionary['User']['fields']['is_customer_c']['labelValue']='is customer';

 

 // created: 2021-09-15 12:58:27
$dictionary['User']['fields']['country_c']['inline_edit']='';
$dictionary['User']['fields']['country_c']['labelValue']='Country';

 

 // created: 2021-10-13 11:07:01
$dictionary['User']['fields']['state_c']['inline_edit']='';
$dictionary['User']['fields']['state_c']['labelValue']='State';

 

 // created: 2021-10-13 11:07:27
$dictionary['User']['fields']['city_c']['inline_edit']='';
$dictionary['User']['fields']['city_c']['labelValue']='city';

 

 // created: 2021-09-17 04:56:06
$dictionary['User']['fields']['passwordtext_c']['inline_edit']='';
$dictionary['User']['fields']['passwordtext_c']['labelValue']='PasswordText';

 
?>