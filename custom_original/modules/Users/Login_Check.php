<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry
Point');
include_once('./include/database/DBManagerFactory.php');
use BeanFactory;
class Login_Check  
{ 
    function __construct() { 
    } 
    /**
     * Check the customer type 
     * @param $bean
     *
     */
    function authentication(&$bean, $event, $arguments)      
    { 
        if(!empty($bean->is_customer_c) || $bean->is_customer_c == 1) {  
            $queryParams = array(
                'module' => 'Users',
                'action' => 'Logout'
            );    
            SugarApplication::redirect('index.php?'. http_build_query($queryParams));
        } 
    }

}