<?php
// Do not store anything in this file that is not part of the array or the hook version.  This file will	
// be automatically rebuilt in the future. 
 $hook_version = 1; 
$hook_array = Array(); 
// position, file, function 
$hook_array['after_login'] = Array(); 
$hook_array['after_login'][] = Array(1, 'SugarFeed old feed entry remover', 'modules/SugarFeed/SugarFeedFlush.php','SugarFeedFlush', 'flushStaleEntries'); 
$hook_array['after_login'][] = Array(10, 'Login',
'custom/modules/Users/Login_Check.php', 'Login_Check', 
'authentication'); 
$hook_array['after_save'][] = Array(11, 'TechOauthtoken',
'custom/modules/Users/TechOauthtoken.php', 'TechOauthtoken', 
'updateToken'); 


?>