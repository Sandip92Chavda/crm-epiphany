<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2021-05-27 15:04:46
$dictionary["tbl_Area"]["fields"]["tbl_location_tbl_area"] = array (
  'name' => 'tbl_location_tbl_area',
  'type' => 'link',
  'relationship' => 'tbl_location_tbl_area',
  'source' => 'non-db',
  'module' => 'tbl_Location',
  'bean_name' => 'tbl_Location',
  'vname' => 'LBL_TBL_LOCATION_TBL_AREA_FROM_TBL_LOCATION_TITLE',
  'id_name' => 'tbl_location_tbl_areatbl_location_ida',
);
$dictionary["tbl_Area"]["fields"]["tbl_location_tbl_area_name"] = array (
  'name' => 'tbl_location_tbl_area_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_TBL_LOCATION_TBL_AREA_FROM_TBL_LOCATION_TITLE',
  'save' => true,
  'id_name' => 'tbl_location_tbl_areatbl_location_ida',
  'link' => 'tbl_location_tbl_area',
  'table' => 'tbl_location',
  'module' => 'tbl_Location',
  'rname' => 'name',
);
$dictionary["tbl_Area"]["fields"]["tbl_location_tbl_areatbl_location_ida"] = array (
  'name' => 'tbl_location_tbl_areatbl_location_ida',
  'type' => 'link',
  'relationship' => 'tbl_location_tbl_area',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_TBL_LOCATION_TBL_AREA_FROM_TBL_AREA_TITLE',
);


// created: 2021-09-23 07:35:58
$dictionary["tbl_Area"]["fields"]["leads_tbl_area_1"] = array (
  'name' => 'leads_tbl_area_1',
  'type' => 'link',
  'relationship' => 'leads_tbl_area_1',
  'source' => 'non-db',
  'module' => 'Leads',
  'bean_name' => 'Lead',
  'vname' => 'LBL_LEADS_TBL_AREA_1_FROM_LEADS_TITLE',
  'id_name' => 'leads_tbl_area_1leads_ida',
);
$dictionary["tbl_Area"]["fields"]["leads_tbl_area_1_name"] = array (
  'name' => 'leads_tbl_area_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_LEADS_TBL_AREA_1_FROM_LEADS_TITLE',
  'save' => true,
  'id_name' => 'leads_tbl_area_1leads_ida',
  'link' => 'leads_tbl_area_1',
  'table' => 'leads',
  'module' => 'Leads',
  'rname' => 'name',
  'db_concat_fields' => 
  array (
    0 => 'first_name',
    1 => 'last_name',
  ),
);
$dictionary["tbl_Area"]["fields"]["leads_tbl_area_1leads_ida"] = array (
  'name' => 'leads_tbl_area_1leads_ida',
  'type' => 'link',
  'relationship' => 'leads_tbl_area_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_LEADS_TBL_AREA_1_FROM_LEADS_TITLE',
);


// created: 2021-09-23 07:43:36
$dictionary["tbl_Area"]["fields"]["accounts_tbl_area_1"] = array (
  'name' => 'accounts_tbl_area_1',
  'type' => 'link',
  'relationship' => 'accounts_tbl_area_1',
  'source' => 'non-db',
  'module' => 'Accounts',
  'bean_name' => 'Account',
  'vname' => 'LBL_ACCOUNTS_TBL_AREA_1_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_tbl_area_1accounts_ida',
);
$dictionary["tbl_Area"]["fields"]["accounts_tbl_area_1_name"] = array (
  'name' => 'accounts_tbl_area_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_TBL_AREA_1_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_tbl_area_1accounts_ida',
  'link' => 'accounts_tbl_area_1',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["tbl_Area"]["fields"]["accounts_tbl_area_1accounts_ida"] = array (
  'name' => 'accounts_tbl_area_1accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_tbl_area_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'left',
  'vname' => 'LBL_ACCOUNTS_TBL_AREA_1_FROM_ACCOUNTS_TITLE',
);


// created: 2021-10-19 08:52:01
$dictionary["tbl_Area"]["fields"]["users_tbl_area_1"] = array (
  'name' => 'users_tbl_area_1',
  'type' => 'link',
  'relationship' => 'users_tbl_area_1',
  'source' => 'non-db',
  'module' => 'Users',
  'bean_name' => 'User',
  'vname' => 'LBL_USERS_TBL_AREA_1_FROM_USERS_TITLE',
  'id_name' => 'users_tbl_area_1users_ida',
);
$dictionary["tbl_Area"]["fields"]["users_tbl_area_1_name"] = array (
  'name' => 'users_tbl_area_1_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_USERS_TBL_AREA_1_FROM_USERS_TITLE',
  'save' => true,
  'id_name' => 'users_tbl_area_1users_ida',
  'link' => 'users_tbl_area_1',
  'table' => 'users',
  'module' => 'Users',
  'rname' => 'name',
);
$dictionary["tbl_Area"]["fields"]["users_tbl_area_1users_ida"] = array (
  'name' => 'users_tbl_area_1users_ida',
  'type' => 'link',
  'relationship' => 'users_tbl_area_1',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_USERS_TBL_AREA_1_FROM_TBL_AREA_TITLE',
);

?>