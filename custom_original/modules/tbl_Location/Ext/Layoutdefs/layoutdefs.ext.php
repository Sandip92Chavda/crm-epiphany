<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2021-05-27 15:04:46
$layout_defs["tbl_Location"]["subpanel_setup"]['tbl_location_tbl_area'] = array (
  'order' => 100,
  'module' => 'tbl_Area',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_TBL_LOCATION_TBL_AREA_FROM_TBL_AREA_TITLE',
  'get_subpanel_data' => 'tbl_location_tbl_area',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


 // created: 2021-05-27 15:04:46
$layout_defs["tbl_Location"]["subpanel_setup"]['tbl_location_users'] = array (
  'order' => 100,
  'module' => 'Users',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_TBL_LOCATION_USERS_FROM_USERS_TITLE',
  'get_subpanel_data' => 'tbl_location_users',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);

?>