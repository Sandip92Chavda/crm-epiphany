<?php
if (!defined('sugarEntry')) { 
    define('sugarEntry', true);
}   
Class CallOlaApi{
    public function getPlanList(){
        $url = $GLOBALS['sugar_config']['get_oss_plan_url'];   
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
        "authorization: Basic YWRtaW46YWRtaW5AMTIz",
        "Content-Type: application/json", 
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);
        $response = [];$keyP = ''; 
        if(!empty($resp)){
          $data =   json_decode($resp, true);
          if(!empty($data['postpaidplanList']) && count($data['postpaidplanList'])>0){
              foreach($data['postpaidplanList'] as $key => $value){
                $response[$value['id']] = $value['name'];
              } 
          } 
        } 
        return $response;
    }  
    public function getPlanDetail($id = ''){
      $url = $GLOBALS['sugar_config']['get_oss_plan_url'];  
      $curl = curl_init($url);
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

      $headers = array(
      "authorization: Basic YWRtaW46YWRtaW5AMTIz",
      "Content-Type: application/json", 
      );
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
      //for debug only!
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

      $resp = curl_exec($curl);
      curl_close($curl);
      $response = [];$keyP = ''; 
      if(!empty($resp)){
        $data =  json_decode($resp, true); 
        if(!empty($data['postpaidplanList']) && count($data['postpaidplanList'])>0){
            foreach($data['postpaidplanList'] as $key => $value){
              // $response[$value['id']] = $value['name'];
              if($value['id'] == $id){
                $response = $value;
              }
            } 
        } 
      } 
      return $response;
    }
    public function getCountryList() {  
      $url = $GLOBALS['sugar_config']['get_oss_url'].'/country';   
      $curl = curl_init($url);
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

      $headers = array( 
      "authorization: Basic YWRtaW46YWRtaW5AMTIz",
      "Content-Type: application/json",  
      );
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
      //for debug only!
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

      $resp = curl_exec($curl);
      curl_close($curl);
      $response = [];$keyP = ''; 
      if(!empty($resp)) {  
        $data =   json_decode($resp, true);
        if(!empty($data['countryList']) && count($data['countryList'])>0){
            foreach($data['countryList'] as $key => $value){
              $response[$value['id']] = $value['name'];
            } 
        }  
      } 
      return $response;
    }
    public function getStateList() {  
      $url = $GLOBALS['sugar_config']['get_oss_url'].'/state';    
      $curl = curl_init($url);
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

      $headers = array( 
      "authorization: Basic YWRtaW46YWRtaW5AMTIz",
      "Content-Type: application/json",  
      );
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
      //for debug only!
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

      $resp = curl_exec($curl);
      curl_close($curl);
      $response = [];$keyP = '';$newKey = 0; 
      if(!empty($resp)) {  
        $data =   json_decode($resp, true);
        if(!empty($data['stateList']) && count($data['stateList'])>0){
            foreach($data['stateList'] as $key => $value){
              $newKey = $value['countryPojo']['id'].'_'.$value['id'];
              $response[$newKey] = $value['name'];
            } 
        }  
      } 
      return $response;
    } 
    public function getCityList() {   
      $url = $GLOBALS['sugar_config']['get_oss_url'].'/city';    
      $curl = curl_init($url);
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

      $headers = array( 
      "authorization: Basic YWRtaW46YWRtaW5AMTIz",
      "Content-Type: application/json",  
      );
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
      //for debug only!
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

      $resp = curl_exec($curl);
      curl_close($curl);
      $response = [];$keyP = ''; $newKey = 0;
      if(!empty($resp)) {   
        $data =   json_decode($resp, true);
        // echo "<pre>";print_r($data);die;
        if(!empty($data['cityList']) && count($data['cityList'])>0){
            foreach($data['cityList'] as $key => $value){ 
              $newKey = $value['statePojo']['countryPojo']['id'].'_'.$value['statePojo']['id'].'_'.$value['id'];
              $response[$newKey] = $value['name'];
            }  
        }  
      }   
      return $response;
    }

    public function getAllCountryList() {  
      $url = $GLOBALS['sugar_config']['get_oss_url'].'/country';   
      $curl = curl_init($url);
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

      $headers = array( 
      "authorization: Basic YWRtaW46YWRtaW5AMTIz",
      "Content-Type: application/json",  
      );
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
      //for debug only!
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

      $resp = curl_exec($curl);
      curl_close($curl);
      $response = [];$keyP = '';$data = []; 
      if(!empty($resp)) {  
        $data =   json_decode($resp, true);
        // if(!empty($data['countryList']) && count($data['countryList'])>0){
        //     foreach($data['countryList'] as $key => $value){
        //       $response[$value['id']] = $value['name'];
        //     } 
        // }  
      } 
      return $data;
    }
    public function getAllStateList() {  
      $url = $GLOBALS['sugar_config']['get_oss_url'].'/state';    
      $curl = curl_init($url);
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

      $headers = array( 
      "authorization: Basic YWRtaW46YWRtaW5AMTIz",
      "Content-Type: application/json",  
      );
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
      //for debug only!
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

      $resp = curl_exec($curl);
      curl_close($curl);
      $response = [];$keyP = ''; $data = [];
      if(!empty($resp)) {  
        $data =   json_decode($resp, true);
        // if(!empty($data['stateList']) && count($data['stateList'])>0){
        //     foreach($data['stateList'] as $key => $value){
        //       $response[$value['id']] = $value['name'];
        //     } 
        // }  
      } 
      return $data;
    }  
    public function getAllCityList() {   
      $url = $GLOBALS['sugar_config']['get_oss_url'].'/city';    
      $curl = curl_init($url);
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

      $headers = array( 
      "authorization: Basic YWRtaW46YWRtaW5AMTIz",
      "Content-Type: application/json",  
      );
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
      //for debug only!
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

      $resp = curl_exec($curl);
      curl_close($curl);
      $response = [];$keyP = '';  $data = [];
      if(!empty($resp)) {  
        $data =   json_decode($resp, true);
        // if(!empty($data['cityList']) && count($data['cityList'])>0){
        //     foreach($data['cityList'] as $key => $value){
        //       $response[$value['id']] = $value['name'];
        //     }  
        // }  
      } 
      return $data;
    }
    public function getBillList(){
      $url = $GLOBALS['sugar_config']['get_oss_url'].'/invoice/search?custmobile=8160958662';   
      $curl = curl_init($url);
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

      $headers = array(  
      "authorization: Basic YWRtaW46YWRtaW5AMTIz",
      "Content-Type: application/json",  
      );
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
      //for debug only!
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

      $resp = curl_exec($curl);
      curl_close($curl);
      $response = [];
      if(!empty($resp)) {   
        $response =  json_decode($resp, true);
      }
      return $response;
    }
    public function getDataUage($username = '') {   
      $url = $GLOBALS['sugar_config']['get_oss_url'].'/dbcdrprocessing/postpaiddemo';   
      // $url = $GLOBALS['sugar_config']['get_oss_url'].'/dbcdrprocessing/'.$username;   
      $curl = curl_init($url);
      curl_setopt($curl, CURLOPT_URL, $url); 
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 

      $headers = array(   
      "authorization: Basic YWRtaW46YWRtaW5AMTIz",
      "Content-Type: application/json",  
      );
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
      //for debug only!
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

      $resp = curl_exec($curl);
      curl_close($curl);
      $response = [];  $download =0; $upload = 0;
      if(!empty($resp)) {   
        $response =  json_decode($resp, true); 
        $downloadArray = array_column($response['dbcdrprocessinglist'], 'acctinputoctets');
        $uploadArray = array_column($response['dbcdrprocessinglist'], 'acctoutputoctets');
        $download = floor(array_sum($downloadArray)/1024);
        $upload = floor(array_sum($uploadArray)/1024);
        // /** convert byte into mb */
        // $common = new CommonService(); 
      }  
      return ['upload' => $upload,'download' => $download];
    }
    public function getCustomerDetail($userName = ''){
      $response = [];
      // $url = $GLOBALS['sugar_config']['get_oss_url'].'/customers/user15';   
      $url = $GLOBALS['sugar_config']['get_oss_url'].'/customers/'.$userName; 
      $curl = curl_init($url); 
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

      $headers = array( 
      "authorization: Basic YWRtaW46YWRtaW5AMTIz",
      "Content-Type: application/json",  
      );
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
      //for debug only!
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

      $resp = curl_exec($curl);
      curl_close($curl);
      if(!empty($resp)) {   
        $response =  json_decode($resp, true); 
      } 
      return !empty($response['customerList'])?$response['customerList']:[];
    }
    public function getCustomers(){
      $response = [];
      // $url = $GLOBALS['sugar_config']['get_oss_url'].'/customers/user15';   
      $url = $GLOBALS['sugar_config']['get_oss_url'].'/customers'; 
      $curl = curl_init($url); 
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

      $headers = array( 
      "authorization: Basic YWRtaW46YWRtaW5AMTIz",
      "Content-Type: application/json",  
      );
      curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
      //for debug only!
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

      $resp = curl_exec($curl);
      curl_close($curl);
      $responseData =  json_decode($resp, true); 
      $response = [];
      if(!empty($responseData['customerList']) && count($responseData['customerList'])>0){
        $response[0] = '';  
          foreach($responseData['customerList'] as $key => $value){
            $response[$value['id']] = $value['username'];
          } 
      } 
      return !empty($response)?$response:[]; 
    }
}  
?>  