<?php
include_once('./include/database/DBManagerFactory.php');
use BeanFactory; 
class CommonService 
{   
    /**
     * Generate Password
     * @param null
     * @result string 
     */
    public function generatePassword(){
        return bin2hex(openssl_random_pseudo_bytes(4));
    }
    /**
     * Generate Account Number
     * @param null
     * @result string 
     */
    public function generateAccountNumber($length = 10){ 
        $intMin = (10 ** $length) / 10; // 100...
        $intMax = (10 ** $length) - 1;  // 999...
        $codeRandom = mt_rand($intMin, $intMax);
        if($this->checkAcNumber($codeRandom)){
            return $this->generateAccountNumber();
        }
        return $codeRandom;
    }
    public function checkAcNumber($number){ 
        $accountBean = BeanFactory::getBean('Accounts')->retrieve_by_string_fields(array('accountnumber_c' => $number));   
        if(!empty($accountBean)){
            return true;
        }
        return false; 
    }

}
