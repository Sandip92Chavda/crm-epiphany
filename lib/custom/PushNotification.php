<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

include_once('./include/database/DBManagerFactory.php');

use BeanFactor;

class PushNotification {

    //Register Device 
	public function registerDevice($args) {
			
		$currentDateTime=date('Y-m-d H:i:s');
		$this->db->query ( "SET NAMES 'utf8';" );
		//Check If Device Already Register Than Update It Else Create It
		$sql = "SELECT * FROM apns_devices WHERE `user_id` = ".$this->_allowNullChar($args['user_id'])."";
		$deviceResult=$this->db->query($sql);
		if($deviceResult->num_rows > 0){
			//Update
			$updateQry = "UPDATE `apns_devices` SET 
            
			`app_name`= ".$this->_allowNullChar($args['app_name']).",
			`app_version`= ".$this->_allowNullChar($args['app_version']).",
			`os`= ".$this->_allowNullChar($args['os']).",
			`device_token`= ".$this->_allowNullChar($args['device_token']).",
			`device_name`= ".$this->_allowNullChar($args['device_name']).",
			`device_model`= ".$this->_allowNullChar($args['device_model']).",
			`device_version`= ".$this->_allowNullChar($args['device_version']).",
			`push_badge`= ".$this->_allowNullChar($args['push_badge']).",
			`push_alert`= ".$this->_allowNullChar($args['push_alert']).",
			`push_sound`= ".$this->_allowNullChar($args['push_sound']).",
			`push_vibrate`= ".$this->_allowNullChar($args['push_vibrate']).",
			`environment`= ".$this->_allowNullChar($args['environment']).",
			`status`='active',
			`updated_date`= '{$currentDateTime}',
			`language`= ".$this->_allowNullChar($args['language'])." 
			WHERE `user_id` = ".$this->_allowNullChar($args['user_id'])."";
			$this->db->query ( $updateQry );
			// print_r($updateQry);die;
		}else{
			//Insert
			$insertQry = "INSERT INTO `apns_devices`(`client_id`,`user_id`,`device_uid`,`device_token`,`app_name`,`app_version`,`os`,`device_name`,`device_model`,`device_version`,`push_badge`,`push_alert`,`push_sound`,`push_vibrate`,`environment`,`status`,`created_date`,`updated_date`,`badge_count`,`language`) 
			VALUES (
			".$this->_allowNullChar($args['client_id']).",
			".$this->_allowNullChar($args['user_id']).",
			".$this->_allowNullChar($args['device_uid']).",
			".$this->_allowNullChar($args['device_token']).",
			".$this->_allowNullChar($args['app_name']).",
			".$this->_allowNullChar($args['app_version']).",
			".$this->_allowNullChar($args['os']).",
			".$this->_allowNullChar($args['device_name']).",
			".$this->_allowNullChar($args['device_model']).",				
			".$this->_allowNullChar($args['device_version']).",
			".$this->_allowNullChar($args['push_badge']).",
			".$this->_allowNullChar($args['push_alert']).",
			".$this->_allowNullChar($args['push_sound']).",
			".$this->_allowNullChar($args['push_vibrate']).",
			".$this->_allowNullChar($args['environment']).",
			'active',
			'{$currentDateTime}',
			'{$currentDateTime}',
			0,
			".$this->_allowNullChar($args['language']).")";
			$this->db->query ( $insertQry );
		}
	}

    private function _allowNullChar($val) {
		if (trim ( $val ) != "") {
			$search_arr = array ("\\","'");
			$replace_arr = array ("","''");
			$ret = "'" . str_replace ( $search_arr, $replace_arr, $val ) . "'";
		} else
			$ret = "NULL";
	
		return $ret;
	}


	/* Send Push Message */
	public static function sendPushMessage($message, $messageType , $id , $receiverId = NULL, $customData = array(),$deliver_by_cron = 'No', $status = '',$ticket_id = 0) {
		// Add Message to APNS Master table
		// echo"hfuh";die;
		$db = \DBManagerFactory::getInstance();
		$GLOBALS['db'];

		$currentDateTime = date('Y-m-d H:i:s');
		$push_type= 'Normal';
		if(!empty($customData['push_type'])) {
			$push_type = 'Admin';
		}/*  */
		$GLOBALS['db']->query ( "SET NAMES 'utf8';" );
		$insertQuery = "INSERT INTO `apns_master` VALUES (NULL, '{$message}', '{$messageType}', '{$id}','{$ticket_id}','{$push_type}', '{$status}', '{$currentDateTime}','{$currentDateTime}')";
		// print_r($insertQuery);die;

		$GLOBALS['db']->query($insertQuery); 
		// $apns_master_id = $this->db->insert_id;
		$apns_master_id = $GLOBALS['db']->insert_id;
		// print_r($apns_master_id);die;

		// Fetch User List to whome push will be sent
		if(!empty($receiverId)) {
			// print_r($receiverId);die;
			if(is_array($receiverId)) { 
				$receiverIds = implode(',', $receiverId);
				
                $sql = "SELECT * FROM apns_devices WHERE id IN ({$receiverIds}) AND push_alert = 'enabled' AND status='active'";
            } else {
                $sql = "SELECT * FROM apns_devices WHERE id = '{$receiverId}' AND push_alert = 'enabled' AND status='active'";
            }
		} else {
			$sql = "SELECT * FROM apns_devices WHERE push_alert = 'enabled' AND status='active'";
		}
		// print_r($sql);die;
		
		$deviceResult = $GLOBALS['db']->query($sql);
		// print_r($result);die;
		// $deviceResult = $GLOBALS['db']->fetchByAssoc($result);
		// echo"<pre>";print_r($deviceResult);die;
		
		//Prepare message for android and iOS based on device result
		$messageIds = array();
		if($deviceResult->num_rows > 0) {
			while($device = $GLOBALS['db']->fetchByAssoc($deviceResult)) {
				// print_r($device);die;
				$customParam = array();
				if(!empty($customData)) {
					$customParam = array_merge($customParam,$customData);
					// print_r($customParam);die;
				}
				// echo"dsd";die;
				// print_r($device['os']);die;
				// echo "d";die;
				self::sendPushNotification($message, $device['device_token']);
			
				if(strtolower($device['os'])==='android') {
					// Create Message for Android
					$customParam['clientid']=$device['client_id'];
					// $customParam['messageTitle']=$messageTitle;
					$customParam['messageType']=$messageType;
					$customParam['receiverId'] = $device['id'];
					if($device['push_vibrate']=='enabled') {
						$customParam ['vibrate'] = ( string ) $device['push_vibrate'];
					}
					
					$messageArr = array ();
					$messageArr ['message'] = $message;
					if($device['push_badge']=='enabled') {
						$messageArr ['badge'] = ( int ) 1;
					}
					// if($device['push_sound']=='enabled') {
						// 	// echo'tyu';die;
						// 	$messageArr ['sound'] = ( string ) $this->sound;
						// }
						if(!empty($customParam)) {
							$messageArr ['customParam'] = $customParam;
						}
						// echo'd';die;

					//Queue Message For Android and Return Message ID
					// echo'fgh';die;
					$messageIds[] = self::_queueMessages($apns_master_id, $device, $messageArr,$deliver_by_cron, 'Android');
					
				} else if(strtolower($device['os'])==='ios') {
					//Create Message for iOS
					$customParam ['clientid'] = $device['client_id'];
					// $customParam ['messageTitle'] = $messageTitle;
					$customParam ['messageType'] = $messageType;
					$customParam ['receiverId'] = $device['id'];
					if($device['push_vibrate']=='enabled') {
						$customParam ['vibrate'] = ( string ) $device['push_vibrate'];
					}

					$messageArr = array ();
					$messageArr ['aps'] = array ();
					$messageArr ['aps'] ['alert'] = $message;
					if($device['push_badge']=='enabled') {
						$messageArr ['aps'] ['badge'] = ( int ) 1;
					}
					if($device['push_sound']=='enabled') {
						$messageArr ['aps'] ['sound'] = ( string ) $this->sound;
					}
					if(!empty($customParam)) {
						$messageArr ['customParam'] = $customParam;
					}
					//Queue Message For iOS and Return Message ID
					$messageIds[] = self::_queueMessages($apns_master_id, $device, $messageArr,$deliver_by_cron, 'iOS');
				}
			}
		}
		//Send Message to User Device
		if(!empty($messageIds) && $deliver_by_cron == 'No') {
			self::_sendPushMessage($messageIds);
		}
		return true;
	}

	//Queue Push Messages for delivery
	public static function _queueMessages($apns_master_id, $deviceDetails, $message ,$deliver_by_cron, $os) {
		$GLOBALS['db']->query("SET NAMES 'utf8';");
		$currentDateTime=date('Y-m-d H:i:s');
		$message = json_encode($message, JSON_UNESCAPED_UNICODE);
		$sql = "INSERT INTO `apns_messages` VALUES ( NULL, {$apns_master_id}, {$deviceDetails['device_id']}, '{$deviceDetails['client_id']}', '{$message}','queued','{$os}','{$currentDateTime}','{$currentDateTime}','{$deliver_by_cron}','No');";
		// print_r($sql);die;
		$GLOBALS['db']->query($sql);
		// print_r($GLOBALS['db']->insert_id);exit();
		return $GLOBALS['db']->insert_id;		
		// echo 'ty';die;
	}

	//Send Push Message to Device
	public static function _sendPushMessage($messageIdArr) {
		$messagesIds = implode(',', $messageIdArr);
		$GLOBALS['db']->query("SET NAMES 'utf8';");
		$sql = "SELECT am.message_id, am.formated_message, am.os, ad.device_token, am.device_id, ad.environment, ad.client_id
				FROM `apns_messages` am 
				INNER JOIN apns_devices ad ON am.device_id = ad.device_id 
				WHERE am.message_id IN (".$messagesIds.")";
		$messageResult = $GLOBALS['db']->query($sql);

		if($messageResult->num_rows > 0) {
			$deliverdIds = array();
			$failedIds = array();
			while($message = $GLOBALS['db']->fetchByAssoc($messageResult)) {
				$message_id = $message['message_id'];
				$messageText = $message['formated_message'];
				$token = $message['device_token'];
				$retValue = "failed";
				if(strtolower($message['os'])==='android') { 
					//Send Push Message to Android
					$androdAPNS = new Android ( $GLOBALS['db'], $message['client_id'] );
					$retValue = $androdAPNS->pushFCMMessage($token, json_decode($messageText));

				} elseif(strtolower($message['os'])==='ios') {
					//Send Push Message to iOS
					$iOSAPNS = new APNS ( $GLOBALS['db'], $message['client_id'] );
					$retValue = $iOSAPNS->pushMessage($messageText, $token, $message['environment']);
				}
				if($retValue == "delivered") {
					$deliverdIds[] = $message_id;
				}
				else if($retValue == "failed") {
					$failedIds[] = $message_id;
				}
			}
			//Update Message Status
			self::_udpateMessageStatus($deliverdIds, $failedIds);
		}
	}

	// Update Message Status
	public static function _udpateMessageStatus($deliverdIdsArr = array(), $failedIdsArr = array()) {
		$currentDateTime=date('Y-m-d H:i:s');	
		if(!empty($deliverdIdsArr)) {
			$deliverdIds = implode(',', $deliverdIdsArr);
			$updateQuryDelivered = "UPDATE apns_messages SET `status` = 'delivered',`updated_date` = '{$currentDateTime}'
									WHERE `status` = 'queued' AND `message_id` IN (".$deliverdIds.") ";
			$GLOBALS['db']->query($updateQuryDelivered);
		}
	
		if(!empty($failedIdsArr)) {
			$failedIds = implode(',', $failedIdsArr);
			$updateQuryFailed = "UPDATE apns_messages SET `status` = 'failed',`updated_date` = '{$currentDateTime}'
									WHERE `status` = 'queued' AND `message_id` IN (".$failedIds.") ";
			$GLOBALS['db']->query($updateQuryFailed);
		}
	}


	//Send Push Notification Using Cron 
	public function sendNotificationByCron() {
		$sendNotification = array();
		$currentDateTime=date('Y-m-d H:i:s');	
		$sql = "SELECT am.message_id,am.device_id, am.formated_message, ad.device_token,ad.environment,am.os,am.client_id FROM apns_messages am
		 INNER JOIN apns_devices ad ON am.device_id = ad.device_id WHERE am.deliver_by_cron='Yes'
		  AND ad.status='active' AND am.status='queued' AND am.sent_by_crone='No' ORDER BY am.client_id LIMIT {$this->cronlimit}";
		$result = $GLOBALS['db']->query($sql);
		if($result->num_rows > 0){
			while($updateArr = $GLOBALS['db']->fetchByAssoc($result)) {
				// print_r($updateArr);
				$updateQury = "UPDATE apns_messages SET `sent_by_crone` = 'Yes',`updated_date` = '{$currentDateTime}'
								WHERE `message_id` = '{$updateArr['message_id']}' ";
				$GLOBALS['db']->query($updateQury);
				array_push($sendNotification,$updateArr['message_id']);
			}
		}
		if(!empty($sendNotification)) {
			$messagesIds = implode(',', $sendNotification);
			$sql2 = "SELECT am.message_id,am.device_id, am.formated_message, ad.device_token,ad.environment,am.os,am.client_id FROM apns_messages am
			INNER JOIN apns_devices ad ON am.device_id = ad.device_id WHERE am.message_id IN (".$messagesIds.") ";
			$data = $GLOBALS['db']->query($sql2);
			if($data->num_rows > 0) {
				$deliverdIds = array();
				$failedIds = array();
				while($row = $GLOBALS['db']->fetchByAssoc($data)) {
					$pid = $row['message_id'];
					$message = $row['formated_message'];
					$token = $row['device_token'];
					$development=$row['environment'];
					$os= $row['os'];
					$clientId=$row['client_id'];
					$retValue = "failed";
				
					if(strtolower($os) === 'android') {
						//Create Object of Android Push Class.
						$androdAPNS = new Android ( $GLOBALS['db'], $clientId );
						$retValue = $androdAPNS->pushFCMMessage($token, json_decode($message));
						
						// if($retValue == "delivered"){
						// 	$this->_udpateMessageStatus($pid, '');
						// }else if($retValue == "failed"){
						// 	$this->_udpateMessageStatus('',$pid);
						// }
					} else if(strtolower($os) === 'ios'){
						//Create Object of iOS Push Class.
						$iOSAPNS = new APNS ( $GLOBALS['db'], $clientId );
						$retValue = $iOSAPNS->pushMessage($message,$token,$development);						
						// if($retValue == "delivered"){
						// 	$this->_udpateMessageStatus($pid, '');
						// }else if($retValue == "failed"){
						// 	$this->_udpateMessageStatus('',$pid);
						// }
					}	
					if($retValue == "delivered"){
						$deliverdIds[] = $pid;
					}
					else if($retValue == "failed"){
						$failedIds[] = $pid;
					}
					//Update Message Status
					self::_udpateMessageStatus($deliverdIds, $failedIds);
				}
			}
	 	}	
	}



	public static function sendPushNotification($message, $device_id){
		// echo "abc";die;
        // echo $message;die;
		// print_r($device_id);die;
        //API URL of FCM
        $url = 'https://fcm.googleapis.com/fcm/send';

        /*api_key available in:
        Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key*/    $api_key = 'AAAAdTmC-Ik:APA91bEMTd_viYJO0FlHuQnZL3C1qVzTlohK6_GxnK_k7E5ipmSQMFMjW_LdLDjZjmdzEjCUkeaXPqV5cP1oWBOKcVfsfN4mWB2Pen1c8SwF4zZ9uYtzTo_CxyiyykiGa4iKIfzZ5Tg7';
		// print_r($device_id);die;          
        $fields = array (
            'registration_ids' => array (
				$device_id
            ),
            // 'data' => array (
            //     "message" => $message
            // )
			'notification' => array (
				"title" => "SuiteCRM",
				"body" => $message,
				'sound' => 'default'
			)
        );

        //header includes Content type and api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key='.$api_key
        );
                    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
		// print_r($result);die;
    }

}
?>