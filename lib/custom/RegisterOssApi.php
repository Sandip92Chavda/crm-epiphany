<?php
/** store oss data */ 
class RegisterOssApi{
    public function getCity($city){
       $city = explode('_',$city);
        return !empty($city[2]) ? $city[2] : 1;
    } 
    public function getState($state){
        $state = explode('_',$state);
        return !empty($state[1]) ? $state[1] : 1;
    } 
    public function addDataToOss($data){
        $dataArray = [
            'username' => $data->name,
            'password' => $data->passwordtext_c,
            'firstname' => $data->firstname_c,
            'lastname' => $data->lastname_c,
            'email' => $data->email1,
            'status' => 'Active',
            'failcount' => 0,
            'acctno' => '',
            'custType' => !empty($data->customer_type_list)?$data->customer_type_list:'Prepaid',
            'phone' => $data->phone_office,
            'billday' => 1,
            'partnerid' => 1,
            'addresstype' => "home",
            'address1' => $data->addressstreet_c,
            'address2' => '',
            'city' => $this->getCity($data->city_c),
            'state' =>  $this->getState($data->state_c),
            'country' => $data->country_c,
            'pincode' => $data->pincode_c, 
            'planMappingPojoList' => [
                [
                    'planId' => $data->plantype_c
                ]
            ],
            "flashMsg" => null,
            "invoiceOption" => $data->invoiceoption_c, 
            "parentCustomersId" => null,  
            "billentityname" =>  $data->billentity_c,
            "authProtocol" => $data->authprotocol_c,
            "allowedIPAddress" => $data->ipaddress_c,
            "billday" => $data->billday_c,
            "building" => $data->building_c,
            "cableLength" => $data->cablelength_c,
            "filterId" => $data->networkfilterid_c,
            "ipMode" => $data->ipmode_c,
            "ipPool" => $data->ippool_c,
            "latitude" => $data->latitude_c,
            "longitude" => $data->longitude_c,
            "macBinding" => $data->macbinding_c,
            "nasportBind" => $data->nasportbinding_c,
            "nasportId" => $data->nasportid_c,
            "node" => $data->node_c,
            "pop" => $data->pop_c,
            "switchName" => $data->switch_c, 
            "switchPortNo" => $data->switchport_c,
            "connectionType" => $data->connectiontype_c,
            "area" => '-' 
        ];
        $postdata = json_encode($dataArray); 
        // print_r($postdata);die; 
        $url = "http://192.168.0.26:8080/api/v1/customers";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $headers = array(
            "authorization: Basic YWRtaW46YWRtaW5AMTIz",
            "Content-Type: application/json",
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
        //for debug only!  
        $resp = curl_exec($ch); 
        curl_close($ch);   
         // print_r($postdata);die;    
    }
    /**
     * Get Customer detail
     * @param $username
     * @return array
     */
    public function getCustomerDetail($userName = ''){ 
        $custId = '';
        $url = $GLOBALS['sugar_config']['get_oss_customer_url'].'/'.$userName;
        $curl = curl_init($url); 
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
        "authorization: Basic YWRtaW46YWRtaW5AMTIz",
        "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);  
        $response = [];$keyP = '';
        if(!empty($resp)){
          $data =  json_decode($resp, true);
          if(!empty($data['customerList'][0]['id'])){
            $custId =  $data['customerList'][0]['id'];  
            return $custId; 
          }
        }
    }
    public function getCustomerAddress($id = ''){ 
        $custId = '';
        $url = $GLOBALS['sugar_config']['get_oss_url'].'/customeraddress/'.$id;
        $curl = curl_init($url); 
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
        "authorization: Basic YWRtaW46YWRtaW5AMTIz",
        "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $resp = curl_exec($curl);
        curl_close($curl);  
        $response = [];$keyP = '';
        if(!empty($resp)){
          $data =  json_decode($resp, true);
          if(!empty($data['customeraddresslist'])){
            $response = $data['customeraddresslist'];
          }
        }
        return $response;
    }
     /**
     * Update Customer detail
     * @param $data ,$custId
     * @return array 
     */
    public function updateDataToOss(object $data,$custId){ 
        $dataArray = [ 
            'username' => $data->name, 
            'password' => $data->passwordtext_c,
            'firstname' => $data->firstname_c,
            'lastname' => $data->lastname_c,
            'email' => $data->email1,
            'status' => 'Active',
            'failcount' => 0,
            'acctno' => "",
            'custType' => !empty($data->customer_type_list)?$data->customer_type_list:'Prepaid',
            'phone' => $data->phone_office,
            'billday' => 1,
            'partnerid' => 1,
            'addresstype' => "home",
            'address1' => $data->addressstreet_c,
            'address2' => '',
            'city' => $this->getCity($data->city_c),
            'state' =>  $this->getState($data->state_c),
            'country' => $data->country_c,
            'pincode' => $data->pincode_c, 
            'planMappingPojoList' => [
                [
                    'planId' => $data->plantype_c
                ]
            ], 
            'outstanding' => 0,   
            'last_password_change' => date('Y-m-d\TH:i:s'), 
            "flashMsg" => null,
            "invoiceOption" => $data->invoiceoption_c, 
            "parentCustomersId" => null,  
            "billentityname" =>  $data->billentity_c,
            "authProtocol" => $data->authprotocol_c,
            "allowedIPAddress" => $data->ipaddress_c,
            "billday" => $data->billday_c,
            "building" => $data->building_c,
            "cableLength" => $data->cablelength_c,
            "filterId" => $data->networkfilterid_c,
            "ipMode" => $data->ipmode_c,
            "ipPool" => $data->ippool_c,
            "latitude" => $data->latitude_c,
            "longitude" => $data->longitude_c,
            "macBinding" => $data->macbinding_c,
            "nasportBind" => $data->nasportbinding_c,
            "nasportId" => $data->nasportid_c,
            "node" => $data->node_c,
            "pop" => $data->pop_c,
            "switchName" => $data->switch_c, 
            "switchPortNo" => $data->switchport_c,
            "connectionType" => $data->connectiontype_c,
            "area" => '-' 
        ];  
        $postdata = json_encode($dataArray);    
        // print_r($postdata);die;
        // echo $postdata;die; 
        $url = $GLOBALS['sugar_config']['get_oss_customer_url'].'/'.$custId;
        $ch = curl_init($url);  
        curl_setopt($ch, CURLOPT_URL, $url);  
        // curl_setopt($ch, CURLOPT_POST, 1); 
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT"); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
        $headers = array( 
            "authorization: Basic YWRtaW46YWRtaW5AMTIz",
            "Content-Type: application/json",
        ); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
        //for debug only!  
        $resp = curl_exec($ch);  
        curl_close($ch);    
        // echo "<pre>";print_r($resp);die;   
    }
}
?>