<?php

if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}
require_once './modules/Users/User.php';   
include_once('./include/database/DBManagerFactory.php');
class Jjwg_AreasViewArea_Detail_Map extends SugarView
{
    public function __construct()
    {
        parent::__construct();
        $this->db = \DBManagerFactory::getInstance();
    }

    /**
     * @deprecated deprecated since version 7.6, PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code, use __construct instead
     */
    public function Jjwg_AreasViewArea_Detail_Map()
    {
        $deprecatedMessage = 'PHP4 Style Constructors are deprecated and will be remove in 7.8, please update your code';
        if (isset($GLOBALS['log'])) {
            $GLOBALS['log']->deprecated($deprecatedMessage);
        } else {
            trigger_error($deprecatedMessage, E_USER_DEPRECATED);
        }
        self::__construct();  
    }  


    public function display() 
    { 
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $parts = parse_url($actual_link);
        parse_str($parts['query'], $query);
        $sql = sprintf('select * from  jjwg_areas where id = "%s"',
        $query['id']);
        $results = $this->db->query($sql);
        $row  = $GLOBALS['db']->fetchByAssoc($results);
        $location = json_decode(html_entity_decode($row['coordinates']),true);
        // $newArr = array_reduce($location, function($carry, $item){
        //     $carry[] = ['lat' => $item['lat'], 'lng' => $item['long']];
        //     return $carry; 
        // }); 
        $locationJson = html_entity_decode($row['coordinates']);  
        // echo "<pre>";print_r($locationJson);die; 

      // $location = [ 
      //               ['name' => '2018-02-21 12:00' ,'lat' => '22.279374', 'long' => '70.763428'],
      //               ['name' => '2018-02-21 12:00' ,'lat' => '22.280486', 'long' => '70.764608'],
      //               ['name' => '2018-02-21 12:00' ,'lat' => '22.282611', 'long' => '70.767119'],
      //               ['name' => '2018-02-21 12:00' ,'lat' => '22.283921', 'long' => '70.768621'],
      //               ['name' => '2018-02-21 12:00' ,'lat' => '22.284854', 'long' => '70.769672'],
      //               ['name' => '2018-02-21 12:00' ,'lat' => '22.285628', 'long' => '70.770831'],
      //               ['name' => '2018-02-21 12:00' ,'lat' => '22.285966', 'long' => '70.772333'],
      //               ['name' => '2018-02-21 12:00' ,'lat' => '22.284894', 'long' => '70.773148']
      //             ]; 
                  // echo "<pre>";print_r($GLOBALS);die; 
    ?> 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  <title><?php echo $GLOBALS['mod_strings']['LBL_AREA_MAP']; ?></title>
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
  <link rel="stylesheet" type="text/css" href="cache/themes/<?php echo $GLOBALS['theme']; ?>/css/style.css" />
  <style type="text/css">
    html { height: 100% }
    body { height: 100%; margin: 0px; padding: 0px }
    #mapCanvas {
      width: 700px;
      height: 500px;
      float: left;
    }
    #infoPanel {
      width: 450px;
      float: left;
      margin-left: 10px;
    }
    #mapCanvas, #infoPanel, #markerStatus, #info, #address { 
      font-size: 12px;
      line-height: 16px;
      font-family:Arial,Verdana,Helvetica,sans-serif;
      color: #444444;
      margin-bottom: 5px;
    }
    b {
      font-weight: normal;
      color: #000000;
    }
  </style>

  <script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=<?= $GLOBALS['jjwg_config']['google_maps_api_key']; ?>&sensor=false&libraries=drawing"></script>

  <script type="text/javascript"> 

// Define Map Data for Javascript
var jjwg_config_defaults = <?php echo (!empty($GLOBALS['jjwg_config_defaults'])) ? json_encode($GLOBALS['jjwg_config_defaults']) : '[]'; ?>;
var jjwg_config = <?php echo (!empty($GLOBALS['jjwg_config'])) ? json_encode($GLOBALS['jjwg_config']) : '[]'; ?>;
var polygonPoints = <?php echo (!empty($GLOBALS['polygon'])) ? json_encode($GLOBALS['polygon']) : '[]'; ?>;
var pathCoordinates = Array();
var map;
function initialize() { 
   var latLng = new google.maps.LatLng(
    <?php echo (!empty($GLOBALS['loc']['lat'])) ? $GLOBALS['loc']['lat'] : $GLOBALS['jjwg_config']['map_default_center_latitude']; ?>,
    <?php echo (!empty($GLOBALS['loc']['lng'])) ? $GLOBALS['loc']['lng'] : $GLOBALS['jjwg_config']['map_default_center_longitude']; ?>
   );

   const map = new google.maps.Map(document.getElementById("mapCanvas"), { 
    // zoom: 2,
    // center: { lat: 0, lng:-180},  
    mapTypeId: "terrain",    
   }); 
  // const flightPlanCoordinates = [  
  //   { name:"sa",lat: 22.279374, lng: 70.763428},
  //   { name:"sa",lat: 22.280486, lng: 70.764608},
  //   { name:"sa",lat: 22.282611, lng: 70.767119},
  //   { name:"sa",lat: 22.283921, lng: 70.768621},
  //   { name:"sa",lat: 22.284854, lng: 70.769672},
  //   { name:"sa",lat: 22.285628, lng: 70.770831},
  //   { name:"sa",lat: 22.285966, lng: 70.772333},
  //   { name:"sa",lat: 22.284894, lng: 70.773148}
  // ];
  const flightPlanCoordinates = <?php echo $locationJson; ?>;
  const flightPath = new google.maps.Polyline({
    path: flightPlanCoordinates,
    geodesic: true,
    strokeColor: "#FF0000",
    strokeOpacity: 1.0,
    strokeWeight: 5,
    zIndex: 1
  });
  var markers = [
        <?php if(count($location) > 0){ 
            foreach($location as $lc){
                echo '["'.$lc['date'].'", '.$lc['lat'].', '.$lc['lng'].'],';
            }
        }
        ?>
    ];
     // Info window content
    var infoWindowContent = [
        <?php if(count($location) > 0){ 
            foreach($location as $lc){?>
                ['<div class="info_content">' +
                '<h3><?php echo $lc['date']; ?></h3>'+'</div>'],
        <?php }
        }
        ?>
    ];

    // Add multiple markers to map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
     var bounds = new google.maps.LatLngBounds(); 
    // Place each marker on the map  
    for( i = 0; i < markers.length; i++ ) { 
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map, 
            title: markers[i][0]
        });
        
        // Add info window to marker    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Center the map to fit all markers on the screen
        map.fitBounds(bounds);
        
    }

  flightPath.setMap(map); 
    
}            

// Onload handler to fire off the app.
google.maps.event.addDomListener(window, 'load', initialize);
</script>
</head>
<body>
  <div id="mapCanvas"></div>
  <div id="infoPanel"><b></b>
    <div id="markerStatus"><i></i></div>
    <div id="info"></div>
    <div id="address"></div>
  </div>
</body>
</html>
<?php
    }
}
