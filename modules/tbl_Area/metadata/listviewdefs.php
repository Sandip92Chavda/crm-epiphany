<?php
$module_name = 'tbl_Area';
$listViewDefs [$module_name] = 
array (
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => true,
  ),
  'DESCRIPTION' => 
  array (
    'type' => 'text',
    'label' => 'LBL_DESCRIPTION',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'PINCODE' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_PINCODE',
    'width' => '10%',
    'default' => true,
  ),
  'OFFICEADDRESS' => 
  array (
    'type' => 'text',
    'studio' => 'visible',
    'label' => 'LBL_OFFICEADDRESS',
    'sortable' => false,
    'width' => '10%',
    'default' => true,
  ),
  'ASSIGNED_USER_NAME' => 
  array (
    'width' => '9%',
    'label' => 'LBL_ASSIGNED_TO_NAME',
    'module' => 'Employees',
    'id' => 'ASSIGNED_USER_ID',
    'default' => false,
  ),
);
;
?>
