<?php
$popupMeta = array (
    'moduleMain' => 'tbl_Location',
    'varName' => 'tbl_Location',
    'orderBy' => 'tbl_location.name',
    'whereClauses' => array (
  'name' => 'tbl_location.name',
  'assigned_user_id' => 'tbl_location.assigned_user_id',
  'longitude' => 'tbl_location.longitude',
  'latitude' => 'tbl_location.latitude',
),
    'searchInputs' => array (
  1 => 'name',
  4 => 'assigned_user_id',
  5 => 'longitude',
  6 => 'latitude',
),
    'searchdefs' => array (
  'name' => 
  array (
    'name' => 'name',
    'width' => '10%',
  ),
  'assigned_user_id' => 
  array (
    'name' => 'assigned_user_id',
    'label' => 'LBL_ASSIGNED_TO',
    'type' => 'enum',
    'function' => 
    array (
      'name' => 'get_user_array',
      'params' => 
      array (
        0 => false,
      ),
    ),
    'width' => '10%',
  ),
  'longitude' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_LONGITUDE',
    'width' => '10%',
    'name' => 'longitude',
  ),
  'latitude' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_LATITUDE',
    'width' => '10%',
    'name' => 'latitude',
  ),
),
);
